//
//  SCDropPinViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 02/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>

#define REASON_TAG 3
#define DESCRIPTION_TAG 2
#define DATE_TAG 4
#define TIME_TAG 5
#define ADDRESS_TAG 6
#define GEOPOINT_TAG 7
#define ADD_IMAGE_LABEL_TAG 8
#define IMAGE_TAG 9

@interface SCDropPinViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    UIActionSheet *actionSheet;
    int CURRENTCASE;
    int numberOfPhotos;
    int selectedRow;
}
@property (retain,nonatomic) UIImage *placehoderImage;
@property (strong,nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSMutableArray *categoryArray;
@property (retain, nonatomic) NSMutableArray *MediaImagesArray;
@property (strong, nonatomic) PFObject *selectedObj;
@property (strong, nonatomic) PFGeoPoint *geoPointVal;
@property (copy,nonatomic) NSString *selectedCategory;
@property (copy,nonatomic) NSString *selectedComments;
@property (copy,nonatomic) NSString *revAddressValue;
@property (copy,nonatomic) NSString *selectedDate;
@property (copy,nonatomic) NSString *selectedTime;
@property (nonatomic,readwrite) int mode;
@property (strong, nonatomic) IBOutlet UITableView *dropPinTableView;
@property (strong, nonatomic) IBOutlet UIPickerView *reasonPickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datetimePickerView;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UILabel *loadViewLabel;
@end
