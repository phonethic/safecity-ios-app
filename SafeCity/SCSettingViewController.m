//
//  SCSettingViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 03/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "SCSettingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SCDatabaseOperations.h"
#import "SCShowWebSiteViewController.h"
#import "SWRevealViewController.h"
#import "SCAppDelegate.h"

#define TEL @"9876543210"
#define EMAIL @"apps@phonethics.in"
#define WEBSITELINK @"http://www.phonethics.in"

@interface SCSettingViewController ()

@end

@implementation SCSettingViewController
@synthesize settingTableView;
@synthesize loadingView;
@synthesize indicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Settings_Tab_Event"];

    //----------start of reveal
    
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slidemenu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    //----------end of reveal

   // self.navigationItem.hidesBackButton = YES;
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressed:)];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.settingTableView  addGestureRecognizer:viewTap];
    
    loadingView.backgroundColor = [UIColor blackColor];
    loadingView.layer.cornerRadius = 10;
    [self.loadingView setHidden:YES];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useNotificationWithString:)
	 name:refreshnotificationName
	 object:nil];
}
- (void)useNotificationWithString:(NSNotification *)notification //use notification method and logic
{
    DebugLog(@"SETTINGS : got notification");
    NSDictionary *dictionary = [notification userInfo];
    NSString *value = [dictionary valueForKey:refreshnotificationKey];
    if ([value isEqualToString:REFRESHSETTINGS])
    {
        DebugLog(@"Got settings notification");
        [self.loadingView setHidden:YES];
    }
}
- (void)doneBtnPressed:(id)sender
{
    [Flurry logEvent:@"Updating_User_Details_Event"];
    if([SAFECITY_APP_DELEGATE networkavailable]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [SCDatabaseOperations addCurrentUserDetails:@"User" first:[defaults objectForKey:@"firstname"] last:[defaults objectForKey:@"lastname"] email:[defaults objectForKey:@"email"] phone:[defaults objectForKey:@"phone"]];
        [self.loadingView setHidden:NO];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    //self.navigationItem.rightBarButtonItem = nil;
    //[self.navigationController popViewControllerAnimated:YES];
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    
    [self.view endEditing:YES];
    [self scrollTobottom];
}
-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [settingTableView setContentOffset:bottomOffset animated:YES];
}
- (void)viewDidUnload
{
    [self setSettingTableView:nil];
    [self setLoadingView:nil];
    [self setIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self getRowsHeight:indexPath.section row:indexPath.row];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    
//    if(section == 0)
//        return @"Contact";
//    else
//        return @"About";
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 8;
    }
    return 40;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,30)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame = CGRectMake(15, 0, 300, 30);
    headerLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    switch (section) {
        case 0:
            headerLabel.text = @"User Details";
            break;
        case 1:
            headerLabel.text = @"About";
            break;
        default:
            headerLabel.text = @"";
            break;
    }
    headerLabel.textColor = DEFAULT_COLOR;
    [customView addSubview:headerLabel];
    return customView;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row] ;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        DebugLog(@"id = %@",CellIdentifier);
        switch (indexPath.section) {
            case 0:
            {
                if(indexPath.row==0)
                {
                    UITextField *firstnameTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    firstnameTextField.placeholder = @"Enter first name";
                    [self setTextFieldProperties:firstnameTextField text:[defaults objectForKey:@"firstname"]];
                    firstnameTextField.tag = 1;
                    firstnameTextField.keyboardType = UIKeyboardTypeDefault;
                    firstnameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    [cell.contentView addSubview:firstnameTextField];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.imageView.image = [UIImage imageNamed:@"contact_card.png"];
                } else if(indexPath.row==1) {
                    UITextField *lastnameTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    lastnameTextField.placeholder = @"Enter last name";
                    [self setTextFieldProperties:lastnameTextField text:[defaults objectForKey:@"lastname"]];
                    lastnameTextField.tag = 2;
                    lastnameTextField.keyboardType = UIKeyboardTypeDefault;
                    lastnameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    [cell.contentView addSubview:lastnameTextField];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.imageView.image = [UIImage imageNamed:@"contact_card.png"];
                } else if(indexPath.row==2) {
                    UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    emailTextField.placeholder = @"Enter email address";
                    [self setTextFieldProperties:emailTextField text:[defaults objectForKey:@"email"]];
                    emailTextField.tag = 3;
                    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
                    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    [cell.contentView addSubview:emailTextField];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.imageView.image = [UIImage imageNamed:@"email.png"];
                } else if(indexPath.row==3) {
                    UITextField *numberTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    numberTextField.placeholder = @"Enter phone number";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [self setTextFieldProperties:numberTextField text:[defaults objectForKey:@"phone"]];
                    numberTextField.tag = 4;
                    numberTextField.keyboardType = UIKeyboardTypePhonePad;
                    [cell.contentView addSubview:numberTextField];
                    cell.imageView.image = [UIImage imageNamed:@"phone.png"];
                }
                    
            }
                break;
                
            case 1:
            {
//                if(indexPath.row==0)
//                {
//                    UILabel *shareLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
//                    shareLabel.tag = 4;
//                    [self setLabelProperties:shareLabel text:@"Share Application"];
//                    [cell.contentView addSubview:shareLabel];
//                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//                    cell.imageView.image = [UIImage imageNamed:@"share.png"];
//                } 
//                    
                if(indexPath.row==0) {
                    UILabel *supportLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    supportLabel.tag = 5;
                    [self setLabelProperties:supportLabel text: EMAIL];
                    [cell.contentView addSubview:supportLabel];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.imageView.image = [UIImage imageNamed:@"email.png"];
                } else if(indexPath.row==1) {
                    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    emailLabel.tag = 6;
                    [self setLabelProperties:emailLabel text: WEBSITELINK];
                    [cell.contentView addSubview:emailLabel];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.imageView.image = [UIImage imageNamed:@"weblink.png"];
                } else if(indexPath.row==2) {
                    UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 280, 100)];
                    thumbImg.tag = 7;
                    thumbImg.image = [UIImage imageNamed:@"phonethics.png"];
                    thumbImg.contentMode = UIViewContentModeScaleAspectFit;
                    thumbImg.layer.cornerRadius = 10;
                    thumbImg.layer.masksToBounds = YES;
                    thumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
                    thumbImg.layer.borderWidth = 1.0;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell.contentView addSubview:thumbImg];

                }
            }
            default:
                break;
        }
        
    } else {
    
        switch (indexPath.section) {
            case 0:
            {
                UITextField *tempTextField = (UITextField *) [cell viewWithTag:indexPath.row+1];

                if(indexPath.row==0)
                {
                    tempTextField.text = [defaults objectForKey:@"firstname"];
                } else if(indexPath.row==1) {
                    tempTextField.text = [defaults objectForKey:@"lastname"];
                } else if(indexPath.row==2) {
                    tempTextField.text = [defaults objectForKey:@"email"];
                } else if(indexPath.row==3) {
                    tempTextField.text = [defaults objectForKey:@"phone"];
                }
            }
                break;
        }
    }
    
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //UILabel * lblTitle = (UILabel *)[cell viewWithTag:2];
    //lblTitle.textColor = [UIColor yellowColor];
    switch (indexPath.section) {
        case 1:
        {
            switch (indexPath.row) {
//                case 0: //share
//                {
//                    
//                }
//                    break;
                case 0: //email
                {
                    MFMailComposeViewController *mailComposeViewController=[[MFMailComposeViewController alloc]init];
                    mailComposeViewController.mailComposeDelegate = self;
                    mailComposeViewController.navigationBar.tintColor = DEFAULT_COLOR;
                    [mailComposeViewController setToRecipients:[NSArray arrayWithObjects:EMAIL,nil]];
                    [mailComposeViewController setSubject:@"Ref: Safe City"];
                    [mailComposeViewController setMessageBody:@"Enter your message here." isHTML:YES];
                    [mailComposeViewController setModalPresentationStyle:UIModalPresentationFormSheet];
                    [self.navigationController presentModalViewController:mailComposeViewController animated:YES];
                }
                    break;
                case 1: //website
                {
                    if([SAFECITY_APP_DELEGATE networkavailable]) {
                        SCShowWebSiteViewController *scshowWebSiteviewController = [[SCShowWebSiteViewController alloc] initWithNibName:@"SCShowWebSiteViewController" bundle:nil] ;
                        scshowWebSiteviewController.urlString = WEBSITELINK;
                        scshowWebSiteviewController.title = @"Phonethics.in";
                        [self.navigationController pushViewController:scshowWebSiteviewController animated:YES];
                    } else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(int)getRowsCount:(int)secVal
{
    switch (secVal) {
        case 0:
            return 4;
            break;
            
        case 1:
            return 3;
            break;
            
                
        default:
            return 0;
            break;
    }
}

-(int)getRowsHeight:(int)secVal  row:(int)rowVal
{
    switch (secVal) {
        case 0:
            return 50;
            break;
            
        case 1:
        {
            if(rowVal==0||rowVal==1)
                return 50;
            else 
                return 120;
        }

            break;
           default:
            return 0;
            break;
    }
}

-(void)setTextFieldProperties:(UITextField *)textField text:(NSString *)textString
{
    textField.text = textString;
    textField.delegate = self;
    textField.backgroundColor = [UIColor clearColor];
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont systemFontOfSize:14.0f];
    textField.borderStyle = UITextBorderStyleNone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.returnKeyType = UIReturnKeyNext;
    textField.textAlignment = UITextAlignmentLeft;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
}



-(void)setLabelProperties:(UILabel *)label text:(NSString *)textString
{
    label.text = textString;
    label.font = [UIFont systemFontOfSize:14.0f];
    label.textAlignment = UITextAlignmentLeft;
    label.textColor = [UIColor blackColor];
    label.backgroundColor =  [UIColor clearColor];
}

#pragma mark TextField Delegate methods
-(void)textChanged:(UITextField *)textField
{
    DebugLog(@"textfield data %@ ",textField.text);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    switch (textField.tag) {
        case 1:
            [defaults setObject:textField.text forKey:@"firstname"];
            break;
        case 2:
            [defaults setObject:textField.text forKey:@"lastname"];
            break;
        case 3:
            [defaults setObject:textField.text forKey:@"email"];
            break;
        case 4:
            [defaults setObject:textField.text forKey:@"phone"];
            break;
            
        default:
            break;
    }
    [defaults synchronize];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 1 && textField.frame.origin.y < settingTableView.contentOffset.y)
        [settingTableView setContentOffset:CGPointMake(0,textField.frame.origin.y-30) animated:YES];
    else if(textField.tag==3 && textField.frame.origin.y > settingTableView.contentOffset.y)
        [settingTableView setContentOffset:CGPointMake(0,textField.frame.origin.y+60) animated:YES];
    else if(textField.tag==4 && textField.frame.origin.y > settingTableView.contentOffset.y)
        [settingTableView setContentOffset:CGPointMake(0,textField.frame.origin.y+60) animated:YES];
}

#pragma mark MFMailComposeViewControllerDelegate methods
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    DebugLog(@"Mail Result %u",result);
    switch (result)
    {
        case MFMailComposeResultCancelled :
            DebugLog(@"Mail Cancelled");
            break;
        case MFMailComposeResultSaved :
            DebugLog(@"Mail Saved");
            break;
        case MFMailComposeResultSent :
            DebugLog(@"Mail Sent");
            break;
        case MFMailComposeResultFailed :
            DebugLog(@"Mail Failed");
            break;
        default:
            break;
    }
    [controller dismissModalViewControllerAnimated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    DebugLog(@"textField.tag %d",textField.tag);
    UITableViewCell *cell = [settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0]];
    if (cell != nil)
    {
        UITextField *tempTextField = (UITextField *) [cell viewWithTag:textField.tag + 1];
        DebugLog(@"temp %@",tempTextField.text);
        [tempTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return NO;
}
@end
