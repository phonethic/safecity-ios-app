//
//  SCAnnotation.h
//  SafeCity
//
//  Created by Kirti Nikam on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface SCAnnotation : NSObject <MKAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@end
