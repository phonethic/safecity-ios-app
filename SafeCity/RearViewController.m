/*

 Copyright (c) 2013 Joan Lluch <joan.lluch@sweetwilliamsl.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

*/

#import "RearViewController.h"
#import "SWRevealViewController.h"
#import "SCViewController.h"
#import "SCShowListViewController.h"
#import "SCSettingViewController.h"
#import "SCHelpViewController.h"

@interface RearViewController()

@end

@implementation RearViewController

@synthesize rearTableView = _rearTableView;
@synthesize menuArray;

/*
 * The following lines are crucial to understanding how the SWRevealController works.
 *
 * In this example, we show how a SWRevealController can be contained in another instance 
 * of the same class. We have three scenarios of hierarchies as follows
 *
 * In the first scenario a FrontViewController is contained inside of a UINavigationController.
 * And the UINavigationController is contained inside of a SWRevealController. Thus the
 * following hierarchy is created:
 *
 * - SWRevealController is parent of:
 * - - UINavigationController is parent of:
 * - - - FrontViewController
 *
 * In the second scenario a MapViewController is contained inside of a UINavigationController.
 * And the UINavigationController is contained inside of a SWRevealController. Thus the
 * following hierarchy is created:
 *
 * - SWRevealController is parent of:
 * - - UINavigationController is parent of:
 * - - - MapViewController
 *
 * In the third scenario a SWRevealViewController is contained directly inside of another.
 * SWRevealController. The second SWRevealController can in turn contain anything.
 * contain any of the above Thus the
 * following hierarchy is created:
 *
 * - SWRevealController is parent of:
 * - - SWRevealController
 *
 * If you set ShouldContainRevealControllerInNavigator to true, a UINavigationController will be used to
 * contain child SWRevealControllers, thus producing a vertical cascade effect on the user interface
 */

#define ShouldContainRevealControllerInNavigator false

- (void)viewDidLoad
{
	[super viewDidLoad];
    menuArray = [[NSArray alloc] initWithObjects:MAP,LIST,SETTINGS,HELP, nil ];

    // We determine whether we have a grand parent SWRevealViewController, this means we are at least one level behind the hierarchy
    SWRevealViewController *parentRevealController = self.revealViewController;
    SWRevealViewController *grandParentRevealController = parentRevealController.revealViewController;
    
    UIBarButtonItem *titleButtonItem = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:nil action:0];
    UIBarButtonItem *spaceButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:0];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slidemenu.png"]
            style:UIBarButtonItemStyleBordered target:grandParentRevealController action:@selector(revealToggle:)];
    
    // if we have a reveal controller as a grand parent, this means we are are being added as a
    // child of a detail (child) reveal controller, so we add a gesture recognizer provided by our grand parent to our
    // toolbar as well as a "reveal" button
    if ( grandParentRevealController )
    {
        // to present a title, we count the number of ancestor reveal controllers we have, this is of course
        // only a hack for demonstration purposes, on a real project you would have a model telling this.
        NSInteger level=0;
        UIViewController *controller = grandParentRevealController;
        while( nil != (controller = [controller revealViewController]) )
            level++;
        
        NSString *title = [NSString stringWithFormat:@"Detail Level %d", level];
        
        // if our parent reveal controller was contained in a navigation controller we set the title, gesture, and button
        // on its navigation bar
        if ([parentRevealController navigationController])
        {
            [parentRevealController.navigationController.navigationBar addGestureRecognizer:grandParentRevealController.panGestureRecognizer];
            parentRevealController.navigationItem.leftBarButtonItem = revealButtonItem;
            parentRevealController.navigationItem.title = title;
        }
        
        // otherwise, our reveal controller was directly inserted in the hierarchy so we use our toolbar to set the
        // controls
        else
        {
            [_rearToolBar addGestureRecognizer:grandParentRevealController.panGestureRecognizer];
            [titleButtonItem setTitle:title];
            [_rearToolBar setItems:@[revealButtonItem,spaceButtonItem,titleButtonItem,spaceButtonItem]];
        }
    }
    
    // otherwise, we are in the top reveal controller, so we just add a title
    else
    {
        [titleButtonItem setTitle:@"Safe City"];
        [_rearToolBar setItems:@[spaceButtonItem,titleButtonItem,spaceButtonItem]];
        [self.rearTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
    }
}



#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [menuArray count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblTitle;
    UIImageView *thumbImg;

	static NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
   
	if (nil == cell)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        //Initialize Image View with tag 1.(Thumbnail Image)
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 27, 27)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleAspectFill;
        [cell.contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(thumbImg.frame.origin.x +thumbImg.frame.size.width + 15, cell.frame.origin.y, 220, cell.frame.size.height )];
        lblTitle.tag = 2;
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.font = [UIFont systemFontOfSize:16.0];
        lblTitle.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblTitle];

	}
	thumbImg = (UIImageView *)[cell viewWithTag:1];
    thumbImg.image =[UIImage imageNamed:[NSString stringWithFormat:@"slide_%@.png",[[menuArray objectAtIndex:indexPath.row] lowercaseString]]];
    
    lblTitle = (UILabel *)[cell viewWithTag:2];
    lblTitle.text = [menuArray objectAtIndex:indexPath.row];
	
    UIView *selectedView = [[UIView alloc] init];
    selectedView.backgroundColor = DEFAULT_COLOR;
    cell.selectedBackgroundView = selectedView;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = [self revealViewController];
    UIViewController *frontViewController = revealController.frontViewController;
    UINavigationController *frontNavigationController =nil;
    
    if ( [frontViewController isKindOfClass:[UINavigationController class]] )
        frontNavigationController = (id)frontViewController;
    
    if ([[menuArray objectAtIndex:indexPath.row] isEqualToString:MAP])
    {
        // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
        if ( ![frontNavigationController.topViewController isKindOfClass:[SCViewController class]] )
        {
			SCViewController *mapViewController = [[SCViewController alloc] init];
            mapViewController.title =  [menuArray objectAtIndex:indexPath.row];
			UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
            navigationController.navigationBar.tintColor = DEFAULT_COLOR;
			[revealController setFrontViewController:navigationController animated:YES];
        }
		else
		{
            [revealController revealToggleAnimated:YES];
		}
    }
    else if ([[menuArray objectAtIndex:indexPath.row] isEqualToString: LIST])
    {
        if ( ![frontNavigationController.topViewController isKindOfClass:[SCShowListViewController class]] )
        {
			SCShowListViewController *listViewController = [[SCShowListViewController alloc] init];
            listViewController.title =  [menuArray objectAtIndex:indexPath.row];
			UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:listViewController];
             navigationController.navigationBar.tintColor = DEFAULT_COLOR;
			[revealController setFrontViewController:navigationController animated:YES];
		}
        else
		{
            [revealController revealToggleAnimated:YES];
		}
    }
    else if ([[menuArray objectAtIndex:indexPath.row] isEqualToString: SETTINGS])
    {
        if ( ![frontNavigationController.topViewController isKindOfClass:[SCSettingViewController class]] )
        {
			SCSettingViewController *settingsViewController = [[SCSettingViewController alloc] init];
            settingsViewController.title =  [menuArray objectAtIndex:indexPath.row];
			UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
             navigationController.navigationBar.tintColor = DEFAULT_COLOR;
			[revealController setFrontViewController:navigationController animated:YES];
		}
        
		// Seems the user attempts to 'switch' to exactly the same controller he came from!
		else
		{
            [revealController revealToggleAnimated:YES];
		}
    }
    else if ([[menuArray objectAtIndex:indexPath.row] isEqualToString: UPDATES])
    {
        NSLog(@"No view controller for updates .. :( ");
    }
    else if ([[menuArray objectAtIndex:indexPath.row] isEqualToString: HELP])
    {
        if ( ![frontNavigationController.topViewController isKindOfClass:[SCHelpViewController class]] )
        {
			SCHelpViewController *helpViewController = [[SCHelpViewController alloc] init];
            helpViewController.title =  [menuArray objectAtIndex:indexPath.row];
			UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:helpViewController];
            navigationController.navigationBar.tintColor = DEFAULT_COLOR;
			[revealController setFrontViewController:navigationController animated:YES];
		}
        
		// Seems the user attempts to 'switch' to exactly the same controller he came from!
		else
		{
            [revealController revealToggleAnimated:YES];
		}
    }
}



@end