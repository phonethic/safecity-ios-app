//
//  SCDatabaseOperations.m
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "SCDatabaseOperations.h"
#import <CommonCrypto/CommonDigest.h>
#import "SCMediaObject.h"

#define SAFECITY_APP_ID @"YpqFCRLYxWEFcOizNbJ027Dw0pzg01vmPUnBILRJ"
#define SAFECITY_APP_KEY @"oAh7kWBv1GEUVwIAC6kE6DvS8Ptcol8WZcbFK66e"

@interface SCDatabaseOperations ()
+ (void)postDropPinNotificationWithData:(PFFile *)value;
+ (void)postNotificationWithString:(NSString *)value;
+ (NSString *) md5:(NSString *) input;
@end

@implementation SCDatabaseOperations


+(void)setAppId
{
    [Parse setApplicationId:SAFECITY_APP_ID clientKey:SAFECITY_APP_KEY];
}
+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

+(void)loginUser : (NSString *) userid password:(NSString *) pass
{
    // Wipe out old user defaults
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"objectIDArray"]){
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"objectIDArray"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Simple way to create a user or log in the existing user
    // For your app, you will probably want to present your own login screen
    
    PFUser *currentUser = [PFUser currentUser];
    
    if (!currentUser) {
        // Dummy username and password
        PFUser *user = [PFUser user];
        user.username = userid ;
        user.password = pass ;
        //user.email = @"";
        DebugLog(@"new sign in");
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                // Assume the error is because the user already existed.
                DebugLog(@"re sign in");
                [PFUser logInWithUsername:userid password:pass];
            }
            else
            {
                DebugLog(@"succeeded %d",succeeded);
            }
        }];
    }
    
    
}


+(NSArray *)getAllRows:(NSString *)tableName
{
    PFQuery *query = [PFQuery queryWithClassName:tableName];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    return [query findObjects];
    
}
//+(void)findAddress:(NSString *)tableName searchText:(NSString *) textToSearch
//{
//    DebugLog(@"finding address...%@ : %@",tableName,textToSearch);
//    PFQuery *query = [PFQuery queryWithClassName:tableName];
//    query.cachePolicy = kPFCachePolicyNetworkElseCache;
//    [query whereKey:@"address" matchesRegex:textToSearch modifiers:@"i"];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (error)
//        {
//            DebugLog(@"--search error => %@--",error);
//        }
//        else
//        {
//            DebugLog(@"%@",objects);
//            [SCDatabaseOperations postNotificationWithString:@"searchSuccess" resultarray:objects];
//        }
//
//    }];
//}

+(void)addCurrentUserDetails:(NSString *)tableName first:(NSString *)firstname last:(NSString *)lastname email:(NSString *)lemail phone:(NSString *)number
{
    PFUser *currentUser = [PFUser currentUser];
    if (firstname != NULL)
        [currentUser setObject:firstname forKey:@"firstname"];
    
    if (lastname != NULL)
        [currentUser setObject:lastname forKey:@"lastname"];
    
    if (lemail != NULL)
        [currentUser setObject:lemail forKey:@"email"];
    
    if (number != NULL)
        [currentUser setObject:number forKey:@"phone"];
    
    [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--add success => --");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"User details saved successfully." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            
            NSMutableString *errorString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%@",error ]];
            DebugLog(@"--add error => %@ %d--",errorString,error.code);
            DebugLog(@"--errorString => %@ %d--",errorString,errorString.length);
            NSRange startRange = [errorString rangeOfString:@"error="];
            if (startRange.location !=NSNotFound) {
                NSRange endRange = [errorString rangeOfString:@"}"];
                DebugLog(@"location %d length %d",startRange.location,endRange.location);
                DebugLog(@"start from %d ",startRange.location+6);
                DebugLog(@"ent %d ",endRange.location-(startRange.location+6));
                DebugLog(@"--errorString substring => %@",[errorString substringWithRange:NSMakeRange(startRange.location+6,endRange.location-(startRange.location+6))]);
                NSString *errorNew = [errorString substringWithRange:NSMakeRange(startRange.location+6,endRange.location-(startRange.location+6))];
                
                NSString *firstCapChar = [[errorNew substringToIndex:1] capitalizedString];
                NSString *cappedString = [errorNew stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: cappedString delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Unknown error. Please try again later." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];

            }
        }
        [SCDatabaseOperations postNotificationWithString:REFRESHSETTINGS];
    }];
    

}
+(void)addRecord:(NSString *)tableName loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)reasonValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue
{
    PFObject *myObject = [PFObject objectWithClassName:tableName];
    [myObject setObject:[PFUser currentUser] forKey:@"user"];
    if (lgeoPoint != NULL)
        [myObject setObject:lgeoPoint forKey:@"location"];
    
    if (addressValue != NULL)
        [myObject setObject:addressValue forKey:@"address"];
    
    if (reasonValue != NULL)
        [myObject setObject:reasonValue forKey:@"reason"];
    
    if (datetimeValue != NULL)
        [myObject setObject:datetimeValue forKey:@"datetime"];

    if (commentsValue != nil)
        [myObject setObject:commentsValue forKey:@"comments"];
    
    if (imageValue != nil)
    {
        NSData *imageData = UIImagePNGRepresentation(imageValue);
        PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
        [imageFile save];
        [myObject setObject:imageFile forKey:@"imageFile"];
    }     
    [myObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--add success => --");
            [SCDatabaseOperations postNotificationWithString:REFRESHMAP];
        }
        else
        {
            DebugLog(@"--add error => %@--",error);
        }
    }];
}

+(void)updateRecord:(NSString *)tableName objId:(NSString *)lobjId loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)reasonValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue
{
    PFQuery *query = [PFQuery queryWithClassName:tableName];
    PFObject *reObj = [query getObjectWithId:lobjId];
    [reObj setObject:[PFUser currentUser] forKey:@"user"];
     if (lgeoPoint != NULL)
         [reObj setObject:lgeoPoint forKey:@"location"];
    
     if (addressValue != NULL)
         [reObj setObject:addressValue forKey:@"address"];
    
     if (reasonValue != NULL)
         [reObj setObject:reasonValue forKey:@"reason"];
    
     if (datetimeValue != NULL)
         [reObj setObject:datetimeValue forKey:@"datetime"];
    
    if (commentsValue != NULL)
        [reObj setObject:commentsValue forKey:@"comments"];
    
    if (imageValue != NULL)
    {
        NSData *imageData = UIImagePNGRepresentation(imageValue);
        PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
        [imageFile save];
        [reObj setObject:imageFile forKey:@"imageFile"];
    }
    [reObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--update success => --");
            [SCDatabaseOperations postNotificationWithString:REFRESHMAP];
        }
        else
        {
            DebugLog(@"--update error => %@--",error);
        }
    }];

}

+(void)deleteRecord:(NSString *)tableName objId:(NSString *)lobjId
{
    PFQuery *query = [PFQuery queryWithClassName:tableName];
    PFObject *reObj = [query getObjectWithId:lobjId];
    [reObj deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--delete success => --");
            [SCDatabaseOperations postNotificationWithString:REFRESHMAP];
        }
        else
        {
            DebugLog(@"--delete error => %@--",error);
        }
    }];
}

+(NSArray *)getCategoryList:(NSString *)tableName
{
    PFQuery *query = [PFQuery queryWithClassName:tableName];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    return [query findObjects];

}

+ (void)postNotificationWithString:(NSString *)value //post notification method and logic
{
    DebugLog(@"posting notification");
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:value forKey:refreshnotificationKey];
    [[NSNotificationCenter defaultCenter] postNotificationName:refreshnotificationName object:nil userInfo:dictionary];
}


//---------- Using PF Relation ------------------
//Incident_SafeCity - [objectId,active,address,category_id,date,descsription,location,mode,user_id,verified]
//Incident_Person - [objectId,incident_id,first,last,email,phone,
//Media - [objectId,incident_id,active,media_date,media_link,media_type]

+(void)addRecordWithRelation:(NSString *)tableName loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue
{
    PFObject *myObject = [PFObject objectWithClassName:INCIDENT_TABLE];
    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
    if (addressValue != NULL)
        [myObject setObject:addressValue forKey:@"address"];
    [myObject setObject:category_idValue forKey:@"category_id"];
    if (datetimeValue != NULL)
        [myObject setObject:datetimeValue forKey:@"date"];
    if (commentsValue != nil)
        [myObject setObject:commentsValue forKey:@"description"];
    if (lgeoPoint != NULL)
        [myObject setObject:lgeoPoint forKey:@"location"];
    [myObject setObject:[NSNumber numberWithInt:5] forKey:@"mode"];
    DebugLog(@"user %@",[PFUser currentUser].objectId);
    if ([PFUser currentUser].objectId != NULL)
        [myObject setObject:[PFUser currentUser].objectId forKey:@"user_id"];
    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"verified"];
    
    [myObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--Incident_SafeCity add success => with objectid %@ --",myObject.objectId);
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            DebugLog(@"--defaults %@ -- %@ -- %@ -- %@ -- %@ --",defaults,[defaults objectForKey:@"firstname"],[defaults objectForKey:@"lastname"],[defaults objectForKey:@"email"], [defaults objectForKey:@"phone"]);

            PFObject *IPObject = [PFObject objectWithClassName:INCIDENTPERSON_TABLE];
            [IPObject setObject:myObject.objectId forKey:@"incident_id"];
            if ([defaults objectForKey:@"firstname"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"firstname"] forKey:@"firstname"];
            if ([defaults objectForKey:@"lastname"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"lastname"] forKey:@"lastname"];
            if ([defaults objectForKey:@"email"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"email"] forKey:@"email"];
            if ([defaults objectForKey:@"phone"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"phone"] forKey:@"phone"];
            [IPObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded)
                {
                    DebugLog(@"--Incident_Person add success => --");}
            }];
            if (imageValue != NULL)
            {
                NSData *imageData = UIImagePNGRepresentation(imageValue);
                PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
                [imageFile save];
                
                PFObject *MObject = [PFObject objectWithClassName:MEDIA_TABLE];
                [MObject setObject:myObject.objectId forKey:@"incident_id"];
                [MObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
                if (datetimeValue != NULL)
                    [MObject setObject:datetimeValue forKey:@"media_date"];
                [MObject setObject:imageFile forKey:@"media_link"];
                [MObject setObject:[NSNumber numberWithInt:1] forKey:@"media_type"];
                [MObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded)
                    {
                        DebugLog(@"--Media add success => --");}
                }];
            }
            [SCDatabaseOperations postNotificationWithString:REFRESHMAP];
        }
        else
        {
            DebugLog(@"--add error => %@--",error);
        }
    }];
}

+(void)addRecordWithRelation:(NSString *)tableName loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue imageArray:(NSArray *)imageArrayValue datetime:(NSDate *)datetimeValue
{
    PFObject *myObject = [PFObject objectWithClassName:INCIDENT_TABLE];
    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
    if (addressValue != NULL)
        [myObject setObject:addressValue forKey:@"address"];
    [myObject setObject:category_idValue forKey:@"category_id"];
    if (datetimeValue != NULL)
        [myObject setObject:datetimeValue forKey:@"date"];
    if (commentsValue != nil)
        [myObject setObject:commentsValue forKey:@"description"];
    if (lgeoPoint != NULL)
        [myObject setObject:lgeoPoint forKey:@"location"];
    [myObject setObject:[NSNumber numberWithInt:5] forKey:@"mode"];
    DebugLog(@"user %@",[PFUser currentUser].objectId);
    if ([PFUser currentUser].objectId != NULL)
        [myObject setObject:[PFUser currentUser].objectId forKey:@"user_id"];
    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"verified"];
    
    [myObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--Incident_SafeCity add success => with objectid %@ --",myObject.objectId);
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            DebugLog(@"--defaults %@ -- %@ -- %@ -- %@ -- %@ --",defaults,[defaults objectForKey:@"firstname"],[defaults objectForKey:@"lastname"],[defaults objectForKey:@"email"], [defaults objectForKey:@"phone"]);
            
            PFObject *IPObject = [PFObject objectWithClassName:INCIDENTPERSON_TABLE];
            [IPObject setObject:myObject.objectId forKey:@"incident_id"];
            if ([defaults objectForKey:@"firstname"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"firstname"] forKey:@"firstname"];
            if ([defaults objectForKey:@"lastname"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"lastname"] forKey:@"lastname"];
            if ([defaults objectForKey:@"email"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"email"] forKey:@"email"];
            if ([defaults objectForKey:@"phone"] != NULL)
                [IPObject setObject:[defaults objectForKey:@"phone"] forKey:@"phone"];
            [IPObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded)
                {
                    DebugLog(@"--Incident_Person add success => --");}
            }];
            
            
            if (imageArrayValue != nil && imageArrayValue.count > 0)
            {
                for (int index = 0; index < imageArrayValue.count; index++)
                {
                    SCMediaObject *mediaObject = (SCMediaObject *)[imageArrayValue objectAtIndex:index];
                    DebugLog(@"index %d image %@ object id = %@",index,mediaObject.mediaImage,mediaObject.mediaObjectId);
                    if ([mediaObject.mediaObjectId isEqualToString:@""] && mediaObject.mediaImage != NULL)
                    {
                        DebugLog(@"inserting index %d image %@ object id = %@",index,mediaObject.mediaImage,mediaObject.mediaObjectId);
                        NSData *imageData = UIImagePNGRepresentation(mediaObject.mediaImage);
                        PFFile *imageFile = [PFFile fileWithName:[NSString stringWithFormat:@"SC_%llu.png",(long long)[[NSDate date] timeIntervalSinceReferenceDate]] data:imageData];
                        [imageFile save];
                        
                        PFObject *MObject = [PFObject objectWithClassName:MEDIA_TABLE];
                        [MObject setObject:myObject.objectId forKey:@"incident_id"];
                        [MObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
                        if (datetimeValue != NULL)
                            [MObject setObject:datetimeValue forKey:@"media_date"];
                        [MObject setObject:imageFile forKey:@"media_link"];
                        [MObject setObject:[NSNumber numberWithInt:1] forKey:@"media_type"];
                        [MObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (succeeded)
                            {
                                  DebugLog(@"--Media  %d add success => --",index);}
                        }];
                    }
                    mediaObject = nil;
                }
            }
            [SCDatabaseOperations postNotificationWithString:REFRESHMAP];
        }
        else
        {
            DebugLog(@"--add error => %@--",error);
        }
    }];
}


+(NSArray *)getAllCategories
{
    PFQuery *query = [PFQuery queryWithClassName:CATEGORY_TABLE];
    [query whereKey:@"visible" equalTo:[NSNumber numberWithBool:true]];
    [query orderByAscending:@"position"];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    return [query findObjects];
}
+(void)getImageFile:(PFObject *) inobject
{
    PFQuery *query = [PFQuery queryWithClassName:MEDIA_TABLE];
    [query whereKey:@"incident_id" equalTo:inobject.objectId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
    {
        // comments now contains the comments for myPost
        if (error)
        {
            // There was an error
            DebugLog(@"-- got media object error => %@--",error);
        } else
        {
            // objects has all the Posts the current user liked.
            DebugLog(@"-- got media object as => %@--",objects);
            if (objects.count > 0)
            {
                PFFile *tempImage = [[objects objectAtIndex:0] objectForKey:@"media_link"];
                DebugLog(@"tempImage %@",tempImage);
                if ((NSNull *)tempImage != [NSNull null])
                {
                    [SCDatabaseOperations postDropPinNotificationWithData:tempImage];
                }
            }
        }
    }];
}
+(void)getImageFiles:(PFObject *) inobject
{
    PFQuery *query = [PFQuery queryWithClassName:MEDIA_TABLE];
    [query whereKey:@"incident_id" equalTo:inobject.objectId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         // comments now contains the comments for myPost
         if (error)
         {
             // There was an error
              DebugLog(@"-- got media object error => %@--",error);
         } else
         {
             // objects has all the Posts the current user liked.
             DebugLog(@"-- got media object as => %@--",objects);
             [SCDatabaseOperations postDropPinNotificationWithArray:objects];
             
//             if (objects.count > 0)
//             {
//                 PFFile *tempImage = [[objects objectAtIndex:0] objectForKey:@"media_link"];
//                 DebugLog(@"tempImage %@",tempImage);
//                 if ((NSNull *)tempImage != [NSNull null])
//                 {
//                     [SCDatabaseOperations postDropPinNotificationWithData:tempImage];
//                 }
//             }
         }
     }];
}

+(NSString *)getTitle:(NSString *) objectId
{
    PFQuery *query = [PFQuery queryWithClassName:CATEGORY_TABLE];
    PFObject *myObject = [query getObjectWithId:objectId];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//     {
//         if (!error)
//         {
//             DebugLog(@"--Got CATEGORY_TABLE objects to delete => --%@",objects);
//        }
//     }];

    return [myObject objectForKey:@"title"];
}
+(NSString *)getTitle:(NSString *) objectId categoryArray:(NSArray *)catgArray
{
   // DebugLog(@"-catgArray -%@--",catgArray);

    for (PFObject *tempObj in catgArray)
    {
    //   DebugLog(@"-compare -%@--%@--",objectId,tempObj.objectId);
       if ([objectId isEqual:tempObj.objectId])
       {
           return [tempObj objectForKey:@"title"];
        }
    }
   return @"";
}
+(void)updateRecordWithRelation:(NSString *)tableName objId:(NSString *)lobjId loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue sendPostAs:(NSString *)postKey
{
    PFQuery *query = [PFQuery queryWithClassName:INCIDENT_TABLE];
    PFObject *myObject = [query getObjectWithId:lobjId];

    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
    if (addressValue != NULL)
        [myObject setObject:addressValue forKey:@"address"];
    [myObject setObject:category_idValue forKey:@"category_id"];
    if (datetimeValue != NULL)
        [myObject setObject:datetimeValue forKey:@"date"];
    if (commentsValue != nil)
        [myObject setObject:commentsValue forKey:@"description"];
    if (lgeoPoint != NULL)
        [myObject setObject:lgeoPoint forKey:@"location"];
    [myObject setObject:[NSNumber numberWithInt:5] forKey:@"mode"];
    if ([PFUser currentUser].objectId != NULL)
         [myObject setObject:[PFUser currentUser].objectId forKey:@"user_id"];
    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"verified"];
    
    [myObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--Incident_SafeCity update success => with objectid %@ --",myObject.objectId);
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            DebugLog(@"--defaults %@ -- %@ -- %@ -- %@ -- %@ --",defaults,[defaults objectForKey:@"firstname"],[defaults objectForKey:@"lastname"],[defaults objectForKey:@"email"], [defaults objectForKey:@"phone"]);
            
            PFQuery *IPquery = [PFQuery queryWithClassName:INCIDENTPERSON_TABLE];
            [IPquery whereKey:@"incident_id" equalTo:lobjId];
            [IPquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
             {
                 if (!error)
                 {   DebugLog(@"--Got Incident_Person objects to update  => --");
                     
                     for (PFObject *IPObject in objects)
                     {
                         if ([defaults objectForKey:@"firstname"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"firstname"] forKey:@"firstname"];
                         if ([defaults objectForKey:@"lastname"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"lastname"] forKey:@"lastname"];
                         if ([defaults objectForKey:@"email"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"email"] forKey:@"email"];
                         if ([defaults objectForKey:@"phone"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"phone"] forKey:@"phone"];
                         [IPObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                             if (succeeded)
                             {
                                 DebugLog(@"--Incident_Person update success => --");}
                         }];
                     }
                 }
             }];
 
            if (imageValue != NULL)
            {
                PFQuery *Mquery = [PFQuery queryWithClassName:MEDIA_TABLE];
                [Mquery whereKey:@"incident_id" equalTo:lobjId];
                [Mquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
                 {
                     if (!error)
                     {
                         DebugLog(@"--Got media objects to delete => --");
                         for (PFObject *obj in objects) {
                             [obj deleteInBackground];
                         }
                     }
                 }];
                NSData *imageData = UIImagePNGRepresentation(imageValue);
                PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
                [imageFile save];
                
                PFObject *MObject = [PFObject objectWithClassName:MEDIA_TABLE];
                [MObject setObject:myObject.objectId forKey:@"incident_id"];
                [MObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
                if (datetimeValue != NULL)
                    [MObject setObject:datetimeValue forKey:@"media_date"];
                [MObject setObject:imageFile forKey:@"media_link"];
                [MObject setObject:[NSNumber numberWithInt:1] forKey:@"media_type"];
                [MObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded)
                    {
                        DebugLog(@"--Media update success => --");}
                }];
            }
            [SCDatabaseOperations postNotificationWithString:postKey];
        }
        else
        {
            DebugLog(@"--update error => %@--",error);
        }
    }];
}

+(void)updateRecordWithRelation:(NSString *)tableName objId:(NSString *)lobjId loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue imageArray:(NSArray *)imageArrayValue datetime:(NSDate *)datetimeValue sendPostAs:(NSString *)postKey
{
    PFQuery *query = [PFQuery queryWithClassName:INCIDENT_TABLE];
    PFObject *myObject = [query getObjectWithId:lobjId];
    
    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
    if (addressValue != NULL)
        [myObject setObject:addressValue forKey:@"address"];
    [myObject setObject:category_idValue forKey:@"category_id"];
    if (datetimeValue != NULL)
        [myObject setObject:datetimeValue forKey:@"date"];
    if (commentsValue != nil)
        [myObject setObject:commentsValue forKey:@"description"];
    if (lgeoPoint != NULL)
        [myObject setObject:lgeoPoint forKey:@"location"];
    [myObject setObject:[NSNumber numberWithInt:5] forKey:@"mode"];
    if ([PFUser currentUser].objectId != NULL)
        [myObject setObject:[PFUser currentUser].objectId forKey:@"user_id"];
    [myObject setObject:[NSNumber numberWithInt:1] forKey:@"verified"];
    
    [myObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--Incident_SafeCity update success => with objectid %@ --",myObject.objectId);
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            DebugLog(@"--defaults %@ -- %@ -- %@ -- %@ -- %@ --",defaults,[defaults objectForKey:@"firstname"],[defaults objectForKey:@"lastname"],[defaults objectForKey:@"email"], [defaults objectForKey:@"phone"]);
            
            PFQuery *IPquery = [PFQuery queryWithClassName:INCIDENTPERSON_TABLE];
            [IPquery whereKey:@"incident_id" equalTo:lobjId];
            [IPquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
             {
                 if (!error)
                 {   DebugLog(@"--Got Incident_Person objects to update  => --");
                     
                     for (PFObject *IPObject in objects)
                     {
                         if ([defaults objectForKey:@"firstname"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"firstname"] forKey:@"firstname"];
                         if ([defaults objectForKey:@"lastname"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"lastname"] forKey:@"lastname"];
                         if ([defaults objectForKey:@"email"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"email"] forKey:@"email"];
                         if ([defaults objectForKey:@"phone"] != NULL)
                             [IPObject setObject:[defaults objectForKey:@"phone"] forKey:@"phone"];
                         [IPObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                             if (succeeded)
                             {
                                 DebugLog(@"--Incident_Person update success => --");}
                         }];
                     }
                 }
             }];
            
            if (imageArrayValue != nil && imageArrayValue.count > 0)
            {
                for (int index = 0; index < imageArrayValue.count; index++)
                {
                    SCMediaObject *mediaObject = (SCMediaObject *)[imageArrayValue objectAtIndex:index];
                    DebugLog(@"index %d image %@ object id = %@",index,mediaObject.mediaImage,mediaObject.mediaObjectId);
                    if ([mediaObject.mediaObjectId isEqualToString:@""] && mediaObject.mediaImage != NULL) {
                        DebugLog(@"inserting index %d image %@ object id = %@",index,mediaObject.mediaImage,mediaObject.mediaObjectId);
                        NSData *imageData = UIImagePNGRepresentation(mediaObject.mediaImage);
                        PFFile *imageFile = [PFFile fileWithName:[NSString stringWithFormat:@"SC_%llu.png",(long long)[[NSDate date] timeIntervalSinceReferenceDate]] data:imageData];
                        [imageFile save];
                        
                        PFObject *MObject = [PFObject objectWithClassName:MEDIA_TABLE];
                        [MObject setObject:myObject.objectId forKey:@"incident_id"];
                        [MObject setObject:[NSNumber numberWithInt:1] forKey:@"active"];
                        if (datetimeValue != NULL)
                            [MObject setObject:datetimeValue forKey:@"media_date"];
                        [MObject setObject:imageFile forKey:@"media_link"];
                        [MObject setObject:[NSNumber numberWithInt:1] forKey:@"media_type"];
                        [MObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (succeeded)
                            {
                                DebugLog(@"--Media %d update success => --",index);}
                        }];
                    }
                    mediaObject = nil;
                }
            }
            [SCDatabaseOperations postNotificationWithString:postKey];
        }
        else
        {
            DebugLog(@"--update error => %@--",error);
        }
    }];
}

+(void)deleteRecordWithRelation:(NSString *)tableName objId:(NSString *)lobj sendPostAs:(NSString *)postKey
{
    PFQuery *query = [PFQuery queryWithClassName:INCIDENT_TABLE];
    PFObject *reObj = [query getObjectWithId:lobj];
    [reObj deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--Incident delete success => --");
            PFQuery *IPquery = [PFQuery queryWithClassName:INCIDENTPERSON_TABLE];
            [IPquery whereKey:@"incident_id" equalTo:lobj];
            [IPquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
             {
                 if (!error)
                 {   DebugLog(@"--Got Incident_Person objects to delete  => --");
                     for (PFObject *obj in objects) {
                         [obj deleteInBackground];
                     }
                 }
             }];
            PFQuery *Mquery = [PFQuery queryWithClassName:MEDIA_TABLE];
            [Mquery whereKey:@"incident_id" equalTo:lobj];
            [Mquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
             {
                 if (!error)
                 {
                     DebugLog(@"--Got media objects to delete => --");
                     for (PFObject *obj in objects) {
                         [obj deleteInBackground];
                     }
                 }
             }];
            [SCDatabaseOperations postNotificationWithString:postKey];
        }
        else
        {
            DebugLog(@"--Incident delete error => %@--",error);
        }
    }];
}

+(void)deleteRecordFromTable:(NSString *)tableName objId:(NSString *)lobjId
{
    PFQuery *query = [PFQuery queryWithClassName:tableName];
    PFObject *reObj = [query getObjectWithId:lobjId];
    [reObj deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
        {
            DebugLog(@"--delete success => --");
        }
        else
        {
            DebugLog(@"--delete error => %@--",error);
        }
    }];
}

+ (void)postDropPinNotificationWithData:(PFFile *)tempImageData
{
    DebugLog(@"postDropPinNotificationWithData");
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"image",DROPPINNOTIFICATIONTYPE,tempImageData,MEDIADATA, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:DROPPINNOTIFICATIONNAME object:nil userInfo:dictionary];
}
+ (void)postDropPinNotificationWithArray:(NSArray *)tempImageArray
{
    DebugLog(@"postDropPinNotificationWithData");
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"imageArray",DROPPINNOTIFICATIONTYPE,tempImageArray,MEDIADATA, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:DROPPINNOTIFICATIONNAME object:nil userInfo:dictionary];
}
@end
