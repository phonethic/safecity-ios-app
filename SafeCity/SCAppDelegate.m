//
//  SCAppDelegate.m
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "SCAppDelegate.h"
#import "SCDatabaseOperations.h"
#import "SCViewController.h"
#import "RearViewController.h"
#import "SWRevealViewController.h"
#import "Appirater.h"

#define APPIRATER_APPID @"616709366"
#define FLURRY_SAFECITY_KEY @"T3J5PD2YBTWV6BXKJ8VS"
#define DATABASENAME @"safecity.db"
#define FACEBOOKID_APPID @"472690646129742"
#define FACEBOOKID_APPSecret @"604b85a1e89ff11f9bbbbbe334b3283e"

#define TEST_FLURRY_SAFECITY_KEY @"HRWPHQW8NZVW5YF4T5GN"
NSString *const SCSessionStateChangedNotification = @"com.facebook.SafeCity:MSessionStateChangedNotification";

@interface SCAppDelegate()<SWRevealViewControllerDelegate>
@end

@implementation SCAppDelegate
@synthesize viewController = _viewController;
@synthesize catgArray;
@synthesize databasePath;
@synthesize networkavailable;

#pragma mark application Methods
//- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    NSUInteger orientations = UIInterfaceOrientationMaskAll;
//    
//    if (self.window.rootViewController) {
//        UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
//        orientations = [presented supportedInterfaceOrientations];
//    }
//    return orientations;
//}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self startCheckNetwork];

    // Override point for customization after application launch.
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];

    [SCDatabaseOperations setAppId];

#if TARGET_IPHONE_SIMULATOR
    [SCDatabaseOperations loginUser:@"rishi" password:@"rishi123"];
#endif

    self.catgArray = [[NSMutableArray alloc] initWithArray:[SCDatabaseOperations getAllCategories]];
    
    // Override point for customization after application launch.
    scViewController = [[SCViewController alloc] initWithNibName:@"SCViewController" bundle:nil];
    RearViewController *rearViewController = [[RearViewController alloc] init];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:scViewController];
    navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearViewController frontViewController:navigationController];
    
    mainRevealController.delegate = self;
    mainRevealController.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    
	self.viewController = mainRevealController;
    
   // self.navigationController = [[UINavigationController alloc] initWithRootViewController:scViewController];
    self.window.rootViewController = self.viewController;
    
    [self readAndCreateDatabase];
    
    //Flurry
    [Flurry startSession:FLURRY_SAFECITY_KEY];
    [Flurry logEvent:@"SafeCity_App_Started_Event"];
    
    //Appirater
    [self setAppRateAlert];
    [Appirater appLaunched:YES];
    
    [self.window makeKeyAndVisible];
    return YES;
}

-(void) setAppRateAlert
{
    [Appirater setAppId:APPIRATER_APPID];
    [Appirater setDaysUntilPrompt:2];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:3];
   // [Appirater setDebug:YES];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    DebugLog(@"didRegisterForRemoteNotificationsWithDeviceToken : devicetoken %@",deviceToken);
    [PFPush storeDeviceToken:deviceToken];
    NSString *userid = [NSString stringWithFormat: @"%@", deviceToken];
    userid = [userid stringByReplacingOccurrencesOfString:@"<" withString:@""];
    userid = [userid stringByReplacingOccurrencesOfString:@" " withString:@""];
    userid = [userid stringByReplacingOccurrencesOfString:@">" withString:@""];
    DebugLog(@"--------%@--------------",userid);
    [SCDatabaseOperations loginUser:userid password:userid];
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            DebugLog(@"Successfully subscribed to broadcast channel!");
        else
            DebugLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DebugLog(@"didFailToRegisterForRemoteNotificationsWithError");
    if (error.code == 3010) {
        DebugLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        DebugLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DebugLog(@"didReceiveRemoteNotification  : userInfo %@",userInfo);
    [PFPush handlePush:userInfo];
}
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            //DebugLog(@"success");
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // FBSample logic
            // Once the user has logged in, we want them to be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            //[self showLoginView];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SCSessionStateChangedNotification
                                                        object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions", @"user_photos", nil];
    return [FBSession openActiveSessionWithPermissions:permissions
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                         [self sessionStateChanged:session state:state error:error];
                                     }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    return [FBSession.activeSession handleOpenURL:url];
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DebugLog(@"applicationWillEnterForeground");
    [Appirater appEnteredForeground:YES];
    
    DebugLog(@"self.catgArray.count = %d networkavailable %d",self.catgArray.count,networkavailable);
    if (self.catgArray.count <= 0 && networkavailable)
    {
        [self reloadAllCategoriesIntoLocalTable];
        //self.catgArray = [SCDatabaseOperations getAllCategories];
        DebugLog(@"catgArray %@",catgArray);
    }
    [scViewController setControlsState:networkavailable];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    DebugLog(@"applicationDidBecomeActive");
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession close];

}

- (NSString *)filterHTMLCodes:(NSString *)String
{
    NSArray *filterArray = [NSArray arrayWithObjects: @"&#8211;", @"&#8230;",@"&#8217;", nil];
    for (int i=0; i<filterArray.count; i++) {
        NSRange range = [[String lowercaseString] rangeOfString:[[filterArray objectAtIndex:i] lowercaseString]];
        if ([[filterArray objectAtIndex:i] isEqualToString:@"&#8217;"]) {
            if(range.location != NSNotFound) {
                //Does contain the substring
                return [String stringByReplacingCharactersInRange:range withString:@"'"];
            }
        }
        else
        {
            if(range.location != NSNotFound) {
                //Does contain the substring
                return [String stringByReplacingCharactersInRange:range withString:@""];
            }
        }
    }
    return String;
}
#pragma mark revealController delegate Methods

- (NSString*)stringFromFrontViewPosition:(FrontViewPosition)position
{
    NSString *str = nil;
    if ( position == FrontViewPositionLeft ) str = @"FrontViewPositionLeft";
    if ( position == FrontViewPositionRight ) str = @"FrontViewPositionRight";
    if ( position == FrontViewPositionRightMost ) str = @"FrontViewPositionRightMost";
    if ( position == FrontViewPositionRightMostRemoved ) str = @"FrontViewPositionRightMostRemoved";
    return str;
}
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    DebugLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    DebugLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController willRevealRearViewController:(UIViewController *)rearViewController
{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didRevealRearViewController:(UIViewController *)rearViewController
{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willHideRearViewController:(UIViewController *)rearViewController
{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didHideRearViewController:(UIViewController *)rearViewController
{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willShowFrontViewController:(UIViewController *)rearViewController
{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didShowFrontViewController:(UIViewController *)rearViewController
{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController willHideFrontViewController:(UIViewController *)rearViewController
{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

- (void)revealController:(SWRevealViewController *)revealController didHideFrontViewController:(UIViewController *)rearViewController

{
    DebugLog( @"%@", NSStringFromSelector(_cmd));
}

#pragma mark database Methods
-(void)readAndCreateDatabase{
    DebugLog(@"==========================read and create local database ==========================");
	// Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:DATABASENAME];
    
    DebugLog(@"==========================databasePath %@==========================",databasePath);
	// Execute the "checkAndCreateDatabase" function
	[self checkAndCreateDatabase];
}

-(void) checkAndCreateDatabase{
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
    
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
    
	// If the database already exists then return without doing anything
	if(success)
    {
        DebugLog(@"========================== database exists at path %@==========================",databasePath);
        return;
    }
    else
    {
        DebugLog(@"========================== creating database at path %@==========================",databasePath);
        sqlite3 *safeCityDB;

        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &safeCityDB) == SQLITE_OK)
        {
            DebugLog(@"Database opened successfully");
            char *errMsg;
            
            NSString *sql_Incident = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (objectId text,active INTEGER,address text,category_id text,date DOUBLE,description text,latitude double,longitude double,mode INTEGER,user_id text,verified INTEGER)",INCIDENT_TABLE];
            
            const char *sql_stmt_Incident = [sql_Incident UTF8String];
            
            if (sqlite3_exec(safeCityDB, sql_stmt_Incident, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"success on creating %@ table",INCIDENT_TABLE);
            }
            else
            {
                DebugLog(@"Failed to create table");
            }
            
             NSString *sql_category = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (objectId text,color text,description text,position INTEGER,title text,visible INTEGER)",CATEGORY_TABLE];
            
            const char *sql_stmt_catg = [sql_category UTF8String];
            
            if (sqlite3_exec(safeCityDB, sql_stmt_catg, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"success on creating %@ table",CATEGORY_TABLE);
            }
            else
            {
                DebugLog(@"Failed to create table");
            }
            [self insertAllCategoriesIntoLocalTable];
        }
        else {
            DebugLog(@"Failed to open/create database");
        }
        
        sqlite3_close(safeCityDB);
    }
	// If not then proceed to copy the database from the application to the users filesystem
    
	// Get the path to the database in the application package
//	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    
	// Copy the database from the package to the users filesystem
//	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
}
-(void)insertAllCategoriesIntoLocalTable
{
    sqlite3 *database;
	// Open the database from the users filessytem
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        DebugLog(@"=============== Inserting  rows count %d===============",catgArray.count);
        sqlite3_stmt *compiledStatement;
        for (PFObject *myObject in catgArray)
        {
            //PFObject *myObject = [scObjectsArray objectAtIndex:0];
                       
            DebugLog(@"Before inserting data is :\n table %@ ==> (objectId %@,color %@,description %@,position %d,title %@,visible %d)",INCIDENT_TABLE,myObject.objectId,[myObject objectForKey:@"color"],[myObject objectForKey:@"description"],[[myObject objectForKey:@"position"] intValue],[myObject objectForKey:@"title"],[[myObject objectForKey:@"visible"] intValue]);
            
            /// data base
            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO %@(objectId,color,description,position,title,visible) values(\"%@\",\"%@\",\"%@\",%d,\"%@\",%d)",CATEGORY_TABLE,myObject.objectId,[myObject objectForKey:@"color"],[myObject objectForKey:@"description"],[[myObject objectForKey:@"position"] intValue],[myObject objectForKey:@"title"],[[myObject objectForKey:@"visible"] intValue]];

            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(database, insert_stmt, -1, &compiledStatement, NULL);
            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
            {
                DebugLog(@"=============== Success on inserting row in %@ ================",CATEGORY_TABLE);
                
            } else
            {
                DebugLog(@"=============== Error while inserting row ================ %d",sqlite3_step(compiledStatement));
            }
            // Release the compiled statement from memory
            sqlite3_reset(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}
- (void) deleteAllCategoriesFromLocalTable
{
    DebugLog(@"=============== Deleting  rows ================");
    
    sqlite3_stmt *statement;
    sqlite3 *database;
    
    if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM %@", CATEGORY_TABLE];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"=============== Success on deleting rows ================");
            
        } else
        {
            DebugLog(@"=============== Error while deleting rows ================");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
}

-(void)reloadAllCategoriesIntoLocalTable
{
    DebugLog(@"reloadAllCategoriesIntoLocalTable");
    if (catgArray == nil) {
        catgArray = [[NSMutableArray alloc] init];
    }
    if (networkavailable)
    {
        [self.catgArray removeAllObjects];
        self.catgArray = [NSMutableArray arrayWithArray:[SCDatabaseOperations getAllCategories]];
        DebugLog(@"reloadAllCategoriesIntoLocalTable : catgArray %@",catgArray);
        if (self.catgArray.count > 0) {
            [self deleteAllCategoriesFromLocalTable];
            [self insertAllCategoriesIntoLocalTable];
        }
    }
}
-(NSMutableArray *)getAllCategoriesFromLocalTable
{
    
    NSMutableArray *catgTitleArray = [[NSMutableArray alloc] init];
    sqlite3 *database;
    NSString *tempString = nil;
	// Open the database from the users filessytem
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select title from %@ where visible = 1 order by position", CATEGORY_TABLE];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				tempString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];\
                [catgTitleArray addObject:tempString];
                DebugLog(@"tempString -> %@",tempString);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    return catgTitleArray;
}
-(NSString *)getTitle:(NSString *) objectId
{
    sqlite3 *database;
    NSString *catgTitle = @"";
	// Open the database from the users filessytem
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select title from %@ WHERE objectId=\"%@\"", CATEGORY_TABLE,objectId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				catgTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"catgTitle -> %@",catgTitle);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    return catgTitle;
}
-(NSString *)getCategoryId:(NSString *)catgTitle
{
    sqlite3 *database;
    NSString *catgObjId  = @"";
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select objectId from %@ WHERE title=\"%@\"", CATEGORY_TABLE,catgTitle];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				catgObjId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"objectId -> %@",catgObjId);
            }
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    return catgObjId;
}
#pragma mark resize image Method
- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (NSString *)saveImage: (UIImage*)image
{
    if (image != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filename = [NSString stringWithFormat: @"screen.png"];
        DebugLog(@"filename = %@" , filename);
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:filename];
        DebugLog(@"fullpath = %@" , fullPath);
        
        NSData* data = UIImagePNGRepresentation(image);
        //[data writeToFile:fullPath atomically:YES];
        BOOL success = [fileManager createFileAtPath:fullPath contents:data attributes:nil];
        if(success == YES)
            return filename;
        else
            return @"";
    } else {
        return  @"";
    }
}
- (UIImage *)loadImage:(NSString *)limgName
{
    if(limgName != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent: limgName];
        UIImage* image = [UIImage imageWithContentsOfFile:fullPath];
        return image;
    }
    return nil;
    
}
//removing an image

- (void)removeImage:(NSString *)imageName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:imageName];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES)
        DebugLog(@"image removed");
    else
        DebugLog(@"image NOT removed");
}

-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    { 
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
    [scViewController setControlsState:networkavailable];
}


@end
