//
//  SCHelpViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 08/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCHelpViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *infoImageView;
@end
