//
//  SCAboutViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 15/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SCAboutViewController : UIViewController <MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UITableView *aboutTableView;
@property (strong, nonatomic) IBOutlet UILabel *appVersionLabel;
@end
