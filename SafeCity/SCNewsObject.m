//
//  SCNewsObject.m
//  SafeCity
//
//  Created by Kirti Nikam on 18/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "SCNewsObject.h"

@implementation SCNewsObject
@synthesize newsTitle,newsContent,newsDateTime;
#pragma mark NSCoding Protocol

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.newsTitle forKey:@"newsTitle"];
    [coder encodeObject:self.newsContent forKey:@"newsContent"];
    [coder encodeObject:self.newsDateTime forKey:@"newsDateTime"];
}


- (id)initWithCoder:(NSCoder *)coder {
	self = [super init];
	if (self != nil) {
        self.newsTitle      = [coder decodeObjectForKey:@"newsTitle"];
        self.newsContent    = [coder decodeObjectForKey:@"newsContent"];
        self.newsDateTime   = [coder decodeObjectForKey:@"newsDateTime"];
	}
	return self;
}

@end
