//
//  SCDropPinViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 02/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <Twitter/Twitter.h>
#import <Social/Social.h>
#import <QuartzCore/QuartzCore.h>
#import "SCAppDelegate.h"
#import "SCDatabaseOperations.h"
#import "SCViewController.h"
#import "SCShowListViewController.h"
#import "SCDropPinViewController.h"
#import "SCDetailMapObjectViewController.h"
#import "FacebookViewController.h"
#import "SCMediaObject.h"

#define SHARE_ACTIONSHEET_TITLE @"Share on"
#define ADDIMAGE_ACTIONSHEET_TITLE @"Add a photo"
#define DEFAULT_NUMBEROFPHOTOS 2
@interface SCDropPinViewController ()
- (void)useDropPinNotificationWithString:(NSNotification *)notification;
@end

@implementation SCDropPinViewController
@synthesize dropPinTableView;
@synthesize revAddressValue;
@synthesize geoPointVal;
@synthesize selectedIndexPath;
@synthesize categoryArray;
@synthesize reasonPickerView;
@synthesize datetimePickerView;
@synthesize loadingView;
@synthesize indicator;
@synthesize loadViewLabel;
@synthesize selectedCategory,selectedDate,selectedTime;
@synthesize selectedObj;
@synthesize mode;
@synthesize selectedComments;
@synthesize placehoderImage;
@synthesize MediaImagesArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [Flurry logEvent:@"Drop_Pin_Event"];
    numberOfPhotos = DEFAULT_NUMBEROFPHOTOS;
    if (MediaImagesArray == nil) {
        MediaImagesArray =  [[NSMutableArray alloc]init];
    }
    categoryArray = [[NSMutableArray alloc] initWithArray:[SAFECITY_APP_DELEGATE getAllCategoriesFromLocalTable]];
    DebugLog(@"List : %@",categoryArray);
    
    selectedIndexPath = [[NSIndexPath alloc] init];
    placehoderImage = [UIImage imageNamed:@"placeholder.png"];
    [self addNavBarButton];
    if(mode==VIEW_RECORD)
    {
        NSDate *preDate = [selectedObj objectForKey:@"date"];
        if (preDate == nil)
        {
            preDate = [NSDate date];
        }
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd MMMM yyyy"];
        selectedDate = [dateFormat stringFromDate:preDate];
        [dateFormat setDateFormat:@"HH:mm a"];
        selectedTime = [dateFormat stringFromDate:preDate];
    }
    else
    {
        [self addPickerWithDoneButton];
    }
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.dropPinTableView  addGestureRecognizer:viewTap];
    
    loadingView.backgroundColor = [UIColor blackColor];
    loadingView.layer.cornerRadius = 10;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useDropPinNotificationWithString:)
	 name:DROPPINNOTIFICATIONNAME
	 object:nil];
    
    self.navigationItem.hidesBackButton = TRUE;
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 32)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)viewDidUnload
{
    [self setDropPinTableView:nil];
    [self setReasonPickerView:nil];
    [self setDatetimePickerView:nil];
    [self setLoadingView:nil];
    [self setIndicator:nil];
    [self setLoadViewLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void)tapDetected:(UIGestureRecognizer *)sender {
    
    [self.view endEditing:YES];
    [self scrollTobottom];
}

-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [dropPinTableView setContentOffset:bottomOffset animated:YES];
}

-(void) addNavBarButton
{
    switch (mode) {
        case NEW_RECORD:
        {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveBtnPressed:)];
           // self.navigationItem.rightBarButtonItem.enabled = [SAFECITY_APP_DELEGATE networkavailable];
        }
            break;
        case EDIT_RECORD:
        {
            [dropPinTableView reloadData];
            UIBarButtonItem *updateButton = [[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStyleBordered target:self action:@selector(updateBtnPressed:)];
            //updateButton.enabled = [SAFECITY_APP_DELEGATE networkavailable];
            
            UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction  target:self  action:@selector(shareBtnClicked:)];
            //shareButton.enabled = [SAFECITY_APP_DELEGATE networkavailable];
            self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:shareButton,updateButton, nil];
        }
            break;
        case VIEW_RECORD:
        {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction  target:self  action:@selector(shareBtnClicked:)];
            //self.navigationItem.rightBarButtonItem.enabled = [SAFECITY_APP_DELEGATE networkavailable];
        }
            break;
        default:
            break;
    }
}
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect categoryPickerFrame = CGRectMake(0, 40, 0, 0);
    
    reasonPickerView = [[UIPickerView alloc] initWithFrame:categoryPickerFrame];
    reasonPickerView.showsSelectionIndicator = YES;
    reasonPickerView.dataSource = self;
    reasonPickerView.delegate = self;
    [actionSheet addSubview:reasonPickerView];
    
    CGRect datetimePickerFrame = CGRectMake(0, 40, 0, 0);
    
    datetimePickerView = [[UIDatePicker alloc] initWithFrame:datetimePickerFrame];
    datetimePickerView.timeZone = [NSTimeZone localTimeZone];
    datetimePickerView.maximumDate = [NSDate date];
    [datetimePickerView addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:datetimePickerView];
    
    UISegmentedControl *reloadButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Reload"]];
    reloadButton.momentary = YES;
    reloadButton.frame = CGRectMake(180, 7.0f, 60.0f, 30.0f);
    reloadButton.segmentedControlStyle = UISegmentedControlStyleBar;
    reloadButton.tintColor = [UIColor blackColor];
    reloadButton.tag = 420;
    [reloadButton addTarget:self action:@selector(reloadFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:reloadButton];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    [reasonPickerView reloadAllComponents];
    if([reasonPickerView numberOfRowsInComponent:0] != -1 && categoryArray.count > 0)
    {
        [reasonPickerView selectRow:0 inComponent:0 animated:YES];
        //selectedCategory = [categoryArray objectAtIndex:0];
    }
    if (mode!= NEW_RECORD)
    {
        NSDate *preDate = [selectedObj objectForKey:@"date"];
        if (preDate != nil)
        {
            datetimePickerView.date = preDate;
        }
        else
        {
            datetimePickerView.date = [NSDate date];
        }
    }
    else
    {
        datetimePickerView.date = [NSDate date];
    }
    [self dateChanged:nil];
}
- (void)backBtnPressed:(id)sender
{
    if(mode == VIEW_RECORD || ![SAFECITY_APP_DELEGATE networkavailable])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
        UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
        DebugLog(@"-- image %@ -- ",cellImageView.image);
        
        if ((selectedCategory != NULL && ![selectedCategory isEqualToString:@""]) || (![selectedComments hasPrefix:@"Enter Description"] && ![selectedComments isEqualToString:@""]) || ![placehoderImage isEqual:cellImageView.image])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Unsaved changes.\n Are you sure you want to discard the changes ?" delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
            alert.tag = 1;
            [alert show];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
#pragma alertView delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
            {
                break;
            }
            case 1:
            {
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
                
            default:
                break;
        }
    }
    else if (alertView.tag == 2)
    {
        switch (buttonIndex) {
            case 0:
            {
                break;
            }
            case 1:
            {
                [Flurry logEvent:@"Delete_Pin_Event"];
                DebugLog(@"Delete");
                loadViewLabel.text = @"Deleting data please wait..";
                [self.loadingView setHidden:NO];
                [indicator startAnimating];
                UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
                DebugLog(@"parent %@",previousViewController);
                if ([previousViewController isKindOfClass:[SCViewController class]])
                {
                    [SCDatabaseOperations deleteRecordWithRelation:INCIDENT_TABLE objId:selectedObj.objectId sendPostAs:REFRESHMAP];
                }
                else if ([previousViewController isKindOfClass:[SCShowListViewController class]])
                {
                    [SCDatabaseOperations deleteRecordWithRelation:INCIDENT_TABLE objId:selectedObj.objectId sendPostAs:REFRESHLISTWHENDELETE];
                }
                [self.navigationController popToRootViewControllerAnimated:YES];

                break;
            }
                
            default:
                break;
        }
    }
    if(alertView.tag == 3)
    {
        switch (buttonIndex) {
            case 0:
            {
                break;
            }
            case 1:
            {
                //NSArray *indexPathArray = [NSArray arrayWithObject:indexPath];
                if (MediaImagesArray.count > 0)
                {
                    DebugLog(@"MediaImagesArray %@ count %d",[MediaImagesArray objectAtIndex:selectedRow-1], MediaImagesArray.count);
                    SCMediaObject *tempDObj = (SCMediaObject *)[self.MediaImagesArray objectAtIndex:selectedRow-1];
                    if (![tempDObj.mediaObjectId isEqualToString:@""]) {
                        [SCDatabaseOperations deleteRecordFromTable:MEDIA_TABLE objId:tempDObj.mediaObjectId];
                    }
                    tempDObj = nil;
                    [self.MediaImagesArray removeObjectAtIndex:selectedRow-1];
                }
                //numberOfPhotos = [dropPinTableView numberOfRowsInSection:2] - 1;
                //[self.dropPinTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedRow inSection:2]]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [dropPinTableView beginUpdates];
                [dropPinTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedRow inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
                numberOfPhotos = [dropPinTableView numberOfRowsInSection:2] - 1;
                [dropPinTableView endUpdates];
                break;
            }
                
            default:
                break;
        }
    }

}
- (void)useDropPinNotificationWithString:(NSNotification *)notification //use notification method and logic
{
    DebugLog(@"got useDropPinNotificationWithString notification ");
    
    NSDictionary *dictionary = [notification userInfo];
    if ([[dictionary valueForKey:DROPPINNOTIFICATIONTYPE] isEqualToString:@"image"])
    {
        if([dictionary valueForKey:MEDIADATA] != NULL)
        {
            DebugLog(@"got image notification");
            NSData *tempImageData = [[dictionary valueForKey:MEDIADATA] getData];
            UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
            UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
            cellImageView.image = [UIImage imageWithData:tempImageData];
            
        }
    }
    if ([[dictionary valueForKey:DROPPINNOTIFICATIONTYPE] isEqualToString:@"imageArray"])
    {
        if([dictionary valueForKey:MEDIADATA] != NULL)
        {
            DebugLog(@"got imageArray notification");
            DebugLog(@"number of images %@",[dictionary valueForKey:MEDIADATA]);
            
            [self viewImagesInTable:[dictionary valueForKey:MEDIADATA]];
            
            
        }
    }
    else if (![[dictionary valueForKey:REVERSEADDRESS] isEqualToString:@""])
    {
        DebugLog(@"got REVERSEADDRESS notification");
        revAddressValue = [dictionary valueForKey:REVERSEADDRESS];
        geoPointVal = [dictionary valueForKey:REVERSEADDRESSGEOPOINT];

        UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
        UITextView *textView = (UITextView *) [cell viewWithTag:ADDRESS_TAG];
        textView.text = revAddressValue;
        cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:3]];
        UILabel *geoLabel = (UILabel *)[cell viewWithTag:GEOPOINT_TAG];
        geoLabel.text = [NSString stringWithFormat:@"%f,%f",geoPointVal.latitude,geoPointVal.longitude];
    }
}

# pragma view/edit multpie images
-(void)viewImagesInTable:(NSArray *)imagesArray
{
    DebugLog(@"imagesArray count  %d",imagesArray.count);
    if (imagesArray.count <= 0)
        return;
    int index = 0;
    for (index = 0; index < imagesArray.count; index++){
        
        PFObject *mediaObj = [imagesArray objectAtIndex:index];
        PFFile *tempImage = [mediaObj objectForKey:@"media_link"];
        DebugLog(@"index %d tempImage %@",index,tempImage);
        if ((NSNull *)tempImage != [NSNull null])
        {
            NSData *tempImageData = [tempImage getData];
            SCMediaObject *tempMObj = [[SCMediaObject alloc] init];
            tempMObj.mediaImage = [UIImage imageWithData:tempImageData];
            tempMObj.mediaObjectId = mediaObj.objectId;
            [MediaImagesArray addObject:tempMObj];
            [self addImageInTable:tempMObj.mediaImage];
            tempMObj = nil;
        }
        mediaObj = nil;
    }
}
//- (void)nowBtnClicked:(id)sender
//{
//    NSDate *currentDate = [NSDate date];
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"dd MMMM yyyy"];
//    selectedDate = [dateFormat stringFromDate:pickerDate];
//    [dateFormat setDateFormat:@"HH:mm a"];
//    selectedTime = [dateFormat stringFromDate:pickerDate];
//    NSDate *pickerDate = [dateFormat dateFromString:[NSString stringWithFormat:@""]];
//    datetimePickerView.date = [NSDate date];
//}
//
//- (void)todayBtnClicked:(id)sender
//{
//    datetimePickerView.date = [NSDate date];
//}
- (void)reloadFilterBtnClicked:(id)sender
{
    [SAFECITY_APP_DELEGATE reloadAllCategoriesIntoLocalTable];
    if (categoryArray == nil) {
        categoryArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [categoryArray removeAllObjects];
    }
    [categoryArray addObjectsFromArray:[SAFECITY_APP_DELEGATE getAllCategoriesFromLocalTable]];
    [reasonPickerView reloadAllComponents];
}
- (void)doneFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    DebugLog(@"selectedCategory %@,%d",selectedCategory,CURRENTCASE);
        switch (CURRENTCASE) {
            case REASON_TAG:
                {
                    if(selectedCategory == NULL)
                    {
                        selectedCategory = [categoryArray objectAtIndex:0];
                       // selectedCategoryId = selectedCategory.objectId;
                    }
                    UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:selectedIndexPath];
                    UITextField *categoryTextField = (UITextField *)[cell viewWithTag:REASON_TAG];
                    //categoryTextField.text = [selectedCategory objectForKey:@"title"];
                    categoryTextField.text = selectedCategory;
                }
                break;
            case DATE_TAG:
            {
                UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:selectedIndexPath];
                UITextField *dateTextField = (UITextField *)[cell viewWithTag:DATE_TAG];
                dateTextField.text = selectedDate;
            }
                break;
            case TIME_TAG:
            {
                UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:selectedIndexPath];
                UITextField *timeTextField = (UITextField *)[cell viewWithTag:TIME_TAG];
                timeTextField.text = selectedTime;
            }
                break;
            default:
                break;
        }
    CURRENTCASE = 0;
}

- (void)cancelFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    CURRENTCASE = 0;
}

- (void) dateChanged:(id)sender
{
    NSDate *pickerDate = [datetimePickerView date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    selectedDate = [dateFormat stringFromDate:pickerDate];
    [dateFormat setDateFormat:@"HH:mm a"];
    selectedTime = [dateFormat stringFromDate:pickerDate];
}
- (void)saveBtnPressed:(id)sender
{
    if(![SAFECITY_APP_DELEGATE networkavailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([selectedCategory isEqualToString:@""] || selectedCategory == NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please select category" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [Flurry logEvent:@"Save_Pin_Event"];
    loadViewLabel.text = @"Saving data please wait..";
    [self.loadingView setHidden:NO];
    [indicator startAnimating];
    DebugLog(@"dropPin: saveBtnPressed");
    
    NSString *selectedCategoryId = [SAFECITY_APP_DELEGATE getCategoryId:selectedCategory];

//    UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
//    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
//    cell = nil;
    
    DebugLog(@"geopt %@ -- address %@ -- reason %@ -- comments %@ -- datetime %@ ",geoPointVal,revAddressValue,selectedCategoryId,selectedComments,[datetimePickerView date]);
    
    if ([selectedComments hasPrefix:@"Enter Description"])
    {
        [SCDatabaseOperations addRecordWithRelation:INCIDENT_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:@"" imageArray:MediaImagesArray  datetime:[datetimePickerView date]];
    }
    else
    {
        [SCDatabaseOperations addRecordWithRelation:INCIDENT_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:selectedComments imageArray:MediaImagesArray datetime:[datetimePickerView date]];
    }
    
    /*previous code
    if ([placehoderImage isEqual:cellImageView.image])
    {
        if ([selectedComments hasPrefix:@"Enter Description"])
        {
            // [SCDatabaseOperations addRecord:LOCATION_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategory comments:@"" image:cellImageView.image datetime:[datetimePickerView date]];
            [SCDatabaseOperations addRecordWithRelation:INCIDENT_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:@"" image:NULL datetime:[datetimePickerView date]];
        }
        else
        {
            //  [SCDatabaseOperations addRecord:LOCATION_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategory comments:selectedComments image:cellImageView.image datetime:[datetimePickerView date]];
            [SCDatabaseOperations addRecordWithRelation:INCIDENT_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:selectedComments image:NULL datetime:[datetimePickerView date]];
        }
    }
    else
    {
        if ([selectedComments hasPrefix:@"Enter Description"] || [selectedComments isEqualToString:@""])
        {
            // [SCDatabaseOperations addRecord:LOCATION_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategory comments:@"" image:cellImageView.image datetime:[datetimePickerView date]];
            [SCDatabaseOperations addRecordWithRelation:INCIDENT_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:@"" image:cellImageView.image datetime:[datetimePickerView date]];
        }
        else
        {
            //  [SCDatabaseOperations addRecord:LOCATION_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategory comments:selectedComments image:cellImageView.image datetime:[datetimePickerView date]];
            [SCDatabaseOperations addRecordWithRelation:INCIDENT_TABLE loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:selectedComments image:cellImageView.image datetime:[datetimePickerView date]];
        }
    }
    */
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)updateBtnPressed:(id)sender
{
    if(![SAFECITY_APP_DELEGATE networkavailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([selectedCategory isEqualToString:@""] || selectedCategory == NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please select category" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [Flurry logEvent:@"Update_Pin_Event"];
    loadViewLabel.text = @"Updating data please wait..";
    [self.loadingView setHidden:NO];
    [indicator startAnimating];
    DebugLog(@"dropPin: updateBtnPressed");
    
    NSString *selectedCategoryId = [SAFECITY_APP_DELEGATE getCategoryId:selectedCategory];

//    UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
//    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
//    cell = nil;
    
    DebugLog(@"objectid %@ geopt %@ -- address %@ -- reason %@ -- comments %@ -- datetime %@ ",selectedObj.objectId,geoPointVal,revAddressValue,selectedCategoryId,selectedComments,[datetimePickerView date]);
    
    
    NSString *refreshString = nil;
    UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    DebugLog(@"parent %@",previousViewController);
    if ([previousViewController isKindOfClass:[SCViewController class]])
    {
        refreshString = REFRESHMAP;
    }
    else if ([previousViewController isKindOfClass:[SCShowListViewController class]])
    {
        refreshString = REFRESHLISTWHENUPDATE;
    }
    
    if ([selectedComments hasPrefix:@"Enter Description"] || [selectedComments isEqualToString:@""])
    {
       //[SCDatabaseOperations updateRecord:LOCATION_TABLE objId:selectedObj.objectId  loc:geoPointVal address:revAddressValue reason:selectedCategory comments:@"" image:cellImageView.image datetime:[datetimePickerView date]];
        
//        [SCDatabaseOperations updateRecordWithRelation:INCIDENT_TABLE objId:selectedObj.objectId  loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:@"" image:cellImageView.image datetime:[datetimePickerView date] sendPostAs:refreshString];
        [SCDatabaseOperations updateRecordWithRelation:INCIDENT_TABLE objId:selectedObj.objectId  loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:@"" imageArray:MediaImagesArray datetime:[datetimePickerView date] sendPostAs:refreshString];
    }
    else
    {
       //[SCDatabaseOperations updateRecord:LOCATION_TABLE objId:selectedObj.objectId  loc:geoPointVal address:revAddressValue reason:selectedCategory comments:selectedComments image:cellImageView.image datetime:[datetimePickerView date]];
         [SCDatabaseOperations updateRecordWithRelation:INCIDENT_TABLE objId:selectedObj.objectId  loc:geoPointVal address:revAddressValue reason:selectedCategoryId comments:selectedComments imageArray:MediaImagesArray datetime:[datetimePickerView date] sendPostAs:refreshString];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)shareBtnClicked:(id)sender
{
    DebugLog(@"share btn clicked");
    if ([MFMessageComposeViewController canSendText]) {
        UIActionSheet *shareActionSheet= [[UIActionSheet alloc]
                                          initWithTitle: @""
                                          delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          destructiveButtonTitle:nil
                                          otherButtonTitles:@"Facebook",@"Twitter",@"Email",@"SMS", nil];
        shareActionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        shareActionSheet.title = SHARE_ACTIONSHEET_TITLE;
        [shareActionSheet showInView:self.navigationController.view];
    }
    else
    {
        UIActionSheet *shareActionSheet= [[UIActionSheet alloc]
                                        initWithTitle: @""
                                        delegate:self
                                        cancelButtonTitle:@"CANCEL"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];
        shareActionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        shareActionSheet.title = SHARE_ACTIONSHEET_TITLE;
        [shareActionSheet showInView:self.navigationController.view];
    }
}

-(void)deletePin:(id)sender
{
    if([SAFECITY_APP_DELEGATE networkavailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Are you sure you want to delete this incident report ?" delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        alert.tag = 2;
        [alert show];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}

-(void)showAddImageActionSheet
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                        initWithTitle: @""
                                        delegate:self
                                        cancelButtonTitle:@"CANCEL"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"Take Photo",@"Choose Existing Photo", nil];
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        uiActionSheetP.title = ADDIMAGE_ACTIONSHEET_TITLE;
        [uiActionSheetP showInView:self.navigationController.view];
    }
    else
    {
        UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                        initWithTitle: @""
                                        delegate:self
                                        cancelButtonTitle:@"CANCEL"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"Choose Existing Photo", nil];
        uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        uiActionSheetP.title = ADDIMAGE_ACTIONSHEET_TITLE;
        [uiActionSheetP showInView:self.navigationController.view];
    }
}
-(void)actionSheet:(UIActionSheet *)localactionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"%d",buttonIndex);

    if ([localactionSheet.title isEqualToString:ADDIMAGE_ACTIONSHEET_TITLE])
    {
        if (buttonIndex == [localactionSheet numberOfButtons]-1)
            return;
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Take Photo"])
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] && [[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Choose Existing Photo"])
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
    }
    else if ([localactionSheet.title isEqualToString:SHARE_ACTIONSHEET_TITLE])
    {
        DebugLog(@"share actionsheet callback with [localactionSheet numberOfButtons] %d",[localactionSheet numberOfButtons]);
        if (buttonIndex == [localactionSheet numberOfButtons]-1)
            return;
        else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Facebook"])
        {
            DebugLog(@"facebook");
            if([SAFECITY_APP_DELEGATE networkavailable]) {
                [self facebookSharePressed];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }
        else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Twitter"])
        {
            DebugLog(@"twitter");
            [self twitterSharePressed];
        }
        else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Email"])
        {
            DebugLog(@"email");
            [self emailSharePressed];
        }
        else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"SMS"])
        {
            DebugLog(@"sms");
            [self smsSharePressed];
        }
    }
}
-(void)facebookSharePressed
{
    if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
    {
        DebugLog(@"Got < ios 6");
        FacebookViewController *facebookViewComposer = [[FacebookViewController alloc] initWithNibName:@"FacebookViewController" bundle:nil];
        facebookViewComposer.title = @"FACEBOOK";
        facebookViewComposer.postTitle = ALERTTITLE;
        facebookViewComposer.postmessageBody = [self getMessageBody:@"facebook"];
        facebookViewComposer.imageAttachments = MediaImagesArray;//[self getImageAttachment];
        facebookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self.navigationController presentModalViewController:facebookViewComposer animated:YES];
    }
    else
    {
        DebugLog(@"Got ios 6");
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *slFBComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [slFBComposer setInitialText:[NSString stringWithFormat:@"%@\n%@",ALERTTITLE,[self getMessageBody:@"facebook"]]];
                [slFBComposer setTitle:ALERTTITLE];
//                UIImage *imageAttachment= [self getImageAttachment];
//                if (imageAttachment != nil)
//                {
//                    [slFBComposer addImage:imageAttachment];
//                }
                for (int index = 0; index < MediaImagesArray.count; index++)
                {
                    SCMediaObject *mediaObject = (SCMediaObject *)[MediaImagesArray objectAtIndex:index];
                    DebugLog(@"index %d image %@ object id = %@",index,mediaObject.mediaImage,mediaObject.mediaObjectId);
                    if (mediaObject.mediaImage != NULL)
                    {
                        [slFBComposer addImage:mediaObject.mediaImage];
                    }
                    mediaObject = nil;
                }
                [self presentViewController:slFBComposer animated:YES completion:nil];
//                slFBComposer.completionHandler = ^(SLComposeViewControllerResult result)
//                {
//                    if (result == SLComposeViewControllerResultCancelled) {
//                        DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
//                    }
//                    else if (result == SLComposeViewControllerResultDone) {
//                        DebugLog(@"User has finished composing the post, and tapped the send button");
//                    }
//                };
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:ALERTTITLE
                                          message:@"You may not have set up facebook service on your device or You may not connected to internet.\nPlease check and try again."
                                          delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
                [alertView show];
            }
    }
}

-(void)twitterSharePressed
{
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetComposer = [[TWTweetComposeViewController alloc] init];
        [tweetComposer setInitialText:[self getMessageBody:@"twitter"]];
//        UIImage *imageAttachment= [self getImageAttachment];
//        if (imageAttachment != nil)
//        {
//            [tweetComposer addImage:imageAttachment];
//        }
        for (int index = 0; index < MediaImagesArray.count; index++)
        {
            SCMediaObject *mediaObject = (SCMediaObject *)[MediaImagesArray objectAtIndex:index];
            DebugLog(@"index %d image %@ object id = %@",index,mediaObject.mediaImage,mediaObject.mediaObjectId);
            if (mediaObject.mediaImage != NULL)
            {
                [tweetComposer addImage:mediaObject.mediaImage];
            }
            mediaObject = nil;
        }
        [self presentModalViewController:tweetComposer animated:YES];
//        tweetComposer.completionHandler = ^(SLComposeViewControllerResult result) {
//            if (result == SLComposeViewControllerResultCancelled) {
//                DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
//            }
//            
//            if (result == SLComposeViewControllerResultDone) {
//                DebugLog(@"User has finished composing the post, and tapped the send button");
//            }
//        };
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERTTITLE
                                  message:@"You may not have set up twitter service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }

}

-(void)emailSharePressed
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        mailComposer.navigationBar.tintColor = DEFAULT_COLOR;
        [mailComposer setSubject:@""];
        [mailComposer setToRecipients:nil];
        [mailComposer setMessageBody:[NSString stringWithFormat:@"%@\n%@",ALERTTITLE,[self getMessageBody:@"email"]] isHTML:NO];
//        UIImage *imageAttachment= [self getImageAttachment];
//        if (imageAttachment != nil)
//        {
//            NSData *data = UIImagePNGRepresentation(imageAttachment);
//            [mailComposer addAttachmentData:data mimeType:@"image/png" fileName:@"screenshot.png"];
//        }
        for (int index = 0; index < MediaImagesArray.count; index++)
        {
            SCMediaObject *mediaObject = (SCMediaObject *)[MediaImagesArray objectAtIndex:index];
            DebugLog(@"index %d image %@ object id = %@",index,mediaObject.mediaImage,mediaObject.mediaObjectId);
            if (mediaObject.mediaImage != NULL)
            {
                NSData *data = UIImagePNGRepresentation(mediaObject.mediaImage);
                [mailComposer addAttachmentData:data mimeType:@"image/png" fileName:[NSString stringWithFormat:@"screenshot_%d.png",index]];
            }
            mediaObject = nil;
        }
        [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
        [self presentModalViewController:mailComposer animated:YES];
    }
}
-(void)smsSharePressed
{
    if ([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *smsComposer = [[MFMessageComposeViewController alloc] init];
        smsComposer.messageComposeDelegate = self;
        smsComposer.recipients = nil; // [NSArray arrayWithObjects:@"987654321",nil];
        smsComposer.body = [NSString stringWithFormat:@"%@\n%@",ALERTTITLE,[self getMessageBody:@"sms"]];
        [self presentModalViewController:smsComposer animated:YES];
    }
}

-(NSMutableString *)getMessageBody:(NSString *)forType
{
    //DebugLog(@"Reason %@, comments %@ lat,long %f,%f, reveraddress %@ date %@ time %@",[SCDatabaseOperations getTitle:selectedCategoryId categoryArray:categoryArray],selectedComments,geoPointVal.latitude,geoPointVal.longitude,revAddressValue,selectedDate,selectedTime);
    DebugLog(@"Reason %@, comments %@ lat,long %f,%f, reveraddress %@ date %@ time %@",selectedCategory,selectedComments,geoPointVal.latitude,geoPointVal.longitude,revAddressValue,selectedDate,selectedTime);

    NSMutableString *messageBody = [[NSMutableString alloc] init];
    if (revAddressValue == nil)
    {
        [messageBody appendString:[NSString stringWithFormat:@"Latitiude and Longitude: %f,%f\n",geoPointVal.latitude,geoPointVal.longitude]];
    }
    else
    {
        [messageBody appendString:[NSString stringWithFormat:@"Address : %@\n",revAddressValue]];
    }
    
    if ([forType isEqualToString:@"twitter"])  //twitter maxlength = 140 
    {
        [messageBody appendString:[NSString stringWithFormat:@"Date: %@ %@\n",selectedDate,selectedTime]];
    }
    else
    {
        
        if ([selectedComments hasPrefix:@"Enter Description"] || [selectedComments isEqualToString:@""])
        {
           // [messageBody appendString:[NSString stringWithFormat:@"Reason: %@\nDate: %@ %@\n",[SCDatabaseOperations getTitle:selectedCategoryId categoryArray:categoryArray],selectedDate,selectedTime]];
            [messageBody appendString:[NSString stringWithFormat:@"Reason: %@\nDate: %@ %@\n",selectedCategory,selectedDate,selectedTime]];
        }
        else
        {
            //[messageBody appendString:[NSString stringWithFormat:@"Reason: %@\nDescription: %@\nDate: %@ %@\n",[SCDatabaseOperations getTitle:selectedCategoryId categoryArray:categoryArray],selectedComments,selectedDate,selectedTime]];
            [messageBody appendString:[NSString stringWithFormat:@"Reason: %@\nDescription: %@\nDate: %@ %@\n",selectedCategory,selectedComments,selectedDate,selectedTime]];
        }
    }
    DebugLog(@"messageBody %@ and its length = %d",messageBody,messageBody.length);
    return messageBody;
}
-(UIImage *)getImageAttachment
{
    UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
    cell = nil;
    if (![placehoderImage isEqual:cellImageView.image])
    {
        return cellImageView.image;
    }
    return nil;
}
#pragma mark MFMailComposeViewController Delegate Methods
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    DebugLog(@"Mail Result %u",result);
    switch (result)
    {
        case MFMailComposeResultCancelled :
            DebugLog(@"Mail Cancelled");
            break;
        case MFMailComposeResultSaved :
            DebugLog(@"Mail Saved");
            break;
        case MFMailComposeResultSent :
            DebugLog(@"Mail Sent");
            break;
        case MFMailComposeResultFailed :
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Unknown Error\nMail failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
            break;
        default:
            break;
    }
    [controller dismissModalViewControllerAnimated:YES];
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
            DebugLog(@"SMS Sent");
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIImagePickerController Delegate Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        DebugLog(@"originalImage %@",originalImage);
        if (originalImage != nil)
        {
            //resizing image
            UIImage * image = [SAFECITY_APP_DELEGATE resizeImage:originalImage newSize:CGSizeMake(100, 100)];
            [SAFECITY_APP_DELEGATE saveImage:image];
            
            //adding image in table
            SCMediaObject *tempMObj = [[SCMediaObject alloc] init];
            tempMObj.mediaImage = image;
            tempMObj.mediaObjectId = @"";
            [MediaImagesArray addObject:tempMObj];
            tempMObj = nil;
            [self addImageInTable:image];
            
            //            if(self.imgName == nil) {
            //                DebugLog(@"image name is empty");
            //            } else
            //            {
            //                DebugLog(@"image name is not empty");
            //            }
        }
        [self dismissModalViewControllerAnimated:YES];

    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message: @"Failed to save image" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

# pragma add multpie images
-(void)addImageInTable:(UIImage *)newImage
{
    DebugLog(@"-------------------------- addImageInTable --------------------------");
    DebugLog(@"-------------------------- MediaImagesArray %@--------------------------",MediaImagesArray);
    NSInteger numberOfRowsINSection = [dropPinTableView numberOfRowsInSection:2];
    DebugLog(@"number of rows %d",numberOfRowsINSection);
    
    BOOL isRowEmptyForNewImage = YES;
    int index = 1;
    for (index=1; index<numberOfRowsINSection; index++)
    {
        UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:2]];
        DebugLog(@"cell %@",cell);
        if(cell == NULL)
            [dropPinTableView reloadInputViews];
        
        UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG + index - 1];
        DebugLog(@"index %d IMAGE_TAG %d",index,IMAGE_TAG + index - 1);
        if ([placehoderImage isEqual:cellImageView.image])
        {
            cellImageView.image = newImage;
            isRowEmptyForNewImage = NO;
        }
        else
        {
            DebugLog(@"already image is added index %d IMAGE_TAG %d",index,IMAGE_TAG + index - 1);
        }
    }
    DebugLog(@"now index %d",index);
    if (isRowEmptyForNewImage)
    {
        DebugLog(@"add new row %d", index);
        
        [dropPinTableView beginUpdates];
        [dropPinTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
        numberOfPhotos = numberOfRowsINSection+1;
       [dropPinTableView endUpdates];
        DebugLog(@"numberOfPhotos %d",numberOfPhotos);
        
//        UITableViewCell *cell = [dropPinTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:2]];
//        //        if(cell == NULL)
//        //            [dropPinTableView reloadInputViews];
//        DebugLog(@"new IMAGE_TAG %d",IMAGE_TAG + index - 1);
//
//        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 280, 80)];
//        thumbImg.tag = IMAGE_TAG + index - 1;
//        thumbImg.image = newImage;
//        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
//        thumbImg.layer.cornerRadius = 10;
//        thumbImg.layer.masksToBounds = YES;
//        thumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        thumbImg.layer.borderWidth = 1.0;
//        [cell.contentView addSubview:thumbImg];
      //[dropPinTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
  //      [dropPinTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
//        UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG];
//        cellImageView.image = newImage;
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(mode == NEW_RECORD || mode == VIEW_RECORD)
        return 5;
    else
        return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self getRowsHeight:indexPath.section row:indexPath.row];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getRowsCount:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row] ;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        DebugLog(@"id = %@",CellIdentifier);
        switch (indexPath.section) {
            case 0:
            {
                UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 250.0f, 31.0f)];
                if (mode==NEW_RECORD)
                {
                    textField.placeholder = @"Select a category";
                    [self setTextFieldProperties:textField text:@""];
                }
                else
                {
                   // DebugLog(@"%@, %@",[selectedObj objectForKey:@"category_id"],[SCDatabaseOperations getTitle:[selectedObj objectForKey:@"category_id"] categoryArray:categoryArray]);
                    DebugLog(@"%@, %@",[selectedObj objectForKey:@"category_id"],[SAFECITY_APP_DELEGATE getTitle:[selectedObj objectForKey:@"category_id"]]);
                    selectedCategory = [SAFECITY_APP_DELEGATE getTitle:[selectedObj objectForKey:@"category_id"]];
                  //  [self setTextFieldProperties:textField text:[SCDatabaseOperations getTitle:selectedCategoryId categoryArray:categoryArray]];
                    [self setTextFieldProperties:textField text:selectedCategory];
                }
                //[selectedCategory fetchIfNeeded];
                textField.tag = REASON_TAG;
                [cell.contentView addSubview:textField];
                if (mode != VIEW_RECORD)
                {
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                }
                cell.imageView.image = [UIImage imageNamed:@"category.png"];
            }
                break;
            case 1:
            {
                //-(UITextView *)setTextViewProperties:(UITextView *)textView text:(NSString *)textString
                DebugLog(@"comments sec %d row %d tag %d",indexPath.section,indexPath.row,DESCRIPTION_TAG);
                UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 250.0f, 80.0f)];
                if (mode==NEW_RECORD)
                {
                   selectedComments = @"Enter Description";
                    textView.textColor = [UIColor lightGrayColor];
                }
                else
                    selectedComments = [selectedObj objectForKey:@"description"];
                textView.tag = DESCRIPTION_TAG;
                [self setTextViewProperties:textView text:selectedComments];
                [cell.contentView addSubview:textView];
                cell.imageView.image = [UIImage imageNamed:@"description.png"];
            }
                break;
                
            case 2:
            {
                if(indexPath.row==0) {
                    UILabel *addphotoLabel = [[UILabel alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 250.0f, 31.0f)];
                    addphotoLabel.tag = ADD_IMAGE_LABEL_TAG;
                    if (mode == VIEW_RECORD)
                        addphotoLabel.text = @"View a photo";
                    else
                        addphotoLabel.text = @"Add a photo";
                    addphotoLabel.font = [UIFont systemFontOfSize:14.0f];
                    addphotoLabel.textAlignment = UITextAlignmentLeft;
                    addphotoLabel.textColor = [UIColor blackColor];
                    addphotoLabel.backgroundColor =  [UIColor clearColor];
                    [cell.contentView addSubview:addphotoLabel];
                    if (mode != VIEW_RECORD)
                    {
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    }
                    cell.imageView.image = [UIImage imageNamed:@"picture.png"];
                    
                }
                else
                {
                    DebugLog(@"cell identifier %@",CellIdentifier);
                    UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 280, 80)];
                    thumbImg.tag = IMAGE_TAG + indexPath.row - 1;
                    DebugLog(@"index %d tag %d",indexPath.row,thumbImg.tag);
                    thumbImg.image = placehoderImage;
                    //[SCDatabaseOperations getImageFile:selectedObj];
                    DebugLog(@"--------- mediaimageArray %@ ",MediaImagesArray);
                    if (mode != NEW_RECORD && MediaImagesArray.count == 0)
                    {
                             DebugLog(@"+++++++++++++-\n\n requesting parse files \n\n +++++++++++++-\n\n");
                            [SCDatabaseOperations getImageFiles:selectedObj];
                    }
                    else if (MediaImagesArray.count > 0)
                    {
                           // [self addImageInTable:[MediaImagesArray objectAtIndex:indexPath.row - 1]];
                            DebugLog(@"+++++++++++++-\n\n adding images from media array \n\n +++++++++++++-\n\n");
                            SCMediaObject *tempObj = (SCMediaObject *)[MediaImagesArray objectAtIndex:indexPath.row - 1];
                            thumbImg.image = tempObj.mediaImage;
                    }
                    thumbImg.contentMode = UIViewContentModeScaleAspectFit;
                    thumbImg.layer.cornerRadius = 10;
                    thumbImg.layer.masksToBounds = YES;
                    thumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
                    thumbImg.layer.borderWidth = 1.0;
                    [cell.contentView addSubview:thumbImg];
                }
            }
                break;
                
            case 3:
            {
                if(indexPath.row==0) {
                    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 250.0f, 80.0f)];
                    if (mode!= NEW_RECORD)
                        revAddressValue = [selectedObj objectForKey:@"address"];
                    textView.tag = ADDRESS_TAG;
                    [self setTextViewProperties:textView text:revAddressValue];
                    [cell.contentView addSubview:textView];
                    cell.imageView.image = [UIImage imageNamed:@"map_black.png"];
                } else {
                    UILabel *geoLabel = [[UILabel alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 250.0f, 31.0f)];
                    geoLabel.tag = GEOPOINT_TAG;
                    if (mode!= NEW_RECORD)
                        geoPointVal = [selectedObj objectForKey:@"location"];
                    geoLabel.text = [NSString stringWithFormat:@"%f,%f",geoPointVal.latitude,geoPointVal.longitude];
                    geoLabel.font = [UIFont systemFontOfSize:14.0f];
                    geoLabel.textAlignment = UITextAlignmentLeft;
                    geoLabel.textColor = [UIColor blackColor];
                    geoLabel.backgroundColor =  [UIColor clearColor];
                    [cell.contentView addSubview:geoLabel];
                    if (mode != VIEW_RECORD)
                    {
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    }
                    cell.imageView.image = [UIImage imageNamed:@"pin.png"];
                }
            }
                break;

            case 4:
            {
                if(indexPath.row==0)
                {
                    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 250.0f, 31.0f)];
                    textField.tag = DATE_TAG;
                    [self setTextFieldProperties:textField text:selectedDate];
                    [cell.contentView addSubview:textField];
                    if (mode != VIEW_RECORD)
                    {
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    }
                    cell.imageView.image = [UIImage imageNamed:@"calendar.png"];
                }
                else
                {
                    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 250.0f, 31.0f)];
                    textField.tag = TIME_TAG;
                    [self setTextFieldProperties:textField text:selectedTime];
                    [cell.contentView addSubview:textField];
                    if (mode != VIEW_RECORD)
                    {
                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    }
                    cell.imageView.image = [UIImage imageNamed:@"time.png"];
                    
                }
            }
                break;
            case 5:
            {
                UIImage *image = [[UIImage imageNamed:@"delete_red.png"]
                                  stretchableImageWithLeftCapWidth:8 topCapHeight:8];
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [button setBackgroundImage:image forState:UIControlStateNormal];
                [button setFrame:CGRectMake(0, 0, 300, 44)];
                [button setTitle:@"Remove" forState:UIControlStateNormal];
                [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [button addTarget:self action:@selector(deletePin:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:button];
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
        DebugLog(@"in else of cellForIndex : => sec %d row %d",indexPath.section,indexPath.row);
        switch (indexPath.section) {
            case 0:
            {
                UITextField *categoryTextField = (UITextField *) [cell viewWithTag:REASON_TAG];
                //categoryTextField.text = [SCDatabaseOperations getTitle:selectedCategoryId categoryArray:categoryArray];
                categoryTextField.text = selectedCategory;
            }
                break;
            case 1:
            {
                UITextView *textView = (UITextView *) [cell viewWithTag:DESCRIPTION_TAG];
                textView.text = selectedComments;
            }
                break;
            case 2:
            {
                if (indexPath.row != 0 && MediaImagesArray.count > 0) {
                    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:IMAGE_TAG + indexPath.row - 1];
                    SCMediaObject *tempObj = (SCMediaObject *)[MediaImagesArray objectAtIndex:indexPath.row - 1];
                    cellImageView.image = tempObj.mediaImage;
                }
            }
                break;
            case 3:
                {
                    if(indexPath.row==0)
                    {
                        UITextView *textView = (UITextView *) [cell viewWithTag:ADDRESS_TAG];
                        textView.text = revAddressValue;
                    } else
                    {
                        UILabel *geoLabel = (UILabel *)[cell viewWithTag:GEOPOINT_TAG];
                        geoLabel.text = [NSString stringWithFormat:@"%f,%f",geoPointVal.latitude,geoPointVal.longitude];
                    }
                }
                break;
                
            case 4:
                {
                    if(indexPath.row==0) {
                        UITextField *dateTextField = (UITextField *) [cell viewWithTag:DATE_TAG];
                        dateTextField.text = selectedDate;
                    } else {
                        UITextField *timeTextField = (UITextField *) [cell viewWithTag:TIME_TAG];
                        timeTextField.text = selectedTime;
                    }
                }
                break;
            default:
                break;
            }

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    UILabel * lblTitle = (UILabel *)[cell viewWithTag:2];
    //lblTitle.textColor = [UIColor yellowColor];
    [dropPinTableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedIndexPath= [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    DebugLog(@"current case %d",CURRENTCASE);
    switch (indexPath.section) {
        case 0:
            {
                DebugLog(@"Reason : %@",categoryArray);
                CURRENTCASE = REASON_TAG;
                [self showPicker:REASON_TAG];
            }
            break;
        case 1:
            {
                DebugLog(@"Description");
            }
            break;
        case 2:
            {
                if(indexPath.row==0)
                {
                    DebugLog(@"Add Photo");
                    selectedIndexPath= [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
                    if (mode != VIEW_RECORD) {
                        
                        if (MediaImagesArray.count == 4)
                        {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message: @"You can't add more than four images" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alert show];
                        }
                        else
                        {
                            [self showAddImageActionSheet];
                        }
                    }
                }
            }
            break;
        case 3:
            {
                if(indexPath.row==1)
                {
                    DebugLog(@"Location");
                    SCDetailMapObjectViewController *scdetailviewController = [[SCDetailMapObjectViewController alloc] initWithNibName:@"SCDetailMapObjectViewController" bundle:nil];
                    scdetailviewController.geoPoint = geoPointVal;
                    scdetailviewController.reverseAddress = revAddressValue;
                    scdetailviewController.mode = mode;
                    [self.navigationController pushViewController:scdetailviewController animated:YES];
                }
            }
            break;
        case 4:
            {
                if(indexPath.row==0)
                {
                    DebugLog(@"Date");
                    CURRENTCASE = DATE_TAG;
                    [self showPicker:DATE_TAG];
                } else {
                    DebugLog(@"Time");
                    CURRENTCASE = TIME_TAG;
                    [self showPicker:TIME_TAG];
                }
            }
            break;
            
        case 5:
        {
            DebugLog(@"Delete");
        }
            break;

        default:
            break;
    }
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES or NO
    if (indexPath.section == 2 && mode!= VIEW_RECORD && indexPath.row != 0) {
        return YES;
    }
    return NO;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
  //  NSArray *indexPathArray = [NSArray arrayWithObject:indexPath];
 //   DebugLog(@"indexArray %@",indexPathArray);
    DebugLog(@"commitEditingStyle curent row %d",indexPath.row);
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if([SAFECITY_APP_DELEGATE networkavailable])
        {
            selectedRow = indexPath.row;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Are you sure you want to delete this  image? If you delete this image then you can't recover it again." delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
            alert.tag = 3;
            [alert show];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

-(void)showPicker:(int) pickerType
{
    if (mode != VIEW_RECORD) {
        switch (pickerType) {
            case REASON_TAG:
                {
                    [reasonPickerView setHidden:NO];
                    [datetimePickerView setHidden:YES];
                }
                break;
            case DATE_TAG:
                {
                    [reasonPickerView setHidden:YES];
                    [datetimePickerView setHidden:NO];
                     datetimePickerView.datePickerMode = UIDatePickerModeDate;
                }
                break;
            case TIME_TAG:
                {
                    [reasonPickerView setHidden:YES];
                    [datetimePickerView setHidden:NO];
                     datetimePickerView.datePickerMode = UIDatePickerModeTime;
                }
                break;
            default:
                return;
        }
        if (datetimePickerView.hidden) {
            
            UISegmentedControl *reloadButton = (UISegmentedControl *)[actionSheet viewWithTag:420];
            [reloadButton setTitle:@"Reload" forSegmentAtIndex:0];
            [reloadButton addTarget:self action:@selector(reloadFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
        }
        else
        {
            UISegmentedControl *todayButton = (UISegmentedControl *)[actionSheet viewWithTag:420];
            todayButton.hidden = TRUE;
//            if (CURRENTCASE == DATE_TAG) {
//                UISegmentedControl *todayButton = (UISegmentedControl *)[actionSheet viewWithTag:420];
//                [todayButton setTitle:@"Today" forSegmentAtIndex:0];
//                [todayButton addTarget:self action:@selector(todayBtnClicked:) forControlEvents:UIControlEventValueChanged];
//            }
//            else
//            {
//                UISegmentedControl *todayButton = (UISegmentedControl *)[actionSheet viewWithTag:420];
//                [todayButton setTitle:@"Now" forSegmentAtIndex:0];
//                [todayButton addTarget:self action:@selector(nowBtnClicked:) forControlEvents:UIControlEventValueChanged];
//            }

        }
        [actionSheet showInView:self.view];
        [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    }
}

-(int)getRowsCount:(int)secVal
{
    switch (secVal) {
        case 0:
            return 1;
            break;
            
        case 1:
            return 1;
            break;
            
        case 2:
            return numberOfPhotos;
            break;
            
        case 3:
            return 2;
            break;
            
        case 4:
            return 2;
            break;
            
        case 5:
            return 1;
            break;
            
        default:
            return 0;
            break;
    }
}

-(int)getRowsHeight:(int)secVal  row:(int)rowVal
{
    switch (secVal) {
        case 0:
            return 50;
            break;
            
        case 1:
            return 100;
            break;
            
        case 2:
        {
            if(rowVal==0)
                return 50;
            else
                return 100;
        }
            break;
            
        case 3:
        {
            if(rowVal==0)
                return 100;
            else
                return 50;
        }
            break;
            
        case 4:
            return 50;
            break;
            
        case 5:
            return 44;
            break;
            
        default:
            return 0;
            break;
    }
}
#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [categoryArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return self.view.frame.size.width;
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    if ([[categoryArray objectAtIndex:row] isKindOfClass:[PFObject class]])
    {
        DebugLog(@"pfobject : true");
        PFObject *tempObj = [categoryArray objectAtIndex:row];
        return [tempObj objectForKey:@"title"];
    }
    else
        return [categoryArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //    if([[typeArray objectAtIndex:row] isEqualToString:@"Custom"])
    //    {
    //        typeTextField.text = @"";
    //        typeTextField.placeholder = @"Custom Reason";
    //    } else {
    //        typeTextField.placeholder = @"";
    //        typeTextField.text = [typeArray objectAtIndex:row];
    //
    //    }
    if(row != -1 && row <= categoryArray.count && categoryArray.count > 0 )
    {
        // DebugLog(@"didSelectRow: category %@", [categoryObjectsArray objectAtIndex:row]);
        // selectedCategory = [categoryObjectsArray objectAtIndex:row];
        if ([[categoryArray objectAtIndex:row] isKindOfClass:[PFObject class]])
        {
//            PFObject *tempObj = [categoryArray objectAtIndex:row];
//            DebugLog(@"didSelectRow: category %@ --> objectid %@", [tempObj objectForKey:@"title"],tempObj.objectId);
//            selectedCategory = [categoryArray objectAtIndex:row];
//            selectedCategoryId = selectedCategory.objectId;
        }
        else
        {
            selectedCategory = [categoryArray objectAtIndex:row];
//            selectedCategoryId = [SAFECITY_APP_DELEGATE getCategoryId:selectedCategory];
        }
    }
}
#pragma mark textView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)txtView
{
    CGPoint convertedPoint = [txtView convertPoint:txtView.frame.origin toView:[self.view window]];
    //DebugLog(@"table %f-- textview %f---tag %d",dropPinTableView.contentOffset.y,convertedPoint.y,txtView.tag);
    if(txtView.tag==2 && txtView.frame.origin.y > dropPinTableView.contentOffset.y)
        [dropPinTableView setContentOffset:CGPointMake(0,txtView.frame.origin.y+20) animated:YES];
    if(txtView.tag==6 && convertedPoint.y > 200)
        [dropPinTableView setContentOffset:CGPointMake(0,350) animated:YES];
    
    if ([txtView.text hasPrefix:@"Enter Description"])
    {
        txtView.textColor = [UIColor blackColor];
        txtView.text = @"";
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    switch (textView.tag) {
        case DESCRIPTION_TAG:
            selectedComments = textView.text;
            break;
        case ADDRESS_TAG:
            revAddressValue = textView.text;
            break;
        default:
            break;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0)
    {
        [self setPlaceholder:textView];
    }
}

-(void)setPlaceholder:(UITextView *)ltxtview
{
    ltxtview.text = NSLocalizedString(@"Enter Description", @"placeholder");
    ltxtview.textColor = [UIColor lightGrayColor];
}

-(void)setTextFieldProperties:(UITextField *)textField text:(NSString *)textString
{
    textField.text = textString;
    textField.backgroundColor = [UIColor clearColor];
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont systemFontOfSize:14.0f];
    textField.borderStyle = UITextBorderStyleNone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.returnKeyType = UIReturnKeyDone;
    textField.textAlignment = UITextAlignmentLeft;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.enabled = FALSE;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

-(void)setTextViewProperties:(UITextView *)textView text:(NSString *)textString
{
    textView.text = textString;
    [textView setDelegate:self];
    textView.backgroundColor = [UIColor clearColor];
    textView.font = [UIFont systemFontOfSize:14.0f];
    textView.returnKeyType = UIReturnKeyDefault;
    textView.textAlignment = UITextAlignmentLeft;
    textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    if (mode == VIEW_RECORD)
        textView.editable = FALSE;
    else
        textView.editable = TRUE;
}
@end