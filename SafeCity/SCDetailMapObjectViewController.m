//
//  SCDetailMapObjectViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 28/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "SCDetailMapObjectViewController.h"
#import "GeoPointAnnotation.h"
#import "GeoQueryAnnotation.h"
#import "SCDropPinViewController.h"
#import "SCShowListViewController.h"
#import "SCAppDelegate.h"

#define REVERSE_API(LAT,LON) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/xml?latlng=%f,%f&sensor=false",LAT,LON]

enum PinAnnotationTypeTag {
    PinAnnotationTypeTagGeoPoint = 0,
    PinAnnotationTypeTagGeoQuery = 1
};
@interface SCDetailMapObjectViewController ()

@end

@implementation SCDetailMapObjectViewController
@synthesize detailObject;
@synthesize detailMapView;
@synthesize geoPoint;
@synthesize reverseAddress;
@synthesize mode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"Location", nil);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
 //   detailMapView.showsUserLocation = YES;
//    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]
//                                         initWithTarget:self action:@selector(handleGesture:)];
//    doubleTap.numberOfTapsRequired = 2;
//    doubleTap.numberOfTouchesRequired = 1;
//    [detailMapView addGestureRecognizer:doubleTap];
    
    NSArray *array = [[self navigationController] viewControllers];
    DebugLog(@"mode %d parentViewController:%@",mode,[[array objectAtIndex:([array count] -2)] class]);
    if ([[array objectAtIndex:([array count] - 2)] class] == [SCDropPinViewController class])
    {
        [self.detailMapView setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude), MKCoordinateSpanMake(0.01, 0.01))];

        GeoPointAnnotation *annotation = [[GeoPointAnnotation alloc] initWithgeoPoint:geoPoint address:reverseAddress];
        [self.detailMapView addAnnotation:annotation];
    }
    else if ([[array objectAtIndex:([array count] - 2)] class] == [SCShowListViewController class])
    {
        if (self.detailObject) {
            // obtain the geopoint
            geoPoint = [self.detailObject objectForKey:@"location"];
            self.title = [self.detailObject objectForKey:@"address"];
            // center our map view around this geopoint
            [self.detailMapView setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude), MKCoordinateSpanMake(0.01, 0.01))];
            
            // add the annotation
            GeoPointAnnotation *annotation = [[GeoPointAnnotation alloc] initWithObject:self.detailObject];
            [self.detailMapView addAnnotation:annotation];
            
            
        }
    }
    if (mode == VIEW_RECORD || mode == EDIT_RECORD) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Get Direction" style:UIBarButtonItemStyleBordered target:self action:@selector(getDirection_Clicked:)];
        //self.navigationItem.rightBarButtonItem.enabled = [SAFECITY_APP_DELEGATE networkavailable];
    }
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:2];
    
        //MedicalRegimenDetailsViewController *parent = [array objectAtIndex:[array count] -2];
        //call the method on the parent view controller
//        if(parent != nil)
//            [parent setBSResultValue:labelResult.text weight:[NSString stringWithFormat:@"%.2f",weightValue] height:[NSString stringWithFormat:@"%.2f",heightValue]];
//        parent = nil;

}

- (void)viewDidUnload
{
    [self setDetailMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self saveCurrentocation:[locationManager location]];
}

-(void)saveCurrentocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
		return;
	}
    // Configure the new event with information from the location.
  //  GeoQueryAnnotation *currentannotation = [[GeoQueryAnnotation alloc] initWithTitle:@"You are here" coordinate:[locationManager.location coordinate] radius:0 reason:@""];
  //  [self.detailMapView addAnnotation:currentannotation];
}
-(void)getDirection_Clicked:(id)sender
{
    if([SAFECITY_APP_DELEGATE networkavailable])
    {
        UIApplication *app = [UIApplication sharedApplication];
        NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%lf,%lf&daddr=%lf,%lf",[locationManager.location coordinate].latitude,[locationManager.location coordinate].longitude,geoPoint.latitude,geoPoint.longitude];
        [app openURL:[NSURL URLWithString:urlString]];
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
//- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
//{
//    
//    DebugLog(@"handleGesture");
//    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
//        return;
//    
//    CGPoint touchPoint = [gestureRecognizer locationInView:detailMapView];
//    CLLocationCoordinate2D touchMapCoordinate = [detailMapView convertPoint:touchPoint toCoordinateFromView:detailMapView];
//    
//    MKPointAnnotation *pa = [[MKPointAnnotation alloc] init];
//    pa.coordinate = touchMapCoordinate;
//    pa.title = @"Hello";
//    [detailMapView addAnnotation:pa];
//}

-(void)reloadAddressAnnotationTitle
{
    GeoPointAnnotation *geoPointAnnotation = (GeoPointAnnotation *)[[self.detailMapView annotations] objectAtIndex:0];
    geoPointAnnotation.title = reverseAddress;
}

#pragma mark - MKMapViewDelegate
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    static NSString *GeoPointAnnotationIdentifier = @"RedPin";
    static NSString *GeoQueryAnnotationIdentifier = @"PurplePin";
    
    if ([annotation isKindOfClass:[GeoQueryAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoQueryAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoQueryAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoQuery;
            annotationView.canShowCallout = YES;
            annotationView.pinColor = MKPinAnnotationColorPurple;
            annotationView.animatesDrop = YES;
            annotationView.draggable = NO;
        }
        
        return annotationView;
    } else if ([annotation isKindOfClass:[GeoPointAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoPointAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoPointAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoPoint;
            annotationView.canShowCallout = YES;
            annotationView.pinColor = MKPinAnnotationColorRed;
            annotationView.animatesDrop = YES;
            if (mode != VIEW_RECORD)
                annotationView.draggable = YES;
            else
                annotationView.draggable = NO;
        }
        
        return annotationView;
    }
    
    return nil;
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    if (![view isKindOfClass:[MKPinAnnotationView class]] || view.tag != PinAnnotationTypeTagGeoPoint) {
        return;
    }
    if (MKAnnotationViewDragStateStarting == newState) {
        [self.detailMapView removeOverlays:[self.detailMapView overlays]];
    } else if (MKAnnotationViewDragStateNone == newState && MKAnnotationViewDragStateEnding == oldState) {
        MKPinAnnotationView *pinAnnotationView = (MKPinAnnotationView *)view;
        GeoPointAnnotation *geoPointAnnotation = (GeoPointAnnotation *)pinAnnotationView.annotation;
        geoPoint = [PFGeoPoint geoPointWithLatitude:geoPointAnnotation.coordinate.latitude longitude:geoPointAnnotation.coordinate.longitude];
       [self getReverseAddress];
        //geoPointAnnotation.title = reverseAddress;
    }
}
- (void)postNotificationWithString:(NSString *)value //post notification method and logic
{
    DebugLog(@"posting notification");
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:value,REVERSEADDRESS,geoPoint,REVERSEADDRESSGEOPOINT, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:DROPPINNOTIFICATIONNAME object:nil userInfo:dictionary];
}
-(void)getReverseAddress
{
    //NSURL *url = [NSURL URLWithString:REVERSE_API(19.131896,72.834396)];
    NSURL *url = [NSURL URLWithString:REVERSE_API(geoPoint.latitude,geoPoint.longitude)];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:YES timeoutInterval:30.0];
    
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (conn)
    {
        webData = [NSMutableData data];
    }
}

#pragma mark Connection Delegate Methods

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [conn cancel];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    if (webData)
    {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
        
        NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
    }
}

#pragma mark XML Parser Delegate Methods

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"GeocodeResponse"])
    {
    }
    else if ([elementName isEqualToString:@"result"])
    {
    }
    else if ([elementName isEqualToString:@"formatted_address"])
    {
        elementFound = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    if (elementFound)
    {
        reverseAddress = string;
        DebugLog(@"-- %@ ---",reverseAddress);
    }
    elementFound = FALSE;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
   
    DebugLog(@"reverseAddress %@ and elementName %@",reverseAddress,elementName);
    if ([elementName isEqualToString:@"formatted_address"])
    {
        if(reverseAddress != nil)
        {
            DebugLog(@"%@",reverseAddress);
            [self reloadAddressAnnotationTitle];
            [self postNotificationWithString:reverseAddress];
        }
        [xmlParser abortParsing];
    }
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    
}

@end
