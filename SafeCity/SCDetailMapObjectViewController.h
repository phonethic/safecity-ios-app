//
//  SCDetailMapObjectViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 28/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>

@interface SCDetailMapObjectViewController : UIViewController <CLLocationManagerDelegate,NSURLConnectionDelegate,NSXMLParserDelegate>
{
    CLLocationManager *locationManager;
    NSMutableData *webData;
	NSURLConnection *conn;
    NSXMLParser *xmlParser;
    BOOL elementFound;
}
@property (strong, nonatomic) PFObject *detailObject;
@property (strong, nonatomic) PFGeoPoint *geoPoint;
@property (copy,nonatomic) NSString *reverseAddress;
@property (nonatomic,readwrite) int mode;
@property (strong, nonatomic) IBOutlet MKMapView *detailMapView;
@end
