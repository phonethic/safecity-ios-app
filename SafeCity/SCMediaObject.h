//
//  SCMediaObject.h
//  SafeCity
//
//  Created by Kirti Nikam on 20/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCMediaObject : NSObject
@property (retain,nonatomic) UIImage *mediaImage;
@property(copy,nonatomic) NSString *mediaObjectId;
@end
