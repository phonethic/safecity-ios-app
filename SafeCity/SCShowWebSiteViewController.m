//
//  SCShowWebSiteViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 11/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "SCShowWebSiteViewController.h"
#import "SCSettingViewController.h"
#import "SCNewsListViewController.h"
#import "SCAboutViewController.h"

@interface SCShowWebSiteViewController ()

@end

@implementation SCShowWebSiteViewController
@synthesize webView;
@synthesize spinnerIndicator;
@synthesize loadingView;
@synthesize urlString;
@synthesize urlContent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    loadingView.backgroundColor = [UIColor blackColor];
    loadingView.layer.cornerRadius = 10;
    UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    DebugLog(@"parent %@",previousViewController);
    if ([previousViewController isKindOfClass:[SCSettingViewController class]])
    {
        [Flurry logEvent:@"Phonethics_WebSite_Event"];
        NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [webView loadRequest:req];
    }
    else if ([previousViewController isKindOfClass:[SCNewsListViewController class]])
    {
        [Flurry logEvent:@"SafeCity_NewsDetail_Event"];
//        NSString *cssold = [NSString stringWithFormat:@"<!DOCTYPE html><head><meta charset=\"UTF-8\"/><meta name=\"viewport\" content=\"width=device-width\" /><title>SafeCity</title><link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"http://madhuridixit-nene.com/wp/wp-content/themes/twentyeleven/style.css\" /><style type=\"text/css\">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><style type=\"text/css\">p {padding:5px !important;margin:5px !important;}</style></head><body class=\"single single-post postid-1606 single-format-standard single-author singular two-column right-sidebar\"><div class=\"entry-content\">%@</div></body></html>",urlContent];
        NSString *css = [NSString stringWithFormat:@"<!DOCTYPE html><head><meta charset=\"UTF-8\"/><meta name=\"viewport\" content=\"width=device-width\" /><title>SafeCity</title><link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\" http://safecityapp.com/wp-content/themes/twentytwelve/style.css\" /><style type=\"text/css\">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><style type=\"text/css\">p {padding:5px !important;margin:5px !important;}</style></head><body class=\"single single-post postid-1606 single-format-standard single-author singular two-column right-sidebar\"><div class=\"entry-content\">%@</div></body></html>",urlContent];
        [webView loadHTMLString:css baseURL:nil];
    }
    else if ([previousViewController isKindOfClass:[SCAboutViewController class]])
    {
        [Flurry logEvent:@"AboutApp_Description_Event"];
        [webView loadHTMLString:urlContent baseURL:nil];
        
    }
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    [self setSpinnerIndicator:nil];
    [self setLoadingView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark webView Delegate methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [spinnerIndicator startAnimating];
    [loadingView setHidden:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [spinnerIndicator stopAnimating];
    [loadingView setHidden:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [spinnerIndicator stopAnimating];
    [loadingView setHidden:YES];
}
@end
