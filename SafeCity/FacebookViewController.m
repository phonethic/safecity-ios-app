//
//  FacebookViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 13/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import "SCAppDelegate.h"
#import "FacebookViewController.h"
#import "SCMediaObject.h"
NSString *const kPlaceholderPostMessage = @"Say something about this...";

@interface FacebookViewController () <UINavigationControllerDelegate>
@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;

- (void)populateUserDetails;
@end

@implementation FacebookViewController
@synthesize userNameLabel = _userNameLabel;
@synthesize userProfileImage = _userProfileImage;
@synthesize announceButton = _announceButton;
@synthesize cancelButton = _cancelButton;
@synthesize message = _message;
@synthesize loginFBBtn = _loginFBBtn;
@synthesize postParams = _postParams;
@synthesize postmessageBody;
@synthesize imageAttachments;
@synthesize postLink,postTitle;

- (void)resetPostMessage
{
    self.message.text = @"";
    self.message.textColor = [UIColor blackColor];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Clear the message text when the user starts editing
    //    if ([textView.text isEqualToString:kPlaceholderPostMessage]) {
    //        textView.text = @"";
    //        textView.textColor = [UIColor blackColor];
    //    }
    
    textView.textColor = [UIColor blackColor];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // Reset to placeholder text if the user is done
    // editing and no message has been entered.
    if ([textView.text isEqualToString:@""]) {
        [self resetPostMessage];
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.message isFirstResponder] &&
        (self.message != touch.view))
    {
        [self.message resignFirstResponder];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen) {
        [self populateUserDetails];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_message becomeFirstResponder];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.postParams =
    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"", @"link",
     @"", @"picture",
     @"Title of Post", @"name",
     @"", @"caption",
     @"Check out this post.", @"description",
     nil];

    UIImage *cancelButtonImage;
    cancelButtonImage = [[UIImage imageNamed:@"DEFacebookSendButtonPortrait"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    [self.cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.announceButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.loginFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    self.message.text = self.postmessageBody;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sessionStateChanged:)
                                                 name:SCSessionStateChangedNotification
                                               object:nil];
    self.mainFBView.layer.cornerRadius = 10;
    self.mainFBView.layer.masksToBounds = YES;
    //self.mainFBView.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.mainFBView.layer.borderWidth = 2.0;
    //self.appIcon.layer.cornerRadius = 10;
    //self.appIcon.layer.masksToBounds = YES;
    //self.appIcon.layer.borderColor = [UIColor grayColor].CGColor;
    //self.appIcon.layer.borderWidth = 1.0;
    
    _announceButton.hidden = TRUE;
    _loginFBBtn.hidden = FALSE;
    
    [SAFECITY_APP_DELEGATE openSessionWithAllowLoginUI:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCancelButton:nil];
    [self setAnnounceButton:nil];
    [self setMessage:nil];
    [self setUserProfileImage:nil];
    [self setLoginFBBtn:nil];
    [self setUserNameLabel:nil];
    [self setMainFBView:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
- (IBAction)cancelModalView:(id)sender {
     [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)announce:(id)sender {
    // Add user message parameter if user filled it in
    if (![self.message.text isEqualToString:kPlaceholderPostMessage] && ![self.message.text isEqualToString:@""])
    {
        if (postLink != nil && ![self.postLink isEqualToString:@""]) {
            [self.postParams setObject:postLink forKey:@"link"];
        }
        if (imageAttachments != nil && imageAttachments.count > 0)
        {
            for (int index = 0; index < imageAttachments.count; index++)
            {
                SCMediaObject *mediaObject = (SCMediaObject *)[imageAttachments objectAtIndex:index];
                if (mediaObject.mediaImage != NULL)
                {
                    NSData *imageData = UIImagePNGRepresentation(mediaObject.mediaImage);
                    [self.postParams setObject:imageData forKey:@"picture"];
                }
                mediaObject = nil;
            }
            [self.postParams setObject:[NSString stringWithFormat:@"%@\n\n%@",self.postTitle,self.message.text] forKey:@"name"];
            [FBRequestConnection
             startWithGraphPath:@"me/photos"
             parameters:self.postParams
             HTTPMethod:@"POST"
             completionHandler:^(FBRequestConnection *connection,
                                 id result,
                                 NSError *error) {
                 NSString *alertText;
                 if (error)
                 {
                     alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d",error.domain, error.code];
                 } else {
                     alertText = @"Your message has been successfully posted on your facebook wall.";
                 }
                 [[[UIAlertView alloc] initWithTitle:ALERTTITLE
                                             message:alertText
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil]
                  show];
             }];
        }
        else
        {
            [self.postParams setObject:postTitle forKey:@"name"];
            [self.postParams setObject:self.message.text forKey:@"message"];
            [FBRequestConnection
             startWithGraphPath:@"me/feed"
             parameters:self.postParams
             HTTPMethod:@"POST"
             completionHandler:^(FBRequestConnection *connection,
                                 id result,
                                 NSError *error) {
                 NSString *alertText;
                 if (error)
                 {
                     alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d",error.domain, error.code];
                 } else {
                     alertText = @"Your message has been successfully posted on your facebook wall.";
                 }
                 [[[UIAlertView alloc] initWithTitle:ALERTTITLE
                                             message:alertText
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil]
                  show];
             }];
        }
        [self dismissModalViewControllerAnimated:YES];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:ALERTTITLE
                                    message:@"Please enter text to post."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil]
         show];
    }
    
}

- (IBAction)loginFB:(id)sender {
    UIButton * button = (UIButton*) sender;
    if([button.titleLabel.text isEqualToString:@"Log In"]) {
        [SAFECITY_APP_DELEGATE openSessionWithAllowLoginUI:YES];
    } else {
        [FBSession.activeSession closeAndClearTokenInformation];
        [self resetPostMessage];
        _announceButton.enabled =  FALSE;
        [_loginFBBtn setTitle:@"Log In" forState:UIControlStateNormal];
    }

}

// FBSample logic
// Displays the user's name and profile picture so they are aware of the Facebook
// identity they are logged in as.
- (void)populateUserDetails {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 //DebugLog(@"%@",user.name);
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = [user objectForKey:@"id"];
                 _announceButton.enabled =  TRUE;
                 _announceButton.hidden = FALSE;
                 [_loginFBBtn setTitle:@"Logout" forState:UIControlStateNormal];
                 _loginFBBtn.hidden = TRUE;
             } else {
                 _announceButton.enabled =  FALSE;
                 _announceButton.hidden = TRUE;
                 _loginFBBtn.hidden = FALSE;
                 [_loginFBBtn setTitle:@"Log In" forState:UIControlStateNormal];
             }
         }];
    }
}
-(void)settingsButtonWasPressed:(id)sender {
    if (self.settingsViewController == nil) {
        self.settingsViewController = [[FBUserSettingsViewController alloc] init];
    }
    [self.navigationController pushViewController:self.settingsViewController animated:YES];
}
- (void)sessionStateChanged:(NSNotification*)notification {
    // A more complex app might check the state to see what the appropriate course of
    // action is, but our needs are simple, so just make sure our idea of the session is
    // up to date and repopulate the user's name and picture (which will fail if the session
    // has become invalid).
    [self populateUserDetails];
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
@end
