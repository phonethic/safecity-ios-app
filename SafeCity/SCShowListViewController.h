//
//  SCShowListViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 27/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface SCShowListViewController : UIViewController
{
        sqlite3 *safecityDB;
}
@property (strong, nonatomic) IBOutlet UITableView *listtableView;
@property (copy, nonatomic) NSString *currentObjectId;
@end
