//
//  SCNewsObject.h
//  SafeCity
//
//  Created by Kirti Nikam on 18/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCNewsObject : NSObject  <NSCoding>
@property (nonatomic, copy) NSString *newsTitle;
@property (nonatomic, copy) NSString *newsContent;
@property (nonatomic, copy) NSString *newsDateTime;
@end
