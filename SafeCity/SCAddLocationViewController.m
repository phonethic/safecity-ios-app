//
//  SCAddLocationViewController.m
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "SCAddLocationViewController.h"
#import "SCDatabaseOperations.h"
#import <QuartzCore/QuartzCore.h>

@interface SCAddLocationViewController ()

@end

@implementation SCAddLocationViewController
@synthesize typePickerView;
@synthesize typeTextField;
@synthesize commentTextView;
@synthesize imageView;
@synthesize typeArray;
@synthesize revAddressValue;
@synthesize geoPointVal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"";
    commentTextView.layer.cornerRadius = 5;
    commentTextView.layer.borderColor=[UIColor blackColor].CGColor;
    commentTextView.layer.borderWidth = 0.5;
    typeArray = [[NSMutableArray alloc] initWithObjects:@"No Lights",@"No Cops",@"No Bus Stop",@"No Signal",@"Custom", nil ];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveBtnPressed:)];

}

- (void)viewDidUnload
{
    [self setTypeTextField:nil];
    [self setTypePickerView:nil];
    [self setCommentTextView:nil];
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

- (IBAction)saveBtnPressed:(id)sender
{
    DebugLog(@"saveBtnPressed");
#if TARGET_IPHONE_SIMULATOR
    if (imageView.image == nil) {
        imageView.image = [UIImage imageNamed:@"iPhoto.png"];
    }
#endif
    [SCDatabaseOperations addRecord:LOCATION_TABLE loc:geoPointVal  address:revAddressValue reason:typeTextField.text comments:commentTextView.text image:imageView.image datetime:nil];
    [self closeBtnPressed:nil];
}

- (IBAction)closeBtnPressed:(id)sender
{
    DebugLog(@"closeBtnPressed");
    [self.navigationController popViewControllerAnimated:YES];
    
//    if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
//    {   //yes->so do it!
//        [self dismissViewControllerAnimated:YES completion:NULL];
//    }
//    else
//    {   //nope->we seem to be running on something prior to iOS5, do it the old way!
//        [self dismissModalViewControllerAnimated:YES];
//    }
}

- (IBAction)addPhotoBtnPressed:(id)sender
{
    DebugLog(@"addPhotoBtnPressed");

    [self showActionsheet];
}

- (IBAction)deleteRecordBtnPressed:(id)sender {
}
-(void)showActionsheet
{
	UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Take Photo",@"Choose Existing Photo", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [uiActionSheetP showInView:self.navigationController.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"%d",buttonIndex);

    switch (buttonIndex) {
        case 0: //camera
        case 1: // from photo library
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;          
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && buttonIndex == 0)
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                }
                else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] && buttonIndex == 1)
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;                                          
                }
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
                imagePicker.allowsEditing = NO;
                [self presentModalViewController:imagePicker animated:YES];
            }
            break;
        default:
            break;
    }
}
#pragma mark -
#pragma mark UIImagePickerController Delegate Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissModalViewControllerAnimated:YES];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        if (image != nil)
        {
            imageView.image = image;
//            if(self.imgName == nil) {
//                DebugLog(@"image name is empty");
//            } else
//            {
//                DebugLog(@"image name is not empty");
//            }
        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Failed to save image" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [typeArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return self.view.frame.size.width;
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [typeArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([[typeArray objectAtIndex:row] isEqualToString:@"Custom"])
    {
        typeTextField.text = @"";
        typeTextField.placeholder = @"Custom Reason";
    } else {
        typeTextField.placeholder = @"";
        typeTextField.text = [typeArray objectAtIndex:row];
        
    }
    //DebugLog(@"%@", [typeArray objectAtIndex:row]);
    
}

@end
