//
//  SCNewsListViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 18/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SCNewsObject;
@interface SCNewsListViewController : UIViewController
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    int status;
    NSMutableData *responseAsyncData;
}
@property (strong, nonatomic) NSMutableArray *newsArray;
@property (strong, nonatomic) IBOutlet UITableView *newslistTableView;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, copy) NSString *dataFilePath;
@end
