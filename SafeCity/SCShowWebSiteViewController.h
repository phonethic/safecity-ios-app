//
//  SCShowWebSiteViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 11/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCShowWebSiteViewController : UIViewController
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    int status;
    NSMutableData *responseAsyncData;
}
@property (copy,nonatomic) NSString *urlString;
@property (copy,nonatomic) NSString *urlContent;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicator;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@end
