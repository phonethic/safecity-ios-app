//
//  SCShowListViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 27/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "SCShowListViewController.h"
#import "SCDetailMapObjectViewController.h"
#import "SCDropPinViewController.h"
#import "SWRevealViewController.h"
#import "SCAppDelegate.h"
#import "SCDatabaseOperations.h"

@interface SCShowListViewController ()

@end

@implementation SCShowListViewController
@synthesize listtableView;
@synthesize currentObjectId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Incident_List"];
    //----------start of reveal
    self.title = NSLocalizedString(@"Incident List", nil);
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slidemenu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    //----------end of reveal
    
    if ([self getRowsCount] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"No results found." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useNotificationWithString:)
	 name:refreshnotificationName
	 object:nil];
}

- (void)viewDidUnload
{
    [self setListtableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

- (void)useNotificationWithString:(NSNotification *)notification //use notification method and logic
{
    DebugLog(@"LIST : got notification");
    NSDictionary *dictionary = [notification userInfo];
    NSString *value = [dictionary valueForKey:refreshnotificationKey];
    if ([value isEqualToString:REFRESHLISTWHENDELETE])
    {
        DebugLog(@"Got refresh list notification (delete query)");
        [self deleteObjectFromLocalDB];
        [self.listtableView deselectRowAtIndexPath:[self.listtableView indexPathForSelectedRow] animated:YES];
        [self.listtableView reloadData];
    }
    else if ([value isEqualToString:REFRESHLISTWHENUPDATE])
    {
        DebugLog(@"Got refresh list notification (update query)");
        [self updateObjectInLocalDB];
        [self.listtableView deselectRowAtIndexPath:[self.listtableView indexPathForSelectedRow] animated:YES];
        [self.listtableView reloadData];
    }
}

-(void)deleteObjectFromLocalDB
{
    sqlite3_stmt *statement = nil;
    if (sqlite3_open([[SAFECITY_APP_DELEGATE databasePath] UTF8String], &safecityDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM %@ where objectId = \"%@\"",INCIDENT_TABLE,currentObjectId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(safecityDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"delete success");
        } else {
           DebugLog(@"delete failed");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(safecityDB);
}

-(void)updateObjectInLocalDB
{
    PFQuery *query = [PFQuery queryWithClassName:INCIDENT_TABLE];
    PFObject *updatedPFObject = [query getObjectWithId:currentObjectId];
    
    PFGeoPoint *geoPt = [updatedPFObject objectForKey:@"location"];
    double datetimeDouble = [[updatedPFObject objectForKey:@"date"] timeIntervalSinceReferenceDate];
//    NSDate *actualDate = [NSDate dateWithTimeIntervalSinceReferenceDate:datetimeDouble];
//    DebugLog(@"DAtetime actual: %@ indouble %f ----- latitude %f longitude %f",actualDate,datetimeDouble,geoPt.latitude,geoPt.longitude);
//    
//    DebugLog(@"Before updating data is :\n table %@ ==> (objectId --%@-- ,active--%d--,address--%@--,category_id--%@--,date--%f--,description--%@--,latitude--%f--,longitude--%f--,mode--%d--,user_id--%@--,verified--%d--)",INCIDENT_TABLE,updatedPFObject.objectId, [[updatedPFObject objectForKey:@"active"] intValue],[updatedPFObject objectForKey:@"address"],[updatedPFObject objectForKey:@"category_id"],datetimeDouble,[updatedPFObject objectForKey:@"description"],geoPt.latitude,geoPt.longitude,[[updatedPFObject objectForKey:@"mode"] intValue],[updatedPFObject objectForKey:@"user_id"],[[updatedPFObject objectForKey:@"verified"] intValue]);

    sqlite3_stmt *statement = nil;
    if (sqlite3_open([[SAFECITY_APP_DELEGATE databasePath] UTF8String], &safecityDB) == SQLITE_OK)
    {

         NSString *updateSQL = [NSString stringWithFormat:@"update %@ set active=%d,address=\"%@\",category_id=\"%@\",date=%f,description=\"%@\",latitude=%f,longitude=%f,mode=%d,user_id=\"%@\",verified=%d where objectId = \"%@\"",INCIDENT_TABLE,[[updatedPFObject objectForKey:@"active"] intValue],[updatedPFObject objectForKey:@"address"],[updatedPFObject objectForKey:@"category_id"],datetimeDouble,[updatedPFObject objectForKey:@"description"],geoPt.latitude,geoPt.longitude,[[updatedPFObject objectForKey:@"mode"] intValue],[updatedPFObject objectForKey:@"user_id"],[[updatedPFObject objectForKey:@"verified"] intValue],currentObjectId];
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(safecityDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"update success");
        }
        else
        {
            DebugLog(@"update failed");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(safecityDB);
}

-(NSInteger)getRowsCount
{
    NSInteger rowCount = 0;
    if(sqlite3_open([[SAFECITY_APP_DELEGATE databasePath] UTF8String], &safecityDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM %@",INCIDENT_TABLE];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *compiledStatementP = nil;
        
        if (sqlite3_prepare_v2(safecityDB, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                rowCount = sqlite3_column_int(compiledStatementP, 0);
            }
        }
        sqlite3_finalize(compiledStatementP);
        sqlite3_close(safecityDB);
    }
    DebugLog(@"rowCount %d",rowCount);
    return rowCount;
}
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self getRowsCount];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSString *cellCatg = nil;
    NSString *cellAddress = nil;
    NSString *cellUserID = nil;
    
    if(sqlite3_open([[SAFECITY_APP_DELEGATE databasePath] UTF8String], &safecityDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT category_id,address,user_id FROM %@ limit %d,1",INCIDENT_TABLE,indexPath.row];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *compiledStatementP = nil;
        
        if (sqlite3_prepare_v2(safecityDB, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                cellCatg = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 0)];
                cellAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 1)];
                cellUserID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 2)];
            }
        }
        sqlite3_finalize(compiledStatementP);
        sqlite3_close(safecityDB);
    }
    if ([cellUserID isEqualToString:[PFUser currentUser].objectId])
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    DebugLog(@"category id %@",cellCatg);
    cell.textLabel.font =[UIFont boldSystemFontOfSize:15];
    cell.textLabel.textColor = DEFAULT_COLOR;
    NSString *catgTitle = [SAFECITY_APP_DELEGATE getTitle:cellCatg];
    if ([catgTitle isEqualToString:@""] || catgTitle == nil) {
        cell.textLabel.text = @"Other";
    }
    else
    {
        cell.textLabel.text = catgTitle;
    }
    cell.detailTextLabel.font=[UIFont boldSystemFontOfSize:13];
    cell.detailTextLabel.numberOfLines = 2;
    cell.detailTextLabel.text = cellAddress;
    cell.imageView.image = [UIImage imageNamed:@"pin.png"];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PFObject *tempObj = [PFObject objectWithClassName:INCIDENT_TABLE];
    if(sqlite3_open([[SAFECITY_APP_DELEGATE databasePath] UTF8String], &safecityDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT objectId,active,address,category_id,date,description,latitude,longitude,mode,user_id,verified FROM %@ limit %d,1",INCIDENT_TABLE,indexPath.row];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *compiledStatementP = nil;
        
        if (sqlite3_prepare_v2(safecityDB, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                tempObj.objectId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 0)];
            
                [tempObj setObject:[NSNumber numberWithInt:sqlite3_column_int(compiledStatementP, 1)] forKey:@"active"];
                [tempObj setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 2)] forKey:@"address"];
                [tempObj setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 3)] forKey:@"category_id"];
                
                double datetimeDouble = sqlite3_column_double(compiledStatementP, 4);
                NSDate *actualDate = [NSDate dateWithTimeIntervalSinceReferenceDate:datetimeDouble];
                [tempObj setObject:actualDate forKey:@"date"];
                
                [tempObj setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 5)] forKey:@"description"];
                
                double LatDouble = sqlite3_column_double(compiledStatementP, 6);
                double LongDouble = sqlite3_column_double(compiledStatementP, 7);
                PFGeoPoint *geoPt = [PFGeoPoint geoPointWithLatitude:LatDouble longitude:LongDouble];
                [tempObj setObject:geoPt forKey:@"location"];
                
                [tempObj setObject:[NSNumber numberWithInt:sqlite3_column_int(compiledStatementP, 8)] forKey:@"mode"];
                [tempObj setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 9)] forKey:@"user_id"];
                [tempObj setObject:[NSNumber numberWithInt:sqlite3_column_int(compiledStatementP, 10)] forKey:@"verified"];
            }
        }
        sqlite3_finalize(compiledStatementP);
        sqlite3_close(safecityDB);
    }
    currentObjectId = tempObj.objectId;
    DebugLog(@"u selected --%@ %@ -- %@",tempObj.objectId,[tempObj objectForKey:@"address"],tempObj);

    SCDropPinViewController *scdropPinviewController = [[SCDropPinViewController alloc] initWithNibName:@"SCDropPinViewController" bundle:nil] ;
    DebugLog(@"%@ -- %@",[tempObj objectForKey:@"user_id"],[PFUser currentUser].objectId);
    if ([[tempObj objectForKey:@"user_id"] isEqualToString:[PFUser currentUser].objectId])
    {
        scdropPinviewController.mode = EDIT_RECORD;
        scdropPinviewController.title = @"Edit Incident";
    }
    else
    {
        scdropPinviewController.mode = VIEW_RECORD;
        scdropPinviewController.title = @"View Incident";
    }
    scdropPinviewController.selectedObj = tempObj;
    [self.navigationController pushViewController:scdropPinviewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
