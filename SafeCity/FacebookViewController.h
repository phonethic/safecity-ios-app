//
//  FacebookViewController.h
//  SafeCity
//
//  Created by Kirti Nikam on 13/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <FacebookSDK/FacebookSDK.h>

@interface FacebookViewController : UIViewController  <FBUserSettingsDelegate>
@property (copy,nonatomic) NSString *postTitle;
@property (copy,nonatomic) NSString *postmessageBody;
@property (copy,nonatomic) NSString *postLink;
@property (retain,nonatomic) NSArray *imageAttachments;
@property (strong, nonatomic) IBOutlet UIView *mainFBView;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *announceButton;
@property (strong, nonatomic) IBOutlet UITextView *message;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) IBOutlet UIButton *loginFBBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) NSMutableDictionary *postParams;

- (IBAction)cancelModalView:(id)sender;
- (IBAction)announce:(id)sender;
- (IBAction)loginFB:(id)sender;

@end
