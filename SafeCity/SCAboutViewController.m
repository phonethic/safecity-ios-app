//
//  SCAboutViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 15/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <Twitter/Twitter.h>
#import "SCAboutViewController.h"
#import "SCHelpViewController.h"
#import "FacebookViewController.h"
#import "SCAppDelegate.h"
#import "SCShowWebSiteViewController.h"

#define TEL @"+91-22-65160691"
#define EMAIL @"apps@phonethics.in"
#define SAFECITY_APPSTORE_URL @"https://itunes.apple.com/in/app/safe-city/id616709366?mt=8"

@interface SCAboutViewController ()

@end

@implementation SCAboutViewController
@synthesize aboutTableView;
@synthesize appVersionLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"About";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"About_Tab_Event"];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    appVersionLabel.text = [NSString stringWithFormat:@"SafeCity iPhone Version %@",version];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAboutTableView:nil];
    [self setAppVersionLabel:nil];
    [super viewDidUnload];
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 100;
    }
    return 45;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if (section == 0) {
//        return @"About App";
//    }
//    else if (section == 1) {
//        return @"Contact Us";
//    }
//    return @"";
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 10;
    }
    return 35;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,30)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame = CGRectMake(15, 2, 300, 30);
    headerLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    switch (section) {
        case 0:
            headerLabel.text = @"About App";
            break;
        case 1:
            headerLabel.text = @"Contact Us";
            break;
        default:
            headerLabel.text = @"";
            break;
    }
    headerLabel.textColor = DEFAULT_COLOR;
    [customView addSubview:headerLabel];
    return customView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row] ;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        DebugLog(@"id = %@",CellIdentifier);
    }
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0)
            {
                cell.textLabel.text = @"Safe City is a handy tool to geographically mark and report a safety issue faced by women and children. You can quickly locate, indentify and pin the issue on the map of your city.\n It is aimed at creating a social response system where this information is actively pursued in reaching out to the person in need and bring to the notice the issues and their causes so they can be addressed and monitored.";
                cell.textLabel.numberOfLines = 4;
              cell.imageView.image = [UIImage imageNamed:@"description.png"];
            }
            else if (indexPath.row == 1) {
                cell.textLabel.text = @"Watch the Video";
                cell.imageView.image = [UIImage imageNamed:@"video.png"];
            }
            else
            {
                cell.textLabel.text = @"How SafeCity Works";
                cell.imageView.image = [UIImage imageNamed:@"help_black.png"];
            }
        }
            break;
        case 1:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = TEL;
                cell.imageView.image = [UIImage imageNamed:@"phone.png"];
            }
            else
            {
                cell.textLabel.text = EMAIL;
                cell.imageView.image = [UIImage imageNamed:@"email.png"];
            }
        }
            break;
        case 2:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Review us on iTunes";
                cell.imageView.image = [UIImage imageNamed:@"review.png"];
            }
            else
            {
                cell.textLabel.text = @"Share this app";
                cell.imageView.image = [UIImage imageNamed:@"share.png"];
            }
        }
            break;
        default:
            break;
    }
    cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
    cell.imageView.frame = CGRectMake(0.0f, 0.0f, 30.0f, 24.0f);
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                DebugLog(@"description");
                SCShowWebSiteViewController *scshowWebSiteviewController = [[SCShowWebSiteViewController alloc] initWithNibName:@"SCShowWebSiteViewController" bundle:nil] ;
                scshowWebSiteviewController.title = @"App Description";
                scshowWebSiteviewController.urlContent = [NSString stringWithFormat:@"<!DOCTYPE html><head><style type=\"text/css\">h1{font-family:\"Times New Roman\";font-size:20px;width:300px;color:rgb(0,99,180);}</style></head><body><br><p><center><h1>%@</h1></center></p><p><font face = \"Times New Roman\" size=\"3\">%@</font></p></body></html>",ALERTTITLE,[[tableView cellForRowAtIndexPath:indexPath].textLabel.text stringByReplacingOccurrencesOfString:@"\n" withString:@"</p><p>"]];
                [self.navigationController pushViewController:scshowWebSiteviewController animated:YES];
                
            }
            else if (indexPath.row == 1) {
               DebugLog(@"video empty");
             if([SAFECITY_APP_DELEGATE networkavailable])
                {
                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://safecityapp.com/wp-content/uploads/2013/03/safe-city-ios-app-demo.mp4"]];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                
            }
            else
            {
                SCHelpViewController *helpViewController = [[SCHelpViewController alloc] init];
                helpViewController.title =  @"How SafeCity Works";
                helpViewController.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
                [self.navigationController pushViewController:helpViewController animated:YES];
            }
        }
            break;
        case 1:
        {
            if (indexPath.row == 0)
            {
                [self callFunction];
            }
            else
            {
                [self emailFunction: EMAIL];
            }
        }
            break;
        case 2:
        {
            if (indexPath.row == 0) {
                //cell.textLabel.text = @"Review us on iTunes";
                if([SAFECITY_APP_DELEGATE networkavailable])
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SAFECITY_APPSTORE_URL]];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }
            else
            {
               // cell.textLabel.text = @"Share this app";
                [self shareBtnFunction];
            }
        }
            break;
        default:
            break;
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark internal methods
- (void)callFunction
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERTTITLE
                                                           message: [NSString stringWithFormat: @"Do you want to call %@ number ?",TEL]
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERTTITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",TEL]]];
    }
}

-(void)emailFunction:(NSString *)mailID
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        mailComposer.navigationBar.tintColor = DEFAULT_COLOR;
        if (mailID == nil)
        {
            [mailComposer setToRecipients:nil];
            [mailComposer setMessageBody:[NSString stringWithFormat:@"%@ iOS app: %@",ALERTTITLE,SAFECITY_APPSTORE_URL] isHTML:YES];
        }
        else
        {
            [mailComposer setToRecipients:[NSArray arrayWithObjects:mailID,nil]];
            [mailComposer setSubject:@"Ref: Safe City"];
            [mailComposer setMessageBody:@"Enter your message here." isHTML:YES];
        }
        [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
        [self presentModalViewController:mailComposer animated:YES];
    }
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    DebugLog(@"Mail Result %u",result);
    switch (result)
    {
        case MFMailComposeResultCancelled :
            DebugLog(@"Mail Cancelled");
            break;
        case MFMailComposeResultSaved :
            DebugLog(@"Mail Saved");
            break;
        case MFMailComposeResultSent :
            DebugLog(@"Mail Sent");
            break;
        case MFMailComposeResultFailed :
            DebugLog(@"Mail Failed");
            break;
        default:
            break;
    }
    [controller dismissModalViewControllerAnimated:YES];
}

- (void)shareBtnFunction
{
    DebugLog(@"share btn clicked");
    UIActionSheet *shareActionSheet= [[UIActionSheet alloc]
                                      initWithTitle: @""
                                      delegate:self
                                      cancelButtonTitle:@"CANCEL"
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];
	shareActionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    shareActionSheet.title = @"Share on";
	[shareActionSheet showInView:self.navigationController.view];
}

-(void)actionSheet:(UIActionSheet *)localactionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"%d",buttonIndex);
    DebugLog(@"share actionsheet callback with [localactionSheet numberOfButtons] %d",[localactionSheet numberOfButtons]);
        if (buttonIndex == [localactionSheet numberOfButtons]-1)
            return;
        else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Facebook"])
        {
            DebugLog(@"facebook");
            if([SAFECITY_APP_DELEGATE networkavailable]) {
                [self facebookSharePressed];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERTTITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }
        else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Twitter"])
        {
            DebugLog(@"twitter");
            [self twitterSharePressed];
        }
        else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Email"])
        {
            DebugLog(@"email");
            [self emailFunction:nil];
        }
}
-(void)facebookSharePressed
{
    if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
    {
        DebugLog(@"Got < ios 6");
        FacebookViewController *facebookViewComposer = [[FacebookViewController alloc] initWithNibName:@"FacebookViewController" bundle:nil];
        facebookViewComposer.title = @"FACEBOOK";
        facebookViewComposer.postTitle = ALERTTITLE;
        facebookViewComposer.postLink = SAFECITY_APPSTORE_URL;
        facebookViewComposer.postmessageBody = @"Safe City App";
        facebookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self.navigationController presentModalViewController:facebookViewComposer animated:YES];
    }
    else
    {
        DebugLog(@"Got ios 6");
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *slFBComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [slFBComposer setTitle: ALERTTITLE];
            [slFBComposer setInitialText:[NSString stringWithFormat:@"%@ iOS app",ALERTTITLE]];
            [slFBComposer addURL:[NSURL URLWithString:SAFECITY_APPSTORE_URL]];
            [self presentViewController:slFBComposer animated:YES completion:nil];
//                slFBComposer.completionHandler = ^(SLComposeViewControllerResult result)
//                {
//                    if (result == SLComposeViewControllerResultCancelled) {
//                        DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
//                    }
//                    else if (result == SLComposeViewControllerResultDone) {
//                        DebugLog(@"User has finished composing the post, and tapped the send button");
//                        [Flurry logEvent:@"FBShare_App_Event"];
//                    }
//                };
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERTTITLE
                                      message:@"You may not have set up facebook service on your device or You may not connected to internet.\nPlease check and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles: nil];
            [alertView show];
        }
    }
}
-(void)twitterSharePressed
{
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetComposer = [[TWTweetComposeViewController alloc] init];
        [tweetComposer setInitialText:[NSString stringWithFormat:@"%@ iOS app",ALERTTITLE]];
        [tweetComposer addURL:[NSURL URLWithString:SAFECITY_APPSTORE_URL]];
        [self presentModalViewController:tweetComposer animated:YES];
//        tweetComposer.completionHandler = ^(SLComposeViewControllerResult result) {
//            if (result == SLComposeViewControllerResultCancelled) {
//                DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
//            }
//
//            if (result == SLComposeViewControllerResultDone) {
//                DebugLog(@"User has finished composing the post, and tapped the send button");
//                [Flurry logEvent:@"TwitterShare_App_Event"];
//            }
//        };
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERTTITLE
                                  message:@"You may not have set up twitter service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

@end
