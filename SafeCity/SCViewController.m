//
//  SCViewController.m
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//
// cateogry : To use key+value in pickerview : http://stackoverflow.com/questions/2405965/uipickerview-with-nsdictionary
//
#import "SCViewController.h"
#import <QuartzCore/QuartzCore.h>
//#import "SCAddLocationViewController.h"
//#import "SCAnnotation.h"
#import "SCDatabaseOperations.h"
#import <Parse/Parse.h>
#import "CircleOverlay.h"
#import "GeoPointAnnotation.h"
#import "GeoQueryAnnotation.h"
#import "SCShowListViewController.h"
#import "SCDropPinViewController.h"
#import "SCSettingViewController.h"
#import "SCAppDelegate.h"

#import "SWRevealViewController.h"
#import "SCAboutViewController.h"
#import "SCNewsListViewController.h"

#define REVERSE_API(LAT,LON) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/xml?latlng=%f,%f&sensor=false",LAT,LON]

enum PinAnnotationTypeTag {
    PinAnnotationTypeTagGeoPoint = 0,
    PinAnnotationTypeTagGeoQuery = 1
};

@interface SCViewController ()
{
    SWRevealViewController *revealController;
}
- (void)postDropPinNotificationWithString:(NSString *)value;
- (void)useNotificationWithString:(NSNotification *)notification;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, assign) CLLocationDistance radius;
@property (nonatomic, strong) CircleOverlay *targetOverlay;
@end

@implementation SCViewController
@synthesize radiusButton;
@synthesize loadingView;
@synthesize safecitymapView;
@synthesize searchBar;
@synthesize radiusSlider;
@synthesize backView;
@synthesize maptypeSegment;
@synthesize currentLatitude,currentLongitude,scObjectsArray,searchArray;
@synthesize reverseAddress;
@synthesize geoPoint;
@synthesize location = _location;
@synthesize radius = _radius;
@synthesize targetOverlay = _targetOverlay;
@synthesize categoryObjectsArray;
@synthesize categoryPickerView;
@synthesize actionView,tabBarView;
@synthesize selectedCategory;
@synthesize infoBtn,searchBtn,filterBtn,dropPinBtn1,refreshBtn;
@synthesize aboutBtn,newsBtn,dropPinBtn;
@synthesize lodingImageView;

-(void) touchesBegan :(NSSet *) touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    DebugLog(@"%@",[[touch view]description]);
    if ([touch view]  == self.safecitymapView)
    {
        //rest of my code
        DebugLog(@"map view touched");
    }
    
    [super touchesBegan:touches withEvent:event];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    infoBtn.frame = CGRectMake(infoBtn.frame.origin.x, self.view.frame.size.height-50, infoBtn.frame.size.width, infoBtn.frame.size.height);
    
    maptypeSegment.frame = CGRectMake(maptypeSegment.frame.origin.x, self.backView.frame.size.height-80, maptypeSegment.frame.size.width, maptypeSegment.frame.size.height);
}
- (void)viewDidLoad
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [Flurry logEvent:@"MapView"];

    
    //----------start of reveal

    self.title = NSLocalizedString(@"Incident Map", nil);

    revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    //----------end of reveal

    
    scObjectsArray = [[NSMutableArray alloc] init];
    searchArray = [[NSMutableArray alloc] init];
    
    categoryObjectsArray = [[NSMutableArray alloc] init];
    [categoryObjectsArray addObject:@"All"];
    [categoryObjectsArray addObjectsFromArray:[SAFECITY_APP_DELEGATE getAllCategoriesFromLocalTable]];
    [categoryObjectsArray addObject:@"My Pins"];
    DebugLog(@"category array %@",categoryObjectsArray);
  
    isSearchOn = NO;
    isFilterOn = NO;
    mapstate = 0;
    searchBar.tintColor = DEFAULT_COLOR;
    [self addnavbarButtons];
    [self addPickerWithDoneButton];
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.radiusButton.enabled = NO;
    self.radiusSlider.enabled = NO;
    self.newsBtn.enabled    = YES;
    self.dropPinBtn.enabled = NO;
    self.aboutBtn.enabled   = YES;

    radiusSlider.transform = CGAffineTransformRotate(radiusSlider.transform, 270.0/180*M_PI);
    
    lodingImageView.animationImages = [NSArray arrayWithObjects:
                                         [UIImage imageNamed:@"satellite_0.png"],
                                         [UIImage imageNamed:@"satellite_1.png"],
                                         [UIImage imageNamed:@"satellite_2.png"],
                                         [UIImage imageNamed:@"satellite_3.png"], nil];
    lodingImageView.animationDuration = 1.0f;
    lodingImageView.animationRepeatCount = 0;
    [lodingImageView startAnimating];
    
    loadingView.backgroundColor = [UIColor blackColor];
    loadingView.layer.cornerRadius = 10;
    //loadingView.layer.borderWidth = 10;
    //loadingView.layer.borderColor = [UIColor whiteColor].CGColor;
    loadingView.hidden = NO;
    [self.view bringSubviewToFront:loadingView];
    
    safecitymapView.mapType = MKMapTypeStandard;
    safecitymapView.zoomEnabled = YES;
    safecitymapView.scrollEnabled = YES;
    safecitymapView.delegate = self;
    safecitymapView.showsUserLocation = YES;
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useNotificationWithString:)
	 name:refreshnotificationName
	 object:nil];
    
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
    }
    else
    {
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
  //  [self setControlsState:[SAFECITY_APP_DELEGATE networkavailable]];
}

-(void) setControlsState:(Boolean) state
{
    DebugLog(@"setControlsState");
    radiusButton.enabled = state;
    radiusSlider.enabled = state;
    //self.navigationItem.leftBarButtonItem.enabled = state;
    self.navigationItem.rightBarButtonItem.enabled = state;
   // self.newsBtn.enabled = state;
    self.dropPinBtn.enabled = state;
   // self.aboutBtn.enabled = state;
    if (self.categoryObjectsArray.count <= 2 && state)
    {
        [categoryObjectsArray addObjectsFromArray:[SAFECITY_APP_DELEGATE getAllCategoriesFromLocalTable]];
        DebugLog(@"categoryObjectsArray %@",categoryObjectsArray);
        [categoryPickerView reloadAllComponents];
    }
    if (loadingView.hidden && state)
    {
        [self.loadingView setHidden:NO];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.dropPinBtn.enabled = NO;
        [lodingImageView startAnimating];
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
    }
}

-(void) addnavbarButtons
{
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slidemenu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(actionBtnPressed:)];
    
  /*
    UIBarButtonItem *settingButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings.png"] style:UIBarButtonItemStyleBordered target:self  action:@selector(setting_Clicked:)];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(search_Clicked:)];
    
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects: settingButton,searchButton, nil];
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"locate.png"] style:UIBarButtonItemStyleBordered target:self  action:@selector(refreshBtnClicked:)];
    UIBarButtonItem *filterButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"] style:UIBarButtonItemStyleBordered target:self  action:@selector(filterBtnClicked:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:refreshButton, filterButton, nil];
   */
}

-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    categoryPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    categoryPickerView.showsSelectionIndicator = YES;
    categoryPickerView.dataSource = self;
    categoryPickerView.delegate = self;
    [actionSheet addSubview:categoryPickerView];
    
    UISegmentedControl *reloadButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Reload"]];
    reloadButton.momentary = YES;
    reloadButton.frame = CGRectMake(180, 7.0f, 60.0f, 30.0f);
    reloadButton.segmentedControlStyle = UISegmentedControlStyleBar;
    reloadButton.tintColor = [UIColor blackColor];
    [reloadButton addTarget:self action:@selector(reloadFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:reloadButton];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    
    [categoryPickerView reloadAllComponents];
    if([categoryPickerView numberOfRowsInComponent:0] != -1 && categoryObjectsArray.count > 0)
    {
        [categoryPickerView selectRow:0 inComponent:0 animated:YES];
        selectedCategory = [categoryObjectsArray objectAtIndex:0];
    }
}

- (void)viewDidUnload
{
    [self setSafecitymapView:nil];
    [self setLoadingView:nil];
    [self setSearchBar:nil];
    [self setRadiusSlider:nil];
    [self setBackView:nil];
    [self setMaptypeSegment:nil];
    [self setRadiusButton:nil];
    [self setCategoryPickerView:nil];
    [self setActionView:nil];
    [self setInfoBtn:nil];
    [self setSearchBtn:nil];
    [self setFilterBtn:nil];
    [self setDropPinBtn:nil];
    [self setRefreshBtn:nil];
    [self setNewsBtn:nil];
    [self setTabBarView:nil];
    [self setAboutBtn:nil];
    [self setDropPinBtn1:nil];
    [self setLodingImageView:nil];
    [super viewDidUnload];
   // Release any retained subviews of the main view.
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

- (void)postDropPinNotificationWithString:(NSString *)value //post notification method and logic
{
    DebugLog(@"posting notification");
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:value,REVERSEADDRESS,geoPoint,REVERSEADDRESSGEOPOINT,nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:DROPPINNOTIFICATIONNAME object:nil userInfo:dictionary];
}
- (void)useNotificationWithString:(NSNotification *)notification //use notification method and logic
{
    DebugLog(@"MAP : got notification");

    NSDictionary *dictionary = [notification userInfo];
    NSString *value = [dictionary valueForKey:refreshnotificationKey];
    if ([value isEqualToString:REFRESHMAP])
    {
        [self refreshBtnClicked:nil];
    }
}
//-(void)configureMapViewData:(NSMutableArray *)annotationsArray
//{
//    DebugLog(@"MainView: configuring ...... MapViewData");
//    
//    if (safecitymapView.annotations.count > 0)
//    {
//        DebugLog(@"MainView: removing old annotations....");
//        [safecitymapView removeAnnotations:safecitymapView.annotations];
//
////        for (SCAnnotation *ann in safecitymapView.annotations) {
////            MKPinAnnotationView *av = (MKPinAnnotationView *)[safecitymapView viewForAnnotation:ann];
////            av = nil;
////        }        
//    }
//    for (PFObject *obj in annotationsArray)
//    {
//        MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
//        region.center.latitude = [[obj objectForKey:@"latitude"] doubleValue];
//        region.center.longitude = [[obj objectForKey:@"longitude"] doubleValue];
//        // region.span.latitudeDelta = 0.1f;
//        // region.span.longitudeDelta = 0.1f;
//        [safecitymapView setRegion:region animated:YES];
//        
//        SCAnnotation *ann = [[SCAnnotation alloc] init];
//        ann.title = [obj objectForKey:@"address"];
//        ann.subtitle = [obj objectForKey:@"reason"];
//        ann.coordinate = region.center;
//        [safecitymapView addAnnotation:ann];
//    }
//}

- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [loadingView setHidden:YES];
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERTTITLE
                                                          message:@"Please check your internet connection and try again."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil] ;
    [errorAlert show];
    [locationManager stopUpdatingLocation];
    [self setControlsState:[SAFECITY_APP_DELEGATE networkavailable]];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self setInitialLocation:[locationManager location]];
    [self saveCurrentocation:[locationManager location]];
}

-(void)saveCurrentocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        [loadingView setHidden:YES];
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERTTITLE
                                                              message:@"Failed to get your current location.Please check your internet connection and try again."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
        [errorAlert show];
		return;
	}
    // Configure the new event with information from the location.
	CLLocationCoordinate2D coordinate = [lLocation coordinate];
    [self.safecitymapView setRegion:MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.05, 0.05))];
    [self configureOverlay];

    geoPoint = [PFGeoPoint geoPointWithLatitude:coordinate.latitude longitude:coordinate.longitude];

    [loadingView setHidden:YES];
    radiusButton.enabled = [SAFECITY_APP_DELEGATE networkavailable];
    radiusSlider.enabled = [SAFECITY_APP_DELEGATE networkavailable];
    //self.navigationItem.leftBarButtonItem.enabled = [SAFECITY_APP_DELEGATE networkavailable];
    self.navigationItem.rightBarButtonItem.enabled = [SAFECITY_APP_DELEGATE networkavailable];
   // self.newsBtn.enabled    = [SAFECITY_APP_DELEGATE networkavailable];
    self.dropPinBtn.enabled = [SAFECITY_APP_DELEGATE networkavailable];
  //  self.aboutBtn.enabled   = [SAFECITY_APP_DELEGATE networkavailable];
    [self getReverseAddress];
}

-(IBAction)search_Clicked:(id)sender
{
    DebugLog(@"search clicked");
    [Flurry logEvent:@"Search_Event"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(actionBtnPressed:)];
    [actionView setHidden:YES];
    infoBtn.enabled = TRUE;
    [tabBarView setHidden:NO];
    radiusButton.hidden = YES;
    if(radiusSlider.frame.origin.x<300)
    {
        [self settingShowHide:0 animate:0];
    } 
    self.searchBar.hidden = FALSE;
    isSearchOn = YES;
    [self.searchBar becomeFirstResponder];
    
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.leftBarButtonItems = nil;
    
//    tgr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
//    tgr.numberOfTapsRequired = 1;
//    tgr.numberOfTouchesRequired = 1;
//    [safecitymapView addGestureRecognizer:tgr];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(cancelSearching:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"] style:UIBarButtonItemStyleBordered target:self  action:@selector(filterBtnClicked:)];    
}

//- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
//{
//    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
//        return;
//    [self cancelSearching:nil];
//}

-(void)setting_Clicked:(id)sender
{
    SCSettingViewController *settingviewController = [[SCSettingViewController alloc] initWithNibName:@"SCSettingViewController" bundle:nil] ;
    [self.navigationController pushViewController:settingviewController animated:YES];
}

- (IBAction)maptypeBtnPressed:(UISegmentedControl *)sender
{

    if(maptypeSegment.selectedSegmentIndex == 0)
    {
		safecitymapView.mapType = MKMapTypeStandard;
	}
    else if(maptypeSegment.selectedSegmentIndex == 1)
    {
        safecitymapView.mapType = MKMapTypeSatellite;
    }
    else if(maptypeSegment.selectedSegmentIndex == 2)
    {
        safecitymapView.mapType = MKMapTypeHybrid;
    }
    for (int i=0; i<[sender.subviews count]; i++)
    {
        if ([[sender.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && [[sender.subviews objectAtIndex:i]isSelected])
        {
            [[sender.subviews objectAtIndex:i] setTintColor:DEFAULT_COLOR];
        }
        if ([[sender.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && ![[sender.subviews objectAtIndex:i] isSelected])
        {
            [[sender.subviews objectAtIndex:i] setTintColor:SELECTION_COLOR];
        }
    }
    [self infoBtnPressed:nil];
}

- (IBAction)radiusBtnPressed:(id)sender {
    //DebugLog(@"%f---%f",radiusSlider.frame.origin.x,radiusSlider.frame.origin.y);
    radiusSlider.hidden = NO;
    if(radiusSlider.frame.origin.x>300)
    {
        [self settingShowHide:1 animate:1];
    } else {
        [self settingShowHide:0 animate:1];
    }
}

-(void) settingShowHide:(int) value animate: (int) anim
{
    if(anim) {
        [UIView beginAnimations:@"Drop_Down" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
    }
    if(value) {
        radiusSlider.frame =  CGRectMake(radiusSlider.frame.origin.x - 42, radiusSlider.frame.origin.y, radiusSlider.frame.size.width, radiusSlider.frame.size.height);
    } else {
        radiusSlider.frame = CGRectMake(radiusSlider.frame.origin.x + 42, radiusSlider.frame.origin.y, radiusSlider.frame.size.width, radiusSlider.frame.size.height);
    }
    if(anim)
        [UIView commitAnimations];
}

- (IBAction)infoBtnPressed:(id)sender {
    if(mapstate == 0) {
        mapstate = 1;
    [UIView animateWithDuration:1.0
                     animations:^{
                         CATransition *animation = [CATransition animation];
                         [animation setDelegate:self];
                         [animation setDuration:0.7];
                         //[animation setTimingFunction:UIViewAnimationCurveEaseInOut];
                         animation.type = @"pageCurl";
                         animation.fillMode = kCAFillModeForwards;
                         animation.endProgress = 0.50;
                         [animation setRemovedOnCompletion:NO];
                         [safecitymapView.layer addAnimation:animation forKey:@"pageCurlAnimation"];
                         [safecitymapView addSubview:backView];
                         ;}
     ];
    } else if(mapstate == 1){
        mapstate = 0;
        [UIView animateWithDuration:1.0
                         animations:^{
                             CATransition *animation = [CATransition animation];
                             [animation setDelegate:self];
                             [animation setDuration:0.7];
                             //[animation setTimingFunction:UIViewAnimationCurveEaseInOut];
                             animation.type = @"pageUnCurl";
                             animation.fillMode = kCAFillModeForwards;
                             animation.startProgress = 0.50;
                             [animation setRemovedOnCompletion:NO];
                             [safecitymapView.layer addAnimation:animation forKey:@"pageUnCurlAnimation"];
                             [backView removeFromSuperview];
                             ;}  
         ];
    }
}

- (IBAction)dropPinBtnClicked:(id)sender
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(actionBtnPressed:)];
    [actionView setHidden:YES];
    infoBtn.enabled = TRUE;
    [tabBarView setHidden:NO];
    SCDropPinViewController *scdropPinviewController = [[SCDropPinViewController alloc] initWithNibName:@"SCDropPinViewController" bundle:nil] ;
    scdropPinviewController.mode = NEW_RECORD;
    scdropPinviewController.title = @"Report Incident";
    scdropPinviewController.geoPointVal = geoPoint;
    scdropPinviewController.revAddressValue = reverseAddress;
    scdropPinviewController.categoryArray = [categoryObjectsArray mutableCopy];
    [self.navigationController pushViewController:scdropPinviewController animated:YES];
//    SCAddLocationViewController *scaddviewController = [[SCAddLocationViewController alloc] initWithNibName:@"SCAddLocationViewController" bundle:nil] ;
//    scaddviewController.geoPointVal = geoPoint;
//    scaddviewController.revAddressValue = reverseAddress;
//    [self.navigationController pushViewController:scaddviewController animated:YES];
    
    
//    if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
//    {   //yes->so do it!
//        [self presentViewController:scaddviewController animated:YES completion:NULL];
//    }
//    else
//    {   //nope->we seem to be running on something prior to iOS5, do it the old way!
//        [self presentModalViewController:scaddviewController animated:YES];
//    }
    DebugLog(@"dropPinBtn clicked");
}

- (IBAction)sliderValueChanged:(UISlider *)aSlider {
    self.radius = aSlider.value;
    
    if (self.targetOverlay) {
        [self.safecitymapView removeOverlay:self.targetOverlay];
    }
    
    self.targetOverlay = [[CircleOverlay alloc] initWithCoordinate:self.location.coordinate radius:self.radius];
    [self.safecitymapView addOverlay:self.targetOverlay];
}

- (IBAction)sliderTouchUp:(id)sender {
    if (self.targetOverlay) {
        [self.safecitymapView removeOverlay:self.targetOverlay];
    }
    [self configureOverlay];
}


- (void)refreshBtnClicked:(id)sender
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(actionBtnPressed:)];
    [actionView setHidden:YES];
    infoBtn.enabled = TRUE;
    [tabBarView setHidden:NO];
   // self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.radiusSlider.enabled = NO;
    [self.loadingView setHidden:NO];
    [lodingImageView startAnimating];
    [locationManager startUpdatingLocation];
    [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
}

- (IBAction)filterBtnClicked:(id)sender
{
    [Flurry logEvent:@"FilterBy_Event"];
    if(!isSearchOn)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(actionBtnPressed:)];
    }
    [actionView setHidden:YES];
    infoBtn.enabled = TRUE;
    [tabBarView setHidden:NO];
    isFilterOn = YES;
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

- (IBAction)aboutBtnPressed:(id)sender
{
    SCAboutViewController *scaboutviewController = [[SCAboutViewController alloc] initWithNibName:@"SCAboutViewController" bundle:nil] ;
    [self.navigationController pushViewController:scaboutviewController animated:YES];
}

- (IBAction)newsBtnClicked:(id)sender
{
    SCNewsListViewController *scnewsListViewController = [[SCNewsListViewController alloc] initWithNibName:@"SCNewsListViewController" bundle:nil] ;
    scnewsListViewController.title = @"safecityapp.com";
    [self.navigationController pushViewController:scnewsListViewController animated:YES];
}

- (void)reloadFilterBtnClicked:(id)sender
{
    [SAFECITY_APP_DELEGATE reloadAllCategoriesIntoLocalTable];
    if (categoryObjectsArray == nil) {
        categoryObjectsArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [categoryObjectsArray removeAllObjects];
    }
    [categoryObjectsArray addObject:@"All"];
    [categoryObjectsArray addObjectsFromArray:[SAFECITY_APP_DELEGATE getAllCategoriesFromLocalTable]];
    [categoryObjectsArray addObject:@"My Pins"];
    [categoryPickerView reloadAllComponents];
}

- (void)doneFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    if([categoryPickerView numberOfRowsInComponent:0] != -1)
        [self configureOverlay];
    isFilterOn = NO;

}

- (void)cancelFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    isFilterOn = NO;
}
- (void)actionBtnPressed:(id)sender
{
    if (actionView.hidden)
    {
        [actionView setHidden:NO];
        infoBtn.enabled = FALSE;
        [tabBarView setHidden:YES];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"delete.png"]
                                                                                  style:UIBarButtonItemStyleBordered target:self action:@selector(actionBtnPressed:)];
    }
    else
    {
        [actionView setHidden:YES];
        infoBtn.enabled = TRUE;
        [tabBarView setHidden:NO];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                                  style:UIBarButtonItemStyleBordered target:self action:@selector(actionBtnPressed:)];
    }
}

-(void)getReverseAddress
{
    //NSURL *url = [NSURL URLWithString:REVERSE_API(19.131896,72.834396)];
    NSURL *url = [NSURL URLWithString:REVERSE_API(geoPoint.latitude,geoPoint.longitude)];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:YES timeoutInterval:30.0];
    
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (conn)
    {
        webData = [NSMutableData data];
    }
}

#pragma mark Connection Delegate Methods

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [conn cancel];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    if (webData)
    {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
        
        NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
    }
}

#pragma mark XML Parser Delegate Methods

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"GeocodeResponse"])
    {
    }
    else if ([elementName isEqualToString:@"result"])
    {
    }
    else if ([elementName isEqualToString:@"formatted_address"])
    {
        elementFound = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    if (elementFound)
    {
        reverseAddress = string;
        //DebugLog(@"-- %@ ---",reverseAddress);
    }
    elementFound = FALSE;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"result"])
    {
        if(reverseAddress != nil)
        {
            DebugLog(@"%@",reverseAddress);
            [self postDropPinNotificationWithString:reverseAddress];
        }
        [xmlParser abortParsing];
    }
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    
}


#pragma mark MapKit methods
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    static NSString *GeoPointAnnotationIdentifier = @"RedPinAnnotation";
    static NSString *GeoQueryAnnotationIdentifier = @"PurplePinAnnotation";
    
    if ([annotation isKindOfClass:[GeoQueryAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoQueryAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoQueryAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoQuery;
            annotationView.canShowCallout = YES;
            annotationView.pinColor = MKPinAnnotationColorPurple;
            annotationView.animatesDrop = NO;
            annotationView.draggable = YES;
        }
        
        return annotationView;
    } else if ([annotation isKindOfClass:[GeoPointAnnotation class]]) {
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[mapView
                                dequeueReusableAnnotationViewWithIdentifier:GeoPointAnnotationIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:GeoPointAnnotationIdentifier];
            annotationView.tag = PinAnnotationTypeTagGeoPoint;
            annotationView.canShowCallout = YES;
            annotationView.pinColor = MKPinAnnotationColorRed;
            annotationView.animatesDrop = YES;
            annotationView.draggable = NO;
            
            UIButton *editButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            annotationView.rightCalloutAccessoryView = editButton;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay {
    static NSString *CircleOverlayIdentifier = @"Circle";
    
    if ([overlay isKindOfClass:[CircleOverlay class]]) {
        CircleOverlay *circleOverlay = (CircleOverlay *)overlay;
        
        MKCircleView *annotationView =
        (MKCircleView *)[mapView dequeueReusableAnnotationViewWithIdentifier:CircleOverlayIdentifier];
        
        if (!annotationView) {
            MKCircle *circle = [MKCircle
                                circleWithCenterCoordinate:circleOverlay.coordinate
                                radius:circleOverlay.radius];
            annotationView = [[MKCircleView alloc] initWithCircle:circle];
        }
        
        if (overlay == self.targetOverlay) {
            annotationView.fillColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.3f];
            annotationView.strokeColor = [UIColor redColor];
            annotationView.lineWidth = 1.0f;
        } else {
            annotationView.fillColor = [UIColor colorWithWhite:0.3f alpha:0.3f];
            annotationView.strokeColor = [UIColor purpleColor];
            annotationView.lineWidth = 2.0f;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    if (![view isKindOfClass:[MKPinAnnotationView class]] || view.tag != PinAnnotationTypeTagGeoQuery) {
        return;
    }
    
    if (MKAnnotationViewDragStateStarting == newState) {
        [self.safecitymapView removeOverlays:[self.safecitymapView overlays]];
    } else if (MKAnnotationViewDragStateNone == newState && MKAnnotationViewDragStateEnding == oldState) {
        MKPinAnnotationView *pinAnnotationView = (MKPinAnnotationView *)view;
        GeoQueryAnnotation *geoQueryAnnotation = (GeoQueryAnnotation *)pinAnnotationView.annotation;
        geoPoint = [PFGeoPoint geoPointWithLatitude:geoQueryAnnotation.coordinate.latitude longitude:geoQueryAnnotation.coordinate.longitude];
        [self getReverseAddress];
        self.location = [[CLLocation alloc] initWithLatitude:geoQueryAnnotation.coordinate.latitude longitude:geoQueryAnnotation.coordinate.longitude];
        [self configureOverlay];
    }
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[safecitymapView viewForAnnotation:view.annotation];
    DebugLog(@"Pin selected as : %@",pinView.annotation.title);
//    if(pinView.pinColor == MKPinAnnotationColorRed)
//    {
//        GeoPointAnnotation *annotation = view.annotation;
//        SCAddLocationViewController *scaddviewController = [[SCAddLocationViewController alloc] initWithNibName:@"SCAddLocationViewController" bundle:nil] ;
//        scaddviewController.geoPointVal = annotation.title;
//        scaddviewController.revAddressValue = annotation.title;
//        [self.navigationController pushViewController:scaddviewController animated:YES];
//    }
    
    GeoPointAnnotation *annotation = view.annotation;
    PFObject *tempObj =  annotation.object;
    SCDropPinViewController *scdropPinviewController = [[SCDropPinViewController alloc] initWithNibName:@"SCDropPinViewController" bundle:nil] ;
    DebugLog(@"%@ -- %@",[tempObj objectForKey:@"user_id"],[PFUser currentUser].objectId);
    if ([[tempObj objectForKey:@"user_id"] isEqualToString:[PFUser currentUser].objectId])
    {
        scdropPinviewController.mode = EDIT_RECORD;
        scdropPinviewController.title = @"Edit Incident";
    }
    else
    {
        scdropPinviewController.mode = VIEW_RECORD;
        scdropPinviewController.title = @"View Incident";
    }
    scdropPinviewController.selectedObj = tempObj;
    [self.navigationController pushViewController:scdropPinviewController animated:YES];
}

#pragma mark SearchBar methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    if (searchArray.count > 0)
    {
        [searchArray removeAllObjects];
    }
   // [SCDatabaseOperations findAddress:LOCATION_TABLE searchText:self.searchBar.text];
    [self configureOverlay];
    [self.safecitymapView removeOverlays:[self.safecitymapView overlays]];

}

-(void)cancelSearching:(id)sender
{
    radiusButton.hidden = NO;
    //[safecitymapView removeGestureRecognizer:tgr];
    [self.searchBar resignFirstResponder];
    isSearchOn = NO;
    self.searchBar.hidden = YES;
    [self addnavbarButtons];
    [self configureOverlay];
}

-(void)doneSearching:(id)sender
{
    [self cancelSearching:nil];
    
   // self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStyleBordered target:self action:@selector(search_Clicked:)];
}

#pragma mark MAPVIEW methods

- (void)setInitialLocation:(CLLocation *)aLocation {
    self.location = aLocation;
    self.radius = 1000;
}

- (void)configureOverlay {
    if (self.location) {
        [self.safecitymapView removeAnnotations:[self.safecitymapView annotations]];
        [self.safecitymapView removeOverlays:[self.safecitymapView overlays]];
        [self.scObjectsArray removeAllObjects];

        CircleOverlay *overlay = [[CircleOverlay alloc] initWithCoordinate:self.location.coordinate radius:self.radius];
        [self.safecitymapView addOverlay:overlay];
        
      //  GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc] initWithCoordinate:self.location.coordinate radius:self.radius];
        GeoQueryAnnotation *annotation = [[GeoQueryAnnotation alloc] initWithTitle:@"Drag Pin" coordinate:self.location.coordinate radius:self.radius reason:@""];
        [self.safecitymapView addAnnotation:annotation];
        
        [self updateLocations];
    }
}

- (void)updateLocations {
    CGFloat kilometers = self.radius/1000.0f;
      DebugLog(@"updateLocations : is searchOn : %d",isSearchOn);
    PFQuery *query = [PFQuery queryWithClassName:INCIDENT_TABLE];
    [query setLimit:1000];

    if (!isSearchOn && !isFilterOn)
    {
        [query whereKey:@"location"
        nearGeoPoint:[PFGeoPoint geoPointWithLatitude:self.location.coordinate.latitude
                                            longitude:self.location.coordinate.longitude]
        withinKilometers:kilometers];
    }
    else if (isSearchOn && isFilterOn)
    {
//        if ([selectedCategory isKindOfClass:[PFObject class]])
//        {
//            [query whereKey:@"category_id" equalTo:selectedCategory];
//        }
//        else
        if(![selectedCategory isEqualToString:@"All"] && ![selectedCategory isEqualToString:@"My Pins"])
        {
            [query whereKey:@"category_id" equalTo:selectedCategory];
        }
        else if ([selectedCategory isEqualToString:@"My Pins"])
        {
             [query whereKey:@"user_id" equalTo:[PFUser currentUser].objectId];
        }
    
        [query whereKey:@"address" matchesRegex:searchBar.text modifiers:@"i"]; //for case insensitive comparison
        [self.safecitymapView removeOverlays:[self.safecitymapView overlays]];
    }
    else if (isSearchOn && !isFilterOn)
    {
        [query whereKey:@"address" matchesRegex:searchBar.text modifiers:@"i"]; //for case insensitive comparison
    }
    else if (isFilterOn && !isSearchOn)
    {
        
//        if ([selectedCategory isKindOfClass:[PFObject class]])
//        {
//            [query whereKey:@"category_id" equalTo:selectedCategory];
//        }
//        else
        if(![selectedCategory isEqualToString:@"All"] &&  ![selectedCategory isEqualToString:@"My Pins"])
        {
            [query whereKey:@"category_id" equalTo:selectedCategory];
            [query whereKey:@"location" nearGeoPoint:[PFGeoPoint geoPointWithLatitude:self.location.coordinate.latitude
                                                                            longitude:self.location.coordinate.longitude]
           withinKilometers:kilometers];
        }
        else if ([selectedCategory isEqualToString:@"All"])
        {
            [query whereKey:@"location" nearGeoPoint:[PFGeoPoint geoPointWithLatitude:self.location.coordinate.latitude
                                                                            longitude:self.location.coordinate.longitude]
           withinKilometers:kilometers];
        }
        else if ([selectedCategory isEqualToString:@"My Pins"])
        {
            DebugLog(@"currrent user %@",[PFUser currentUser]);
            [query whereKey:@"user_id" equalTo:[PFUser currentUser].objectId];
            [self.safecitymapView removeOverlays:[self.safecitymapView overlays]];
        }
    }

    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
           // DebugLog(@"found objects as %@",objects);
            scObjectsArray = [NSMutableArray arrayWithArray:objects];
            if (scObjectsArray.count > 0)
                   [self updateLocalTable];
            
            for (PFObject *object in objects) {
                GeoPointAnnotation *geoPointAnnotation = [[GeoPointAnnotation alloc]
                                                          initWithObject:object];
                [self.safecitymapView addAnnotation:geoPointAnnotation];
            }
        }
        else
        {
            DebugLog(@"error %@",error);
        }
    }];
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [categoryObjectsArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return self.view.frame.size.width;
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    //DebugLog(@"titleForRow: categorypickerarray %@",categoryObjectsArray);
    if ([[categoryObjectsArray objectAtIndex:row] isKindOfClass:[PFObject class]])
    {
        DebugLog(@"pfobject : true");
        PFObject *tempObj = [categoryObjectsArray objectAtIndex:row];
        return [tempObj objectForKey:@"title"];
    }
    else
        return [categoryObjectsArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(row != -1 && row <= categoryObjectsArray.count && categoryObjectsArray.count > 0 )
    {
       // DebugLog(@"didSelectRow: category %@", [categoryObjectsArray objectAtIndex:row]);
       // selectedCategory = [categoryObjectsArray objectAtIndex:row];
        if ([[categoryObjectsArray objectAtIndex:row] isKindOfClass:[PFObject class]])
        {
            PFObject *tempObj = [categoryObjectsArray objectAtIndex:row];
            DebugLog(@"didSelectRow: category %@ --> objectid %@", [tempObj objectForKey:@"title"],tempObj.objectId);
            selectedCategory = tempObj.objectId;
        }
        else
            selectedCategory = [categoryObjectsArray objectAtIndex:row];
    }
}
- (void) DeleteRowsFromTable
{
    DebugLog(@"=============== Deleting  rows ================");

    sqlite3_stmt *statement;
    sqlite3 *database;

    if (sqlite3_open([[SAFECITY_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM %@", INCIDENT_TABLE];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"=============== Success on deleting rows ================");
            
        } else
        {
            DebugLog(@"=============== Error while deleting rows ================");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
}

-(void) updateLocalTable {
    [self DeleteRowsFromTable];
	// Setup the database object
	sqlite3 *database;
	// Open the database from the users filessytem
	if(sqlite3_open([[SAFECITY_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        DebugLog(@"=============== Inserting  rows ===============");
        sqlite3_stmt *compiledStatement;
        for (PFObject *myObject in scObjectsArray)
        {
            //PFObject *myObject = [scObjectsArray objectAtIndex:0];
            PFGeoPoint *geoPt = [myObject objectForKey:@"location"];
            double datetimeDouble = [[myObject objectForKey:@"date"] timeIntervalSinceReferenceDate];
            NSDate *actualDate = [NSDate dateWithTimeIntervalSinceReferenceDate:datetimeDouble];
            DebugLog(@"DAtetime actual: %@ indouble %f ----- latitude %f longitude %f",actualDate,datetimeDouble,geoPt.latitude,geoPt.longitude);
        
        
            DebugLog(@"Before inserting data is :\n table %@ ==> (objectId --%@-- ,active--%d--,address--%@--,category_id--%@--,date--%f--,description--%@--,latitude--%f--,longitude--%f--,mode--%d--,user_id--%@--,verified--%d--)",INCIDENT_TABLE,myObject.objectId, [[myObject objectForKey:@"active"] intValue],[myObject objectForKey:@"address"],[myObject objectForKey:@"category_id"],datetimeDouble,[myObject objectForKey:@"description"],geoPt.latitude,geoPt.longitude,[[myObject objectForKey:@"mode"] intValue],[myObject objectForKey:@"user_id"],[[myObject objectForKey:@"verified"] intValue]);
        
            /// data base
            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO %@(objectId,active,address,category_id,date,description,latitude,longitude,mode,user_id,verified) values(\"%@\",%d,\"%@\",\"%@\",%f,\"%@\",%f,%f,%d,\"%@\",%d)",INCIDENT_TABLE,myObject.objectId, [[myObject objectForKey:@"active"] intValue],[myObject objectForKey:@"address"],[myObject objectForKey:@"category_id"],datetimeDouble,[myObject objectForKey:@"description"],geoPt.latitude,geoPt.longitude,[[myObject objectForKey:@"mode"] intValue],[myObject objectForKey:@"user_id"],[[myObject objectForKey:@"verified"] intValue]];
               
           //  NSString *insertSQL = @"insert into Incident_SafeCity(objectId,active,address,category_id,date,description,latitude,longitude,mode,user_id,verified) values(\"pToU26KHdh\",1,\"New Dadabhai Road, Tepgaon, Andheri West, Mumbai, Maharashtra, India\",\"CPERFWKsff\",380184143.000000,\"No autoricks\",19.125220,72.839270,5,\"JjmjS0EDDA\",1)";
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(database, insert_stmt, -1, &compiledStatement, NULL);
            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
            {
                    DebugLog(@"=============== Success on inserting row ================");
                    
                } else
                {
                    DebugLog(@"=============== Error while inserting row ================ %d",sqlite3_step(compiledStatement));
            }
            // Release the compiled statement from memory
            sqlite3_reset(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}


@end