//
//  SCHelpViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 08/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "SCHelpViewController.h"
#import "SWRevealViewController.h"
#import "SCAboutViewController.h"

@interface SCHelpViewController ()

@end

@implementation SCHelpViewController
@synthesize infoImageView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Help_Tab_Event"];
     
    DebugLog(@"navcount %d and parent %@",[self.navigationController.viewControllers count],self.parentViewController);
//    UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
  //  DebugLog(@"parent %@",previousViewController);
    //if ([previousViewController isKindOfClass:[SCAboutViewController class]])
//    int currentVCIndex = [self.navigationController.viewControllers indexOfObject:self.navigationController.topViewController];
//    DebugLog(@"parent %@",[self.navigationController.viewControllers objectAtIndex:currentVCIndex-1]);
//    if([[self.navigationController.viewControllers objectAtIndex:currentVCIndex-1] isKindOfClass:[SCAboutViewController class]])
//    {
//        DebugLog(@"SCAboutViewController");
//    } else {
//        DebugLog(@"not SCAboutViewController");
//    }
    if([self.navigationController.viewControllers count] > 1)
    {
        UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slidemenu.png"]
                                                                             style:UIBarButtonItemStyleBordered target:self action:@selector(backBtnClicked:)];
        self.navigationItem.leftBarButtonItem = revealButtonItem;
    }
    else
    {
        //----------start of reveal
        SWRevealViewController *revealController = [self revealViewController];
        [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
        UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slidemenu.png"]
                                                                             style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        //----------end of reveal
 
    }
    infoImageView.image = [UIImage imageNamed:@"info_screen.jpg"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(openBtnPressed:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setInfoImageView:nil];
    [super viewDidUnload];
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void)openBtnPressed:(id)sender
{
    infoImageView.image = [UIImage imageNamed:@"info_screen2.jpg"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"delete.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(closeBtnPressed:)];
}

- (void)closeBtnPressed:(id)sender
{
    infoImageView.image = [UIImage imageNamed:@"info_screen.jpg"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(openBtnPressed:)];
}

- (void)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
