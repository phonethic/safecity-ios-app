//
//  SCViewController.h
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <sqlite3.h>

@interface SCViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate,NSURLConnectionDelegate,NSXMLParserDelegate,UISearchBarDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
      
    CLLocationManager *locationManager;
    NSMutableData *webData;
	NSURLConnection *conn;
    NSXMLParser *xmlParser;
    BOOL elementFound;
    BOOL isSearchOn;
    BOOL isFilterOn;
    int mapstate;
    UIActionSheet *actionSheet;
    UITapGestureRecognizer *tgr;
}
@property (strong, nonatomic) IBOutlet UIButton *radiusButton;
@property (readwrite,nonatomic) double currentLatitude;
@property (readwrite,nonatomic) double currentLongitude;
@property (strong, nonatomic) NSMutableArray *scObjectsArray;
@property (strong, nonatomic) NSMutableArray *searchArray;
@property (copy,nonatomic) NSString *reverseAddress;
@property (copy,nonatomic) NSString *selectedCategory;
@property (strong, nonatomic) PFGeoPoint *geoPoint;
@property (strong, nonatomic) NSMutableArray *categoryObjectsArray;

@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet MKMapView *safecitymapView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISlider *radiusSlider;
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *maptypeSegment;
@property (strong, nonatomic) IBOutlet UIPickerView *categoryPickerView;
@property (strong, nonatomic) IBOutlet UIView *actionView;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet UIButton *filterBtn;
@property (strong, nonatomic) IBOutlet UIButton *dropPinBtn;
@property (strong, nonatomic) IBOutlet UIButton *refreshBtn;
@property (strong, nonatomic) IBOutlet UIButton *newsBtn;
@property (strong, nonatomic) IBOutlet UIView *tabBarView;
@property (strong, nonatomic) IBOutlet UIButton *aboutBtn;
@property (strong, nonatomic) IBOutlet UIButton *dropPinBtn1;
@property (strong, nonatomic) IBOutlet UIImageView *lodingImageView;


- (IBAction)maptypeBtnPressed:(id)sender;
- (IBAction)radiusBtnPressed:(id)sender;
- (IBAction)infoBtnPressed:(id)sender;
- (IBAction)dropPinBtnClicked:(id)sender;
- (IBAction)sliderValueChanged:(UISlider *)aSlider;
- (IBAction)sliderTouchUp:(id)sender;
- (IBAction)refreshBtnClicked:(id)sender;
- (IBAction)search_Clicked:(id)sender;
- (IBAction)filterBtnClicked:(id)sender;
- (IBAction)aboutBtnPressed:(id)sender;
- (IBAction)newsBtnClicked:(id)sender;

-(void) setControlsState:(Boolean) state;

@end
