//
//  SCAddLocationViewController.h
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface SCAddLocationViewController : UIViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
}

@property (strong, nonatomic) PFGeoPoint *geoPointVal;
@property (copy,nonatomic) NSString *revAddressValue;
@property (strong,nonatomic) NSMutableArray *typeArray;
@property (strong, nonatomic) IBOutlet UIPickerView *typePickerView;
@property (strong, nonatomic) IBOutlet UITextField *typeTextField;
@property (strong, nonatomic) IBOutlet UITextView *commentTextView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)saveBtnPressed:(id)sender;
- (IBAction)closeBtnPressed:(id)sender;
- (IBAction)addPhotoBtnPressed:(id)sender;
- (IBAction)deleteRecordBtnPressed:(id)sender;

@end
