//
//  SCNewsListViewController.m
//  SafeCity
//
//  Created by Kirti Nikam on 18/03/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "SCNewsListViewController.h"
#import "SCViewController.h"
#import "SCNewsObject.h"
#import "SCShowWebSiteViewController.h"
#import "SCAppDelegate.h"

#define NEWLINK @"http://safecityapp.com/api/get_category_posts/?slug=news&dev=1"

@interface SCNewsListViewController ()

@end

@implementation SCNewsListViewController
@synthesize newsArray;
@synthesize newslistTableView;
@synthesize loadingView,indicator;
@synthesize dataFilePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"NewsList_Tab_Event"];
    newsArray = [[NSMutableArray alloc] init];
    loadingView.backgroundColor = [UIColor blackColor];
    loadingView.layer.cornerRadius = 10;
    //loadingView.layer.borderWidth = 10;
    //loadingView.layer.borderColor = [UIColor whiteColor].CGColor;
    loadingView.hidden = NO;
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    dataFilePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"news.archive"]];
    
    if ([SAFECITY_APP_DELEGATE networkavailable]) {
        [self asynchronousCall:NEWLINK];
    }
    else
    {
        [self readFromFile];
    }
}

-(void)readFromFile
{
    [self.loadingView setHidden:YES];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: dataFilePath])
    {
        newsArray = [NSKeyedUnarchiver unarchiveObjectWithFile: dataFilePath];
        DebugLog(@"readFromFile newsArray %@",newsArray);
        if (newsArray.count == 0) {
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:ALERTTITLE
                                      message:@"Please check your internet connection and try again."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
        else
        {
            [newslistTableView reloadData];
        }
    }
    else
    {
        DebugLog(@"file not exists");
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:ALERTTITLE
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
 
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setNewslistTableView:nil];
    [self setLoadingView:nil];
    [self setIndicator:nil];
    [super viewDidUnload];
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

-(void)asynchronousCall:(NSString *)url
{
    [self.loadingView setHidden:NO];
    [self.indicator startAnimating];
	/****************Asynchronous Request**********************/
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NO timeoutInterval:30.0];
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    DebugLog(@"Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self.loadingView setHidden:YES];
	if(responseAsyncData)
	{
		//NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
      //  DebugLog(@"json: %@", json);
        [newsArray removeAllObjects];
        DebugLog(@"%@", [json objectForKey:@"status"]);
        DebugLog(@"%@", [json objectForKey:@"posts"]);

        NSArray* postsArray = [json objectForKey:@"posts"];
        for (NSDictionary *dict in postsArray)
        {
                SCNewsObject *newsObject = [[SCNewsObject alloc] init];
                newsObject.newsTitle = [SAFECITY_APP_DELEGATE filterHTMLCodes:[dict objectForKey:@"title"]];
                newsObject.newsContent = [dict objectForKey:@"content"];
                newsObject.newsDateTime = [dict objectForKey:@"modified"];
                DebugLog(@"\ntitle: %@  , content: %@ datetimne: %@\n", newsObject.newsTitle, newsObject.newsContent,newsObject.newsDateTime);
                [newsArray addObject:newsObject];
                newsObject = nil;
        }
		responseAsyncData = nil;
	}
    DebugLog(@"array count: %d", [newsArray count]);
    if (newsArray.count > 0) {
        [NSKeyedArchiver archiveRootObject:newsArray toFile:dataFilePath];
        [newslistTableView reloadData];
    }
    else
    {
        [self readFromFile];
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    SCNewsObject *tempnewsObject  = [newsArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];
    cell.textLabel.text = tempnewsObject.newsTitle;
    cell.imageView.image = [UIImage imageNamed:@"news_black.png"];
    cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:12.0];
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    cell.detailTextLabel.text = tempnewsObject.newsDateTime;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SCNewsObject *tempnewsObject  = [newsArray objectAtIndex:indexPath.row];
    SCShowWebSiteViewController *scshowWebSiteviewController = [[SCShowWebSiteViewController alloc] initWithNibName:@"SCShowWebSiteViewController" bundle:nil] ;
    scshowWebSiteviewController.title = @"News";
    scshowWebSiteviewController.urlContent = [NSString stringWithFormat:@"<p><center><U><H2>%@</H2></U></center></p>%@",tempnewsObject.newsTitle,tempnewsObject.newsContent];
    [self.navigationController pushViewController:scshowWebSiteviewController animated:YES];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}
@end
