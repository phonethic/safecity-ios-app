//
//  SCAppDelegate.h
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <FacebookSDK/FacebookSDK.h>
#import "Reachability.h"

#define SAFECITY_APP_DELEGATE (SCAppDelegate *)[[UIApplication sharedApplication] delegate]

//CREATE TABLE Incident_SafeCity(objectId text,active INTEGER,address text,category_id text,date DOUBLE,descsription text,latitude double,longitude double,mode INTEGER,user_id text,verified INTEGER);
//insert into Incident_SafeCity(objectId,active,address,category_id,date,descsription,latitude,longitude,mode,user_id,verified) values("oo",1,"aa","cc",12.13,"descrp",11.5,22.5,5,"aa",1);
extern NSString *const SCSessionStateChangedNotification;

@class SWRevealViewController;
@class SCViewController;

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
    SCViewController *scViewController;
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) NSMutableArray *catgArray;
@property(copy,nonatomic) NSString *databasePath;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;

- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;
- (NSString *)saveImage: (UIImage*)image;
- (void)removeImage:(NSString *)imageName;

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;

- (NSString *)filterHTMLCodes:(NSString *)String;

-(void)reloadAllCategoriesIntoLocalTable;
-(void)insertAllCategoriesIntoLocalTable;
-(NSMutableArray *)getAllCategoriesFromLocalTable;
-(NSString *)getTitle:(NSString *) objectId;
-(NSString *)getCategoryId:(NSString *)catgTitle;
@end
