//
//  SCDatabaseOperations.h
//  SafeCity
//
//  Created by Rishi on 24/12/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOCATION_TABLE @"LocationDetails"

@interface SCDatabaseOperations : NSObject {
    
}

+(void)setAppId;
+(void)loginUser : (NSString *) userid password:(NSString *) pass;
+(void)addRecord:(NSString *)tableName loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)reasonValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue;
+(NSArray *)getAllRows:(NSString *)tableName;
+(NSArray *)getCategoryList:(NSString *)tableName;
+(void)updateRecord:(NSString *)tableName objId:(NSString *)lobjId loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)reasonValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue;
+(void)deleteRecord:(NSString *)tableName objId:(NSString *)lobjId;
//+(void)findAddress:(NSString *)tableName searchText:(NSString *) textToSearch;
+(void)addCurrentUserDetails:(NSString *)tableName first:(NSString *)firstname last:(NSString *)lastname email:(NSString *)lemail phone:(NSString *)number;

//single images
+(void)addRecordWithRelation:(NSString *)tableName loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue;
+(void)updateRecordWithRelation:(NSString *)tableName objId:(NSString *)lobjId loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue image:(UIImage *)imageValue datetime:(NSDate *)datetimeValue sendPostAs:(NSString *)postKey;

//multiple images
+(void)addRecordWithRelation:(NSString *)tableName loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue imageArray:(NSArray *)imageArrayValue datetime:(NSDate *)datetimeValue;
+(void)updateRecordWithRelation:(NSString *)tableName objId:(NSString *)lobjId loc:(PFGeoPoint *)lgeoPoint address:(NSString *)addressValue reason:(NSString *)category_idValue comments:(NSString *)commentsValue imageArray:(NSArray *)imageArrayValue datetime:(NSDate *)datetimeValue sendPostAs:(NSString *)postKey;

+(void)deleteRecordWithRelation:(NSString *)tableName objId:(NSString *)lobj sendPostAs:(NSString *)postKey;
+(void)deleteRecordFromTable:(NSString *)tableName objId:(NSString *)lobjId;

+(NSArray *)getAllCategories;
+(void)getImageFile:(PFObject *) objectId;
+(void)getImageFiles:(PFObject *) objectId;
+(NSString *)getTitle:(NSString *) objectId;
+(NSString *)getTitle:(NSString *) objectId categoryArray:(NSArray *)catgArray;
@end
