//
//  GeoPointAnnotation.h
//  Geolocations
//
//  Created by Héctor Ramos on 8/2/12.
//  Copyright (c) 2012 Parse, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>

@interface GeoPointAnnotation : NSObject <MKAnnotation>

- (id)initWithObject:(PFObject *)aObject;
- (id)initWithgeoPoint:(PFGeoPoint *)geoPoint address:(NSString *)laddress;
@property (nonatomic,strong) PFObject *object;
@property (nonatomic,readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *subtitle;

@end
